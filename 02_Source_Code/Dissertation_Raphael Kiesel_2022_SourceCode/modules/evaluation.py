#!/usr/bin/env python
# -*- coding: utf-8 -*-
from gluon import *
import numpy as np

kpis = []

def calculateGoal(goalArray):
    newArray = [x for x in goalArray if x is not None]
    arrCount = len(newArray)
    goalVariable = 0
    for goalItem in newArray:
        itemWeight = 1 / arrCount
        goalItemVar = goalItem * itemWeight
        goalVariable = goalVariable + goalItemVar

    return goalVariable

def calculatePercChange(goalNormal, goalFiveG):
    if goalNormal == 0:
        return goalFiveG
    else:
        change = (goalFiveG/goalNormal + 1)*100
        return change


def addBarValue(goalArray, goalArray5G):
    goalNormal = calculateGoal(goalArray)
    goal5G = calculateGoal(goalArray5G)
    percChange = calculatePercChange(goalNormal, goal5G)
    kpis.append(percChange)
    return percChange

#Arguments should always be of the form [Numberwihtout5G, Numberwith5G]
def goalFlexibility(pvas, tpas, asts, apts):
    returnList = []
    returnList_5g = []
    returnList_5g_lat = []
    returnList_5g_avail = []
    returnList_5g_reli = []
    returnList_5g_ict = []
    #Setup Ratio
    if apts[0] == 0:
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
    else:
        calc = calculateSubGoal(asts,apts,'/')
        returnList.append(1-calc[0])
        returnList_5g.append(1-calc[1])
        returnList_5g_lat.append(1-calc[2])
        returnList_5g_avail.append(1-calc[3])
        returnList_5g_reli.append(1-calc[4])
        returnList_5g_ict.append(1-calc[5])
    if tpas[0] == 0:
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
    #Machine Flexibility
    else:
        calc = calculateSubGoal(pvas,tpas,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
    return returnList, returnList_5g, returnList_5g_lat, returnList_5g_avail, returnList_5g_reli, returnList_5g_ict

def goalMobility(pathssystem, pathstotal, tpa, ra):
    returnList = []
    returnList_5g = []
    returnList_5g_lat = []
    returnList_5g_avail = []
    returnList_5g_reli = []
    returnList_5g_ict = []
    #Material Handling Mobility
    if pathstotal[0] == 0:
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
    else:
        calc = calculateSubGoal(pathssystem,pathstotal,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
    #Space Productivity
    if tpa[0] == 0:
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
    else:
        calc = calculateSubGoal(tpa,ra,'-')
        calc = calculateSubGoal(calc,tpa,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
    return returnList, returnList_5g, returnList_5g_lat, returnList_5g_avail, returnList_5g_reli, returnList_5g_ict

def goalProductivity(prtpp, pq, apt, pwt, pat, aet):
    returnList = []
    returnList_5g = []
    returnList_5g_lat = []
    returnList_5g_avail = []
    returnList_5g_reli = []
    returnList_5g_ict = []
    #Effectiveness
    if pq[0] == 0:
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
    else:
        calc = calculateSubGoal(prtpp,pq,'*')
        calc = calculateSubGoal(calc,apt,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
    #Throughput Ratio
    if aet[0] == 0:
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
    else:
        calc = calculateSubGoal(pq,aet,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
    #Worker Efficiency
    if pat[0] == 0:
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
    else:
        calc = calculateSubGoal(pwt,pat,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
    return returnList, returnList_5g, returnList_5g_lat, returnList_5g_avail, returnList_5g_reli, returnList_5g_ict

def goalQuality(ftgq, iq, gq, pq, rq, sq):
    returnList = []
    returnList_5g = []
    returnList_5g_lat = []
    returnList_5g_avail = []
    returnList_5g_reli = []
    returnList_5g_ict = []
    #First Pass Yield
    if iq[0] == 0:
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
    else:
        calc = calculateSubGoal(ftgq,iq,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
    #Quality Ratio, Rework Ratio and Scrap Ratio
    if pq[0] == 0:
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
    else:
        calc = calculateSubGoal(gq,pq,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
        calc = calculateSubGoal(rq,pq,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
        calc = calculateSubGoal(sq,pq,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
    return returnList, returnList_5g, returnList_5g_lat, returnList_5g_avail, returnList_5g_reli, returnList_5g_ict

def goalSafety(acc, pat, tbf, fe, ttr):
    returnList = []
    returnList_5g = []
    returnList_5g_lat = []
    returnList_5g_avail = []
    returnList_5g_reli = []
    returnList_5g_ict = []
    fe = [x+1 for x in fe]
    #Accident Ratio, Mean Time between Failures, Mean Time to Repair
    if pat[0] == 0:
        returnList.append(1)
        returnList_5g.append(1)
        returnList_5g_lat.append(1)
        returnList_5g_avail.append(1)
        returnList_5g_reli.append(1)
        returnList_5g_ict.append(1)
        calc = calculateSubGoal(tbf,fe,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
        calc = calculateSubGoal(ttr,fe,'/')
        returnList.append(1-calc[0])
        returnList_5g.append(1-calc[1])
        returnList_5g_lat.append(1-calc[2])
        returnList_5g_avail.append(1-calc[3])
        returnList_5g_reli.append(1-calc[4])
        returnList_5g_ict.append(1-calc[5])
    else:
        calc = calculateSubGoal(acc,pat,'/')
        returnList.append(1-calc[0])
        returnList_5g.append(1-calc[1])
        returnList_5g_lat.append(1-calc[2])
        returnList_5g_avail.append(1-calc[3])
        returnList_5g_reli.append(1-calc[4])
        returnList_5g_ict.append(1-calc[5])
        calc = calculateSubGoal(tbf,fe,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
        calc = calculateSubGoal(ttr,fe,'/')
        returnList.append(1-calc[0])
        returnList_5g.append(1-calc[1])
        returnList_5g_lat.append(1-calc[2])
        returnList_5g_avail.append(1-calc[3])
        returnList_5g_reli.append(1-calc[4])
        returnList_5g_ict.append(1-calc[5])
    return returnList, returnList_5g, returnList_5g_lat, returnList_5g_avail, returnList_5g_reli, returnList_5g_ict

def goalSustainability(pq, tec, cac, epc, gc, wc):
    TECinCW = 0.00026
    returnList = []
    returnList_5g = []
    returnList_5g_lat = []
    returnList_5g_avail = []
    returnList_5g_reli = []
    returnList_5g_ict = []
    #Compressed Air, Electric Power, Water, Carbon Weight Consumption
    if pq[0] == 0:
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
        returnList.append(1-tec[0]* TECinCW)
        returnList_5g.append(1-tec[1]* TECinCW)
        returnList_5g_lat.append(1-tec[2]* TECinCW)
        returnList_5g_avail.append(1-tec[3]* TECinCW)
        returnList_5g_reli.append(1-tec[4]* TECinCW)
        returnList_5g_ict.append(1-tec[5]* TECinCW)
    else:
        calc = calculateSubGoal(cac,pq,'/')
        returnList.append(1-calc[0])
        returnList_5g.append(1-calc[1])
        returnList_5g_lat.append(1-calc[2])
        returnList_5g_avail.append(1-calc[3])
        returnList_5g_reli.append(1-calc[4])
        returnList_5g_ict.append(1-calc[5])
        
        calc = calculateSubGoal(epc,pq,'/')
        returnList.append(1-calc[0])
        returnList_5g.append(1-calc[1])
        returnList_5g_lat.append(1-calc[2])
        returnList_5g_avail.append(1-calc[3])
        returnList_5g_reli.append(1-calc[4])
        returnList_5g_ict.append(1-calc[5])

        calc = calculateSubGoal(gc,pq,'/')
        returnList.append(1-calc[0])
        returnList_5g.append(1-calc[1])
        returnList_5g_lat.append(1-calc[2])
        returnList_5g_avail.append(1-calc[3])
        returnList_5g_reli.append(1-calc[4])
        returnList_5g_ict.append(1-calc[5])

        calc = calculateSubGoal(wc,pq,'/')
        returnList.append(1-calc[0])
        returnList_5g.append(1-calc[1])
        returnList_5g_lat.append(1-calc[2])
        returnList_5g_avail.append(1-calc[3])
        returnList_5g_reli.append(1-calc[4])
        returnList_5g_ict.append(1-calc[5])

        returnList.append(1-tec[0]* TECinCW)
        returnList_5g.append(1-tec[1]* TECinCW)
        returnList_5g_lat.append(1-tec[2]* TECinCW)
        returnList_5g_avail.append(1-tec[3]* TECinCW)
        returnList_5g_reli.append(1-tec[4]* TECinCW)
        returnList_5g_ict.append(1-tec[5]* TECinCW)
    return returnList, returnList_5g, returnList_5g_lat, returnList_5g_avail, returnList_5g_reli, returnList_5g_ict

def goalUtilization(abt, pabt, apt, adt):
    returnList = []
    returnList_5g = []
    returnList_5g_lat = []
    returnList_5g_avail = []
    returnList_5g_reli = []
    returnList_5g_ict = []
    #Allocation Efficiency, Availability
    if pabt[0] == 0:
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
    else:
        calc = calculateSubGoal(abt,pabt,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])

        calc = calculateSubGoal(apt,pabt,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
    #Technical Efficiency
    if (apt[0] + adt[0]) == 0:
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
    else:
        calc = calculateSubGoal(apt,adt,'+')
        calc = calculateSubGoal(apt,calc,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
    #Utilization
    if abt[0] == 0:
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
    else:
        calc = calculateSubGoal(apt,abt,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
        utilization_efficiency = apt[0] / abt[0]
        utilization_efficiency_5g = apt[1] / abt[1]
    return returnList, returnList_5g, returnList_5g_lat, returnList_5g_avail, returnList_5g_reli, returnList_5g_ict

def calculateSubGoal(arg1, arg2, operator):
    returnList = []
    if operator == '+':
        for i in range(6):
            returnList.append(arg1[i]+arg2[i])
    elif operator == '-':
        for i in range(6):
            returnList.append(arg1[i]-arg2[i])
    elif operator == '*':
        for i in range(6):
            returnList.append(arg1[i]*arg2[i])
    elif operator == '/':
        for i in range(6):
            returnList.append(arg1[i]/arg2[i])
    return returnList

def createDataset(argList):
    dataset = []
    for data in argList:
        if data is not None:
            dataset.append(data)
    return dataset

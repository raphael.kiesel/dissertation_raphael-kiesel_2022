#!/usr/bin/env python
# -*- coding: utf-8 -*-
from gluon import *

#AGV SPEEDS
agvSpeedStraight = 3  #m/s
agvSpeedCurve = 2     #m/s
agvSpeedCrossing = 0.5  #m/s
agvSpeedPpl = 0      
agvWaitingPpl = 10    #s
#AGV SPEEDS 5G
agvSpeedStraight5G = 5  #m/s
agvSpeedCurve5G = 3       #m/s
agvSpeedCrossing5G = 2    #m/s
agvSpeedPpl5G = 0.5       #m/s
agvDistancePpl5G = 10     #m
#TIME FOR ON AND OFF LOADING
agvLoadingTime = 30     #s

#People per Meter Crossing
pplPerCrossingMeter = 0.05

#latencySensor
latencySensor = 1      #ms

#latencyActuator
latencyActuator = 1    #ms

#latencyController
latencySController = 1 #ms

#Netzwerkkosten
cost5GNetwork = 50000  #€

costCBM = 49000

costAi = 1000

costEdge = 200000

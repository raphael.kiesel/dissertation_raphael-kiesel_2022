#!/usr/bin/env python
# -*- coding: utf-8 -*-
from gluon import *

def latencyRecommendation(latency):
    if latency == 0.1:
        return 'Bluetooth, NB-IoT'
    elif latency == 0.2:
        return '4G, ISA 100.11a, LoRaWAN, Sigfox'
    elif latency <= 0.4:
        return 'WirelessHART, Wi-Fi 6, ZigBee'
    elif latency == 0.5:
        return 'WISA'
    else:
        return '5G'

def reliabilityRecommendation(reliability):
    if reliability == 0.1:
        return 'LoRaWAN, NB-IoT'
    elif reliability == 0.2:
        return '4G'
    elif reliability <= 0.5:
        return '5G'
    else:
        return 'No Recommendation'

def availabilityRecommendation(availability):
    if availability <= 0.2:
        return '4G, Wi-Fi 6'
    elif availability <= 0.5:
        return '5G'
    else:
        return 'No Recommendation'

def dataRecommendation(datarate):
    if datarate == 0.1:
        return 'Sigfox, WirelessHART, ZigBee, ISA 100.11a, WISA, Bluetooth, NB-IoT, LoRaWAN'
    elif datarate == 0.2:
        return 'Wi-Fi 6'
    elif datarate <= 0.4:
        return '4G'
    else:
        return '5G'

def conDensRecommendation(conDens):
    if conDens <= 0.2:
        return '4G'
    elif conDens == 0.3:
        return '5G'
    elif conDens == 0.4:
        return 'NB-IoT'
    else:
        return 'No Recommendation'

def locRecommendation(locprec):
    if locprec <= 0.3:
        return 'Bluetooth, Wi-Fi 6n WISA'
    elif locprec == 0.4:
        return '5G'
    else:
        return 'No Recommendation'

def comrangeRecommendation(comrange):
    if comrange <= 0.2:
        return 'Bluetooth'
    elif comrange == 0.3:
        return 'LoRaWAN, WISA'
    elif comrange == 0.4:
        return 'Wi-Fi 6'
    else:
        return '5G, 4G, Sigfox, WirelessHART, ISA 100.11a'

def mobilityRecommendation(mobility):
    if mobility <= 0.3:
        return 'NB-IoT'
    elif mobility == 0.4:
        return '4G'
    elif mobility == 0.5:
        return '5G'
    else:
        return 'No Recommendation'

def finalRecommendation(lat,rel,avail,data, cons,loc,com,mob):
    allStrings = lat + rel + avail + data + cons + loc + com + mob
    if 'No Recommendation' in allStrings:
        return 'No Recommendation. Requirements cannot be fulfilled.'
    elif '5G' in allStrings:
        return '5G'
    elif '4G' in allStrings:
        return '4G'
    elif 'Wi-Fi' in allStrings:
        return 'WiFi'
    elif 'ZigBee' in allStrings:
        return 'ZigBee'
    elif 'WirelessHART' in allStrings:
        return 'WirelessHART'
    elif 'ISA 100.11a' in allStrings:
        return 'ISA 100.11'
    elif 'WISA' in allStrings:
        return 'WISA'
    elif 'Bluetooth' in allStrings:
        return 'Bluetooth'
    elif '6LoWPAN' in allStrings:
        return '6LoWPAN'
    elif 'LoRaWAN' in allStrings:
        return 'LoRaWAN'

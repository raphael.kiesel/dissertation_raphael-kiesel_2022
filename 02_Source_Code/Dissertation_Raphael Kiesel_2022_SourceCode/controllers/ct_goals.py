# -*- coding: utf-8 -*-

def chooseGoal():
    record = db.t_goals(1)
    nextUrl = URL('ct_goals','economicGoal')
    backUrl = URL('ct_start', 'mainScreen')
    formGoals = SQLFORM(db.t_goals, record, fields=['f_flex', 'f_mob', 'f_prod', 'f_qual', 'f_safe', 'f_sus', 'f_util'])
    formGoals.custom.submit["_value"] = "Continue"
    if formGoals.process(keepvalues = True).accepted:
        response.flash = 'form accepted'
        redirect(nextUrl)
    return dict(form=formGoals, url=nextUrl, backUrl = backUrl, progress=10)

def economicGoal():
    record = db.t_goals(1)
    nextUrl = URL('ct_start', 'mainScreen')
    backUrl = URL('ct_goals', 'chooseGoal')
    formGoals = SQLFORM(db.t_goals, record, fields=['f_roi', 'f_npv','f_pb','f_opexp'])
    formGoals.custom.submit["_value"] = "Continue"
    if formGoals.process(keepvalues = True).accepted:
        response.flash = 'form accepted'
        redirect(nextUrl)
    return dict(form=formGoals, url=nextUrl, backUrl = backUrl, progress=20)

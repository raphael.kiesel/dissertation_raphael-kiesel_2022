#import calculateRecommendations as cR
from gluon.serializers import json

def selectApplication():
    agvUrl = URL('ct_recommendation', 'agvlinkfunction')
    assemblyUrl = URL('ct_recommendation', 'assemblylinkfunction')
    droneUrl = URL('ct_recommendation', 'dronelinkfunction')
    mctoolUrl = URL('ct_recommendation', 'machinetoolslinkfunction')
    backUrl = URL('ct_start', 'mainScreen')
    record = db.t_applicationselection(1)
    formUseCase = FORM(TABLE(TR(TD(INPUT(_type="radio", _value="agv", _name='useCase', _id="checkAGV", _class="radioBox"),LABEL(SPAN(_class='icon-Choose-Application_AGV'),_for="checkAGV"), _class="imgRow"),
                       TD(INPUT(_type="radio", _value="assembly", _name='useCase', _id="checkassembly", _class="radioBox", _disabled="True"),LABEL(SPAN(_class='icon-Choose-Application_Assembly-Robots deactive'),_for="checkassembly"), _class="imgRow"),
                       TD(INPUT(_type="radio", _value="drone", _name='useCase', _id="checkdrone", _class="radioBox", _disabled="True"),LABEL(SPAN(_class='icon-Choose-Application_Drones deactive'),_for="checkdrone"), _class="imgRow"),
                       TD(INPUT(_type="radio", _value="blisk", _name='useCase', _id="checkmacTool", _class="radioBox"),LABEL(SPAN(_class='icon-Choose-Application_Machine-Tool'),_for="checkmacTool"), _class="imgRow")),
                       TR(TD(XML("AGV/<br>Mobile Robots"), _class="textRow"),TD(XML("Assembly<br> Robot"), _class="textRow deactive"),TD("Drones", _class="textRow deactive"),TD(XML("Machine<br> Tools"), _class="textRow"))),
                       INPUT(_type='submit'),
                       _method = 'post')
    if formUseCase.process(keepvalues=True).accepted:
        response.flash = 'form accepted'
        if record is None:
            db.t_applicationselection[None] = dict(f_name = request.vars.useCase)
        else:
            db.t_applicationselection[1] = dict(f_name = request.vars.useCase)
        redirect(nextUrl)
    return dict(agvurl=agvUrl, assemblyurl=assemblyUrl, droneurl=droneUrl, mctoolurl=mctoolUrl,progress=10, backUrl = backUrl)

def selectUseCase():
    mcpUrl = URL('ct_recommendation', 'mobilecontrolpanelusecasefunction')
    mcUrl = URL('ct_recommendation', 'motioncontrolusecasefunction')
    mobautUrl = URL('ct_recommendation', 'agvusecasefunction')
    backUrl = URL('ct_recommendation', 'selectApplication')
    app = db(db.t_applicationselection).select().first()
    if app.f_name == 'agv':
        formUseCase = TABLE(TR(TD(XML('<button id="checkMOBAUT" class="buttonBox" onclick=' + 'window.location="' + mobautUrl +'"' + ';' + '><span class="icon-Choose-Application_Mobility-Automation"></span></button>'), _class="imgRow")),
                        TR(TD(XML("Mobility<br> Automation"), _class="textRow")))
    elif app.f_name == 'blisk':
        formUseCase = TABLE(TR(TD(XML('<button id="checkMCP" class="buttonBox" disabled="True" onclick=' + 'window.location="' + mcpUrl +'"' + ';' + '><span class="icon-Choose-Application_Mobile-Control-Panel deactive"></span></button>'), _class="imgRow"),
                       TD(XML('<button id="checkMC" class="buttonBox" onclick=' + 'window.location="' + mcUrl +'"' + ';' + '><span class="icon-Choose-Use-Case_Motion-Control"></span></button>'), _class="imgRow"),
                       TR(TD(XML("Mobile<br> Control Panels"), _class="textRow deactive"),TD(XML("Motion<br> Control"), _class="textRow"))))
    return dict(form = formUseCase, mcpUrl=mcpUrl, mcUrl=mcUrl, mobautUrl=mobautUrl, progress=10, backUrl = backUrl)

def requires():
    nextUrl = URL('ct_recommendation','recommended')
    skipUrl = URL('ct_recommendation','checkarchitecture')
    backUrl = URL('ct_recommendation', 'selectUseCase')
    formTable=FORM(TABLE(TR(TH('Category'), TH('Least difficult'), TH(), TH(), TH(), TH(), TH('Most Difficult')),
                         TR(TD('Availability (in %)' ,SPAN("measures the ability to fulfil a required functionality during a specified time interval"), _id="cat_avail"), TD(INPUT(_type='radio', _name='availG',_value=0.1, value=0.1), "<99"), TD(INPUT(_type='radio', _name='availG',_value=0.2, value=0.1), "99-99.9"), TD(INPUT(_type='radio', _name='availG',_value=0.3, value=0.1),"99.9-99.99"), TD(INPUT(_type='radio', _name='availG',_value=0.4, value=0.1),"99.99-99.999"), TD(INPUT(_type='radio', _name='availG',_value=0.5, value=0.1),"99.999-99.9999"), TD(INPUT(_type='radio', _name='availG',_value=0.6, value=0.1),">99.9999"), _id="tr_avail"),
                         TR(TD('Connection Density (in Dev./m²)', SPAN("defines the maximum number of devices per unit area that a communication network can connect"), _id="cat_con"), TD(INPUT(_type='radio', _name='conDG',_value=0.1, value=0.1), "<0.01"), TD(INPUT(_type='radio', _name='conDG',_value=0.2, value=0.1), "0.01-0.1"), TD(INPUT(_type='radio', _name='conDG',_value=0.3, value=0.1),"0.1-1"), TD(INPUT(_type='radio', _name='conDG',_value=0.4, value=0.1),"1-10"), TD(INPUT(_type='radio', _name='conDG',_value=0.5, value=0.1),"10-20"), TD(INPUT(_type='radio', _name='conDG',_value=0.6, value=0.1),"<20"), _id="tr_con"),
                         TR(TD('Data Rate (in Mbit/s)', SPAN("defines the minimum value of number of bits transmitted or received over time"), _id="cat_data"), TD(INPUT(_type='radio', _name='dataG',_value=0.1, value=0.1), "<5"), TD(INPUT(_type='radio', _name='dataG',_value=0.2, value=0.1), "5-50"), TD(INPUT(_type='radio', _name='dataG',_value=0.3, value=0.1),"50-500"), TD(INPUT(_type='radio', _name='dataG',_value=0.4, value=0.1),"500-1000"), TD(INPUT(_type='radio', _name='dataG',_value=0.5, value=0.1),"1000-2000"), TD(INPUT(_type='radio', _name='dataG',_value=0.6, value=0.1),">2000"), _id="tr_data"),
                         TR(TD('Latency (in ms)', SPAN("of a communication system is the time taken to transfer a given piece of information from an information source (e.g., sensor) to the moment it is successfully received at the destination (e.g., controller, actuator)"), _id="cat_lat"), TD(INPUT(_type='radio', _name='latencyG',_value=0.1, value=0.1), "> 100"), TD(INPUT(_type='radio', _name='latencyG',_value=0.2, value=0.1), "100-50"), TD(INPUT(_type='radio', _name='latencyG',_value=0.3, value=0.1),"50-20"), TD(INPUT(_type='radio', _name='latencyG',_value=0.4, value=0.1),"20-10"), TD(INPUT(_type='radio', _name='latencyG',_value=0.5, value=0.1),"10-1"), TD(INPUT(_type='radio', _name='latencyG',_value=0.6, value=0.1),"<1"), _id="tr_lat"),
                         TR(TD('Localization Precision (in m)', SPAN("defines the accuracy until which a communication technology can position the application"), _id="cat_loc"), TD(INPUT(_type='radio', _name='locG',_value=0.1, value=0.1), "> 10"), TD(INPUT(_type='radio', _name='locG',_value=0.2, value=0.1), "10-5"), TD(INPUT(_type='radio', _name='locG',_value=0.3, value=0.1),"5-1"), TD(INPUT(_type='radio', _name='locG',_value=0.4, value=0.1),"1-0.1"), TD(INPUT(_type='radio', _name='locG',_value=0.5, value=0.1),"0.1-0.01"), TD(INPUT(_type='radio', _name='locG',_value=0.6, value=0.1),"<0.01"), _id="tr_loc"),
                         TR(TD('Mobility (in km/h)', SPAN("of a communication network describes the ability to keep the connection between transmitter and receiver in case of movement of the application"), _id="cat_mob"), TD(INPUT(_type='radio', _name='mobG',_value=0.1, value=0.1), "<5"), TD(INPUT(_type='radio', _name='mobG',_value=0.2, value=0.1), "5-10"), TD(INPUT(_type='radio', _name='mobG',_value=0.3, value=0.1), "10-50"), TD(INPUT(_type='radio', _name='mobG',_value=0.4, value=0.1),"50-100"), TD(INPUT(_type='radio', _name='mobG',_value=0.5, value=0.1),"100-500"), TD(INPUT(_type='radio', _name='mobG',_value=0.6, value=0.1),">500"), _id="tr_mob"),
                         TR(TD('Range (in m)', SPAN("indicates the possible distance of data transfer between transmitter and receiver a communication system can enable"), _id="cat_range"), TD(INPUT(_type='radio', _name='comrG',_value=0.1, value=0.1), "<5"), TD(INPUT(_type='radio', _name='comrG',_value=0.2, value=0.1), "5-10"), TD(INPUT(_type='radio', _name='comrG',_value=0.3, value=0.1),"10-50"), TD(INPUT(_type='radio', _name='comrG',_value=0.4, value=0.1),"50-100"), TD(INPUT(_type='radio', _name='comrG',_value=0.5, value=0.1),"100-500"), TD(INPUT(_type='radio', _name='comrG',_value=0.6, value=0.1),">500"), _id="tr_range"),
                         TR(TD('Reliability (in %)', SPAN("describes the ability of the communication service to perform as required for a given time interval under given conditions without failure"), _id="cat_rel"), TD(INPUT(_type='radio', _name='reliG',_value=0.1, value=0.1), "<99"), TD(INPUT(_type='radio', _name='reliG',_value=0.2, value=0.1), "99-99.9"), TD(INPUT(_type='radio', _name='reliG',_value=0.3, value=0.1),"99.9-99.99"), TD(INPUT(_type='radio', _name='reliG',_value=0.4, value=0.1),"99.99-99.999"), TD(INPUT(_type='radio', _name='reliG',_value=0.5, value=0.1),"99.999-99.9999"), TD(INPUT(_type='radio', _name='reliG',_value=0.6, value=0.1),">99.9999"), _id="tr_rel")),
                   INPUT(_type='submit'), _method = 'get', _action=nextUrl)
    ##Delete old stuff, only from here is important
    chartLabels = [{'text':'Availabiltiy [%]', 'max': 99.999, 'min': 90},{'text':'Connection Density [Dev./m²]', 'max': 20, 'min': 0.01},{'text':'Data Rate [Mbit/s]', 'max': 2000, 'min': 0},{'text':'Latency [ms]', 'max': 100, 'min': 1},{'text':'Localization Precision [m]', 'max': 10, 'min': 0.01},{'text':'Mobility [km/h]', 'max': 500, 'min': 5},{'text':'Range [m]', 'max': 500, 'min': 5},{'text':'Reliability [%]', 'max': 99.999, 'min': 90}]
    data_5g = [99.999, 10, 50, 10, 0.1, 10, 100, 99.999]
    data_4g = []
    data_wifi = []
    app = db(db.t_applicationselection).select().first()
    imgUrl = ''
    if app.f_name == 'agv':
        imgUrl = URL('static', 'images/13_Requirements_AGV.svg')
    elif app.f_name == 'blisk':
        imgUrl = URL('static', 'images/13_Requirements_BLISK.svg')
    return dict(labels = json(chartLabels), data_5g = json(data_5g),url=nextUrl,imgUrl = imgUrl, progress=10, backUrl = backUrl)

def recommended():
    latRec = 0#cR.latencyRecommendation(float(request.vars.latencyG))
    relRec = 0#cR.reliabilityRecommendation(float(request.vars.reliG))
    avaRec = 0#cR.availabilityRecommendation(float(request.vars.availG))
    dataRec =0 #cR.dataRecommendation(float(request.vars.dataG))
    conRec = 0#cR.conDensRecommendation(float(request.vars.conDG))
    locRec = 0#cR.locRecommendation(float(request.vars.locG))
    comRec =0 #cR.comrangeRecommendation(float(request.vars.comrG))
    mobRec =0 #cR.mobilityRecommendation(float(request.vars.mobG))
    finalRec = 0#cR.finalRecommendation(latRec,relRec,avaRec,dataRec,conRec,locRec,comRec,mobRec)
    nextUrl = URL('ct_recommendation','selectcomparetech')
    backUrl = URL('ct_recommendation', 'requires')
    chosenUseCase = db(db.t_applicationselection).select().first().as_dict()["f_name"]
    if chosenUseCase == "agv":
        newTable = DIV(DIV(SPAN("Min. Application Performance", _class="tableHead"),TABLE(COL(_span="3",_width="100px"),COL(_span="10",_width="70px"),
                TR(TH('Performance Characteristic'), TH('Unit'), TH('Required Performance'), TH('5G'), TH('LTE'), TH('Wi-Fi 6'), TH('NB-IoT'), TH('ISA 110.11a'), TH('Bluetooth'), TH('Sigfox'), TH('WISA'), TH('Wireless-HART'), TH('ZigBee')),
                         TR(TD(DIV(SPAN('Availability',_style="font-weight:bold", _class="tableDescription", **{'_data-text': 'Availability measures the ability to fulfil a required functionality during a specified time interval.'}),_class="tableCat")),TD('%',_id="value"), TD(99.5,_id="value"), TD(XML("&#10004"),_style="font-weight:bold",_id="value"), TD(XML("&#10004"),_style="font-weight:bold",_id="value"), TD(XML("&#10004"),_style="font-weight:bold",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value")),
                         TR(TD(DIV('Connection Density',_class="tableDescription", **{'_data-text': 'Connection density defines the maximum number of devices per unit area that a communication network can connect.'}), _class="tableCat"),TD('Dev./m²',_id="value"), TD(0.1,_id="value"), TD(XML("&#10004"),_id="value"), TD("",_id="value"), TD("",_id="value"), TD(XML("&#10004"),_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value")),
                         TR(TD(DIV('Data Rate',_class="tableDescription", **{'_data-text': 'The data rate, also referred to as user data rate, is defined as the minimum number of bits transmitted or received over time'}), _class="tableCat"),TD('Mbit/s',_id="value"), TD(50,_id="value"), TD(XML("&#10004"),_id="value"), TD(XML("&#10004"),_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value")),
                         TR(TD(DIV(SPAN('Latency',_style="font-weight:bold",_class="tableDescription", **{'_data-text': 'Latency of a communication system is the time taken to transfer a given piece of information from an information source (e.g., sensor) to the moment it is successfully received at the destination (e.g., controller, actuator).'}), _class="tableCat")),TD('ms',_id="value"), TD(50,_id="value"), TD(XML("&#10004"),_style="font-weight:bold",_id="value"), TD(XML("&#10004"),_style="font-weight:bold",_id="value"), TD(XML("&#10004"),_style="font-weight:bold",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD(XML("&#10004"),_style="font-weight:bold",_id="value"), TD("",_id="value"), TD("",_id="value")),
                         TR(TD(DIV('Localization Precision',_class="tableDescription", **{'_data-text': 'Localization precision defines the accuracy to which a communication technology can position the application.'}), _class="tableCat"),TD('m',_id="value"), TD(0.1,_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value")),
                         TR(TD(DIV('Mobility',_class="tableDescription", **{'_data-text': 'Mobility of a communication network describes the ability to keep the connection between transmitter and receiver in case of movement of the application.'}), _class="tableCat"),TD('km/h',_id="value"), TD(10,_id="value"), TD(XML("&#10004"),_id="value"), TD("",_id="value"), TD(XML("&#10004"),_id="value"), TD(XML("&#10004"),_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value")),
                         TR(TD(DIV('Range',_class="tableDescription", **{'_data-text': 'The range indicates the possible distance of data transfer between transmitter and receiver of a set data rate a communication system can enable.'}), _class="tableCat"),TD('m',_id="value"), TD(50,_id="value"), TD(XML("&#10004"),_id="value"), TD(XML("&#10004"),_id="value"), TD(XML("&#10004"),_id="value"), TD(XML("&#10004"),_id="value"), TD(XML("&#10004"),_id="value"), TD("",_id="value"), TD(XML("&#10004"),_id="value"), TD("",_id="value"), TD(XML("&#10004"),_id="value"), TD("",_id="value")),
                         TR(TD(DIV(SPAN('Reliability',_style="font-weight:bold",_class="tableDescription", **{'_data-text': 'According to IEC 61907, reliability describes the ability of the communication service to perform as required for a given time interval under given conditions without failure.'}), _class="tableCat")),TD('%',_id="value"), TD(99.5,_id="value"), TD(XML("&#10004"),_style="font-weight:bold",_id="value"), TD(XML("&#10004"),_style="font-weight:bold",_id="value"), TD(XML("&#10004"),_style="font-weight:bold",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value")),
                         TR(TD(DIV(SPAN('Suitable',_style="font-weight:bold",_class="tableDescription", **{'_data-text': 'Technologies that are suitable for the Application'}), _class="tableCat")),TD('',_id="value"), TD('',_id="value"), TD(XML("&#10004"),_id="value"), TD(XML("&#10004"),_id="value"), TD(XML("&#10004"),_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD('',_id="value")), _class="table1")),
                      DIV(SPAN("Max. Application Performance", _class="tableHead"),TABLE(COL(_span="3",_width="100px"),COL(_span="10",_width="70px"),
                        TR(TH('Performance Characteristic'),TH('Unit'), TH('Required Performance'), TH('5G'), TH('LTE'), TH('Wi-Fi 6'), TH('NB-IoT'), TH('ISA 110.11a'), TH('Bluetooth'), TH('Sigfox'), TH('WISA'), TH('Wireless-HART'), TH('ZigBee')),
                         TR(TD(DIV(SPAN('Availability',_style="font-weight:bold", _class="tableDescription", **{'_data-text': 'Availability measures the ability to fulfil a required functionality during a specified time interval.'}),_class="tableCat")),TD('%',_id="value"), TD(99.99,_id="value"), TD(XML("&#10004"),_style="font-weight:bold",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value")),
                         TR(TD(DIV('Connection Density',_class="tableDescription", **{'_data-text': 'Connection density defines the maximum number of devices per unit area that a communication network can connect.'}), _class="tableCat"),TD('Dev./m²',_id="value"), TD(0.1,_id="value"), TD(XML("&#10004"),_id="value"), TD("",_id="value"), TD("",_id="value"), TD(XML("&#10004"),_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value")),
                         TR(TD(DIV('Data Rate',_class="tableDescription", **{'_data-text': 'The data rate, also referred to as user data rate, is defined as the minimum number of bits transmitted or received over time'}), _class="tableCat"),TD('Mbit/s',_id="value"), TD(50,_id="value"), TD(XML("&#10004"),_id="value"), TD(XML("&#10004"),_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value")),
                         TR(TD(DIV(SPAN('Latency',_style="font-weight:bold",_class="tableDescription", **{'_data-text': 'Latency of a communication system is the time taken to transfer a given piece of information from an information source (e.g., sensor) to the moment it is successfully received at the destination (e.g., controller, actuator).'}), _class="tableCat")),TD('ms',_id="value"), TD(10,_id="value"), TD(XML("&#10004"),_style="font-weight:bold",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD(XML("&#10004"),_style="font-weight:bold",_id="value"), TD("",_id="value"), TD("",_id="value")),
                         TR(TD(DIV('Localization Precision',_class="tableDescription", **{'_data-text': 'Localization precision defines the accuracy to which a communication technology can position the application.'}), _class="tableCat"),TD('m',_id="value"), TD(0.1,_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value")),
                         TR(TD(DIV('Mobility',_class="tableDescription", **{'_data-text': 'Mobility of a communication network describes the ability to keep the connection between transmitter and receiver in case of movement of the application.'}), _class="tableCat"),TD('km/h',_id="value"), TD(10,_id="value"), TD(XML("&#10004"),_id="value"), TD("",_id="value"), TD(XML("&#10004"),_id="value"), TD(XML("&#10004"),_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value")),
                         TR(TD(DIV('Range',_class="tableDescription", **{'_data-text': 'The range indicates the possible distance of data transfer between transmitter and receiver of a set data rate a communication system can enable.'}), _class="tableCat"),TD('m',_id="value"), TD(100,_id="value"), TD(XML("&#10004"),_id="value"), TD(XML("&#10004"),_id="value"), TD("",_id="value"), TD(XML("&#10004"),_id="value"), TD(XML("&#10004"),_id="value"), TD("",_id="value"), TD(XML("&#10004"),_id="value"), TD("",_id="value"), TD(XML("&#10004"),_id="value"), TD("",_id="value")),
                         TR(TD(DIV(SPAN('Reliability',_style="font-weight:bold",_class="tableDescription", **{'_data-text': 'According to IEC 61907, reliability describes the ability of the communication service to perform as required for a given time interval under given conditions without failure.'}), _class="tableCat")),TD('%',_id="value"), TD(99.999,_id="value"), TD(XML("&#10004"),_style="font-weight:bold",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value")),
                        TR(TD(DIV(SPAN('Suitable',_style="font-weight:bold",_class="tableDescription", **{'_data-text': 'Technologies that are suitable for the Application'}), _class="tableCat")),TD('',_id="value"), TD('',_id="value"), TD(XML("&#10004"),_id="value"), TD(XML(""),_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD('',_id="value")),_class="table2")), _class="tableDiv")
    else:
        newTable = DIV(DIV(TABLE(COL(_span="3",_width="100px"),COL(_span="10",_width="70px"),
                TR(TH('Performance Characteristic'), TH('Unit'), TH('Min. Performance'), TH('5G'), TH('LTE'), TH('Wi-Fi 6'), TH('NB-IoT'), TH('ISA 110.11a'), TH('Bluetooth'), TH('Sigfox'), TH('WISA'), TH('Wireless-HART'), TH('ZigBee')),
                         TR(TD(DIV(SPAN('Availability',_style="font-weight:bold", _class="tableDescription", **{'_data-text': 'Availability measures the ability to fulfil a required functionality during a specified time interval.'}),_class="tableCat")),TD('%',_id="value"), TD(99.5,_id="value"), TD(XML("&#10004"),_style="font-weight:bold",_id="value"), TD("",_style="font-weight:bold",_id="value"), TD("",_style="font-weight:bold",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value")),
                         TR(TD(DIV('Connection Density',_class="tableDescription", **{'_data-text': 'Connection density defines the maximum number of devices per unit area that a communication network can connect.'}), _class="tableCat"),TD('Dev./m²',_id="value"), TD(0.1,_id="value"), TD(XML("&#10004"),_id="value"), TD("",_id="value"), TD("",_id="value"), TD(XML("&#10004"),_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value")),
                         TR(TD(DIV('Data Rate',_class="tableDescription", **{'_data-text': 'The data rate, also referred to as user data rate, is defined as the minimum number of bits transmitted or received over time'}), _class="tableCat"),TD('Mbit/s',_id="value"), TD(50,_id="value"), TD(XML("&#10004"),_id="value"), TD(XML("&#10004"),_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value")),
                         TR(TD(DIV(SPAN('Latency',_style="font-weight:bold",_class="tableDescription", **{'_data-text': 'Latency of a communication system is the time taken to transfer a given piece of information from an information source (e.g., sensor) to the moment it is successfully received at the destination (e.g., controller, actuator).'}), _class="tableCat")),TD('ms',_id="value"), TD(50,_id="value"), TD(XML("&#10004"),_style="font-weight:bold",_id="value"), TD("",_style="font-weight:bold",_id="value"), TD("",_style="font-weight:bold",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_style="font-weight:bold",_id="value"), TD("",_id="value"), TD("",_id="value")),
                         TR(TD(DIV('Localization Precision',_class="tableDescription", **{'_data-text': 'Localization precision defines the accuracy to which a communication technology can position the application.'}), _class="tableCat"),TD('m',_id="value"), TD(0.1,_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value")),
                         TR(TD(DIV('Mobility',_class="tableDescription", **{'_data-text': 'Mobility of a communication network describes the ability to keep the connection between transmitter and receiver in case of movement of the application.'}), _class="tableCat"),TD('km/h',_id="value"), TD(10,_id="value"), TD(XML("&#10004"),_id="value"), TD("",_id="value"), TD(XML("&#10004"),_id="value"), TD(XML("&#10004"),_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value")),
                         TR(TD(DIV('Range',_class="tableDescription", **{'_data-text': 'The range indicates the possible distance of data transfer between transmitter and receiver of a set data rate a communication system can enable.'}), _class="tableCat"),TD('m',_id="value"), TD(50,_id="value"), TD(XML("&#10004"),_id="value"), TD(XML("&#10004"),_id="value"), TD(XML("&#10004"),_id="value"), TD(XML("&#10004"),_id="value"), TD(XML("&#10004"),_id="value"), TD("",_id="value"), TD(XML("&#10004"),_id="value"), TD("",_id="value"), TD(XML("&#10004"),_id="value"), TD("",_id="value")),
                         TR(TD(DIV(SPAN('Reliability',_style="font-weight:bold",_class="tableDescription", **{'_data-text': 'According to IEC 61907, reliability describes the ability of the communication service to perform as required for a given time interval under given conditions without failure.'}), _class="tableCat")),TD('%',_id="value"), TD(99.5,_id="value"), TD(XML("&#10004"),_style="font-weight:bold",_id="value"), TD(XML("&#10004"),_style="font-weight:bold",_id="value"), TD("",_style="font-weight:bold",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value")),
                         TR(TD(DIV(SPAN('Suitable',_style="font-weight:bold",_class="tableDescription", **{'_data-text': 'Technologies that are suitable for the Application'}), _class="tableCat")),TD('',_id="value"), TD('',_id="value"), TD(XML("&#10004"),_id="value"), TD(XML(""),_id="value"), TD(XML(""),_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD("",_id="value"), TD('',_id="value")), _class="table1")), _class="tableDiv", _style="justify-content: center !important")
    
    return dict(form = newTable, url=nextUrl,progress=10, backUrl = backUrl)

def selectcomparetech():
    chosenUseCase = db(db.t_applicationselection).select().first().as_dict()["f_name"]
    backUrl = URL('ct_recommendation', 'recommended')
    if chosenUseCase == "agv":
        urlLTE = URL('ct_recommendation','chooseLTE')
        urlWIFI = URL('ct_recommendation','chooseWiFi')
    else:
        urlLTE = ""
        urlWIFI = URL('ct_recommendation','chooseEthernet')
    return dict(urlLTE = urlLTE,urlWIFI = urlWIFI, progress = 5, backUrl = backUrl)

def comparetechspecs():
    nextUrl = URL('ct_recommendation', 'checkarchitecture')
    backUrl = URL('ct_recommendation', 'selectcomparetech')
    record = db.t_comparetechspecs(1)
    techForm = SQLFORM(db.t_comparetechspecs, record)
    techForm.custom.submit["_value"] = "Continue"
    choseLTE = db(db.t_comparetech).select().first().as_dict()["f_lte"]
    if choseLTE:
        latency = 10
        if record is None:
            techForm.vars.f_availability = 99.9
            techForm.vars.f_reliability = 99.9
        else:
            techForm.vars.f_availability = record.as_dict()["f_availability"]*100
            techForm.vars.f_reliability = record.as_dict()["f_reliability"]*100
    else:
        latency = 50
        if record is None:
            techForm.vars.f_availability = 99.5
            techForm.vars.f_reliability = 99.5
        else:
            techForm.vars.f_availability = record.as_dict()["f_availability"]*100
            techForm.vars.f_reliability = record.as_dict()["f_reliability"]*100
    if techForm.process(keepvalues = True, dbio=True, onvalidation=comparetechspectransform).accepted: #Accepts method makes it so it shows the values that were added
        response.flash = 'form accepted'
        redirect(nextUrl)
    return dict(form = techForm, url = nextUrl, progress = 5,backUrl = backUrl, latency=latency)

def checkarchitecture():
    chosenUseCase = db(db.t_applicationselection).select().first().as_dict()["f_name"]
    backUrl = URL('ct_recommendation', 'comparetechspecs')
    if chosenUseCase == "agv":
        urlexist = URL('ct_recommendation','existornew')
    else:
        urlexist = URL('ct_recommendation','bliskData')
    record = db.t_statusquo(1)
    architectureForm = SQLFORM(db.t_statusquo, record)
    architectureForm.custom.submit["_value"] = "Continue"
    if architectureForm.process(keepvalues = True, dbio=True).accepted: #Accepts method makes it so it shows the values that were added
        response.flash = 'form accepted'
        redirect(urlexist)
    return dict(urlexist = urlexist, form = architectureForm,progress = 5, backUrl = backUrl)

def existornew():
    urlexist = URL('ct_recommendation','existingagv')
    backUrl = URL('ct_recommendation', 'selectcomparetech')
    urlnew = URL('ct_recommendation','newagv', vars=dict(new=True))
    return dict(urlexist = urlexist, urlnew = urlnew, progress = 5, backUrl = backUrl)

def existingagv():
    record = db.t_applicationData_mobileRobots(1)
    backUrl = URL('ct_recommendation', 'existornew')
    nextUrl = URL('ct_recommendation','sameordifferent')
    existForm = SQLFORM(db.t_applicationData_mobileRobots, record)
    existForm.custom.submit["_value"] = "Continue"
    if record is None:
        agvFormPrefill(existForm)
    if existForm.process(keepvalues = True, dbio=True, onvalidation=existcheck).accepted: #Accepts method makes it so it shows the values that were added
        response.flash = 'form accepted'
        redirect(nextUrl)
    return dict(form = existForm, url = nextUrl, progress = 5, backUrl = backUrl)

def newagv():
    record = db.t_applicationData_mobileRobots(1)
    backUrl = URL('ct_recommendation', 'existornew')
    nextUrl = URL('ct_recommendation','sameordifferent')
    existForm = SQLFORM(db.t_applicationData_mobileRobots, record)
    existForm.custom.submit["_value"] = "Continue"
    if record is None:
        agvFormPrefill(existForm)
    if request.vars.fromsame:
        nextUrl = URL('ct_recommendation','differentapplication')
    if request.vars.new:
        redirect(URL('ct_recommendation','sameordifferent', vars=dict(new=True)))
    if existForm.process(keepvalues = True, dbio=True, onvalidation=newcheck).accepted: #Accepts method makes it so it shows the values that were added
        response.flash = 'form accepted'
        redirect(nextUrl)
    return dict(form = existForm, url = nextUrl, progress = 5, backUrl = backUrl)

def sameordifferent():
    urlsame = URL('ct_recommendation','sameapplication')
    urldifferent = URL('ct_recommendation','differentapplication')
    chosenUseCase = db(db.t_applicationselection).select().first().as_dict()["f_name"]
    if chosenUseCase == "agv": #TODO: LOGIK FEINER MACHEN
        backUrl = URL('ct_recommendation','existornew')
    else:
        backUrl = URL('ct_recommendation','existornew')
    if request.vars.new:
        urldifferent = URL('ct_recommendation','newagv', vars=dict(fromsame=True))
        urlsame = URL('ct_recommendation','sameapplication', vars=dict(new=True))
    return dict(urlsame = urlsame, urldifferent = urldifferent, progress = 5, backUrl = backUrl)

def differentapplication():
    chosenUseCase = db(db.t_applicationselection).select().first().as_dict()["f_name"]
    #Fetch the fitting record
    if chosenUseCase == "agv":
        record = db.t_applicationData_mobileRobots(2)
        if request.vars.new:
            record = db.t_applicationData_mobileRobots(1)
    elif chosenUseCase == "blisk":
        record = db.t_applicationData_blisk(2)
        if request.vars.new:
            record = db.t_applicationData_blisk(1)
    nextUrl = URL('ct_start','mainScreen')
    if chosenUseCase == "agv":
        if record is not None:
            existForm = SQLFORM(db.t_applicationData_mobileRobots, record)
        else:
            existForm = SQLFORM(db.t_applicationData_mobileRobots)
            agvForm5GPrefill(existForm)
    elif chosenUseCase == "blisk":
        if record is not None:
            existForm = SQLFORM(db.t_applicationData_blisk, record)
        else:
            existForm = SQLFORM(db.t_applicationData_blisk)
            bliskFormPrefill(existForm)
    existForm.custom.submit["_value"] = "Continue"
    backUrl = URL('ct_recommendation','sameordifferent')
    if existForm.process(keepvalues = True, dbio=True, onvalidation=newcheck).accepted: #Accepts method makes it so it shows the values that were added
        response.flash = 'form accepted'
        redirect(nextUrl)
    return dict(form = existForm, url = nextUrl, progress = 5, backUrl = backUrl)

def sameapplication():
    nextUrl = URL('ct_start','mainScreen')
    backUrl = URL('ct_recommendation','sameordifferent')
    chosenUseCase = db(db.t_applicationselection).select().first().as_dict()["f_name"]
    if chosenUseCase == "agv":
        record = db.t_applicationData_mobileRobots(2)
    else:
        record = db.t_applicationData_blisk(2)
    if chosenUseCase == "agv":
        if record is not None:
            existForm = SQLFORM(db.t_applicationData_mobileRobots, record)
        else:
            existForm = SQLFORM(db.t_applicationData_mobileRobots)
            agvForm5GPrefill(existForm)
    else:
        redirect(nextUrl)
    existForm.custom.submit["_value"] = "Continue"
    if request.vars.new:
        if existForm.process(keepvalues = True, dbio=True, onvalidation=newcheck).accepted: #Accepts method makes it so it shows the values that were added
            if record is None:
                db.t_applicationData_mobileRobots[None] = db.t_applicationData_mobileRobots(1)
            else:
                db.t_applicationData_mobileRobots[2] = db.t_applicationData_mobileRobots(1)
            response.flash = 'form accepted'
            redirect(nextUrl)
    elif existForm.process(keepvalues = True, dbio=True, onvalidation=sameexistcheck).accepted: #Accepts method makes it so it shows the values that were added
        response.flash = 'form accepted'
        redirect(nextUrl)
    return dict(form = existForm, url = nextUrl, progress = 5, backUrl = backUrl)


def bliskData():
    record = db.t_applicationData_blisk(1)
    backUrl = URL('ct_recommendation', 'checkarchitecture')
    nextUrl = URL('ct_recommendation','sameordifferent')
    existForm = SQLFORM(db.t_applicationData_blisk, record)
    existForm.custom.submit["_value"] = "Continue"
    if record is None:
        bliskFormPrefill(existForm)
    if existForm.process(keepvalues = True, dbio=True).accepted: #Accepts method makes it so it shows the values that were added
        response.flash = 'form accepted'
        redirect(nextUrl)
    return dict(form = existForm, url = nextUrl, progress = 5, backUrl = backUrl)


#Functions to control the DB inputs, they are not connected to views

##################SELECT APPLICATION LINKS#################
def agvlinkfunction():
    nextUrl = URL('ct_recommendation', 'selectUseCase')
    record = db.t_applicationselection(1)
    if record is None:
        db.t_applicationselection[None] = dict(f_name = 'agv')
    else:
        db.t_applicationselection[1] = dict(f_name = 'agv')
    redirect(nextUrl)
    
def machinetoolslinkfunction():
    nextUrl = URL('ct_recommendation', 'selectUseCase')
    record = db.t_applicationselection(1)
    if record is None:
        db.t_applicationselection[None] = dict(f_name = 'blisk')
    else:
        db.t_applicationselection[1] = dict(f_name = 'blisk')
    redirect(nextUrl)
    
def assemblylinkfunction():
    nextUrl = URL('ct_recommendation', 'selectUseCase')
    record = db.t_applicationselection(1)
    if record is None:
        db.t_applicationselection[None] = dict(f_name = 'assembly')
    else:
        db.t_applicationselection[1] = dict(f_name = 'assembly')
    redirect(nextUrl)
    
def dronelinkfunction():
    nextUrl = URL('ct_recommendation', 'selectUseCase')
    record = db.t_applicationselection(1)
    if record is None:
        db.t_applicationselection[None] = dict(f_name = 'drone')
    else:
        db.t_applicationselection[1] = dict(f_name = 'drone')
    redirect(nextUrl)

    
##################SELECT USE CASE LINKS#################
def agvusecasefunction():
    nextUrl = URL('ct_recommendation', 'requires')
    record = db.t_choices(1)
    if record is None:
        db.t_choices[None] = dict(f_useCase = 'mobaut')
    else:
        db.t_choices[1] = dict(f_useCase = 'mobaut')
    redirect(nextUrl)

def motioncontrolusecasefunction():
    nextUrl = URL('ct_recommendation', 'requires')
    record = db.t_choices(1)
    if record is None:
        db.t_choices[None] = dict(f_useCase = 'mc')
    else:
        db.t_choices[1] = dict(f_useCase = 'mc')
    redirect(nextUrl)

def mobilecontrolpanelusecasefunction():
    nextUrl = URL('ct_recommendation', 'requires')
    record = db.t_choices(1)
    if record is None:
        db.t_choices[None] = dict(f_useCase = 'mcp')
    else:
        db.t_choices[1] = dict(f_useCase = 'mcp')
    redirect(nextUrl)

def futureapplication():
    return dict()

def existcheck(form):
    form.vars.f_existing = True
    
def newcheck(form):
    form.vars.f_existing = False
    
def sameexistcheck(form):
    chosenUseCase = db(db.t_applicationselection).select().first().as_dict()["f_name"]
    if chosenUseCase == "agv":
        record = db.t_applicationData_mobileRobots(1).as_dict()
    form.vars.f_existing = False
    form.vars.f_batterychargingduration = record['f_batterychargingduration']
    form.vars.f_batterycapacity = record['f_batterycapacity']
    form.vars.f_batteryvoltage = record['f_batteryvoltage']
    
def chooseLTE():
    nextUrl = URL('ct_recommendation', 'comparetechspecs')
    record = db.t_comparetech(1)
    if record is None:
        db.t_comparetech[None] = dict(f_lte = True, f_wifi = False)
    else:
        db.t_comparetech[1] = dict(f_lte = True, f_wifi = False)
    redirect(nextUrl)
    
def chooseWiFi():
    nextUrl = URL('ct_recommendation', 'comparetechspecs')
    record = db.t_comparetech(1)
    if record is None:
        db.t_comparetech[None] = dict(f_lte = False, f_wifi = True)
    else:
        db.t_comparetech[1] = dict(f_lte = False, f_wifi = True)
    redirect(nextUrl)
    
def chooseEthernet():
    nextUrl = URL('ct_recommendation', 'checkarchitecture')
    record = db.t_comparetech(1)
    if record is None:
        db.t_comparetech[None] = dict(f_lte = False, f_wifi = True)
    else:
        db.t_comparetech[1] = dict(f_lte = False, f_wifi = True)
    redirect(nextUrl)


def comparetechspectransform(form):
    form.vars.f_availability = form.vars.f_availability/100
    form.vars.f_reliability = form.vars.f_reliability/100

def agvFormPrefill(form):
    form.vars.f_batteryoperationtimepercharge_planned = 3
    form.vars.f_batterycapacity = 20
    form.vars.f_batterychargingduration = 4
    form.vars.f_batteryvoltage = 24
    form.vars.f_speed_average = 0.9
    form.vars.f_speedcurve_average = 0.9
    form.vars.f_speedcrossing_average = 0.75
    form.vars.f_loadingduration = 300

def agvForm5GPrefill(form):
    form.vars.f_batteryoperationtimepercharge_planned = 3
    form.vars.f_batterycapacity = 20
    form.vars.f_batterychargingduration = 4
    form.vars.f_batteryvoltage = 24
    form.vars.f_speed_average = 1
    form.vars.f_speedcurve_average = 1
    form.vars.f_speedcrossing_average = 0.8
    form.vars.f_loadingduration = 270

def bliskFormPrefill(form):
    form.vars.f_workpiecechange_duration = 0.166
    form.vars.f_toolchange_duration = 0.166
    form.vars.f_toolchange_number = 1

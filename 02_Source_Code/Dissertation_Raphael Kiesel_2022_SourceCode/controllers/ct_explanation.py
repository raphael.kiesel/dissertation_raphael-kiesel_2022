# -*- coding: utf-8 -*-
def mainScreen():
    chosenUseCase = 0
    try:
        chosenUseCase = db(db.t_applicationselection).select().first().as_dict()["f_name"]
    except:
        pass
    productionUrl = URL('ct_explanation', 'mainScreen')
    processUrl = productionUrl
    facilityUrl = productionUrl
    failureUrl = productionUrl
    goalUrl = productionUrl
    evaluationUrl = productionUrl
    useCaseUrl = productionUrl
    if chosenUseCase == "agv":
        loopUrl = productionUrl
    elif chosenUseCase == "blisk":
        loopUrl = productionUrl
    else:
        loopUrl = productionUrl
    recommendationUrl = productionUrl
    emptydbUrl = productionUrl
    return dict(useCaseUrl=useCaseUrl, productionUrl=productionUrl, processUrl=processUrl, facilityUrl=facilityUrl, failureUrl=failureUrl, goalUrl=goalUrl, evaluationUrl=evaluationUrl, loopUrl=loopUrl, recommendationUrl=recommendationUrl, emptydbUrl = emptydbUrl)

def applicationSelectionExplanation():
    return

def goalSelectionExplanation():
    return

def controlTaskExplanation():
    return

def dataInputExplanation():
    return

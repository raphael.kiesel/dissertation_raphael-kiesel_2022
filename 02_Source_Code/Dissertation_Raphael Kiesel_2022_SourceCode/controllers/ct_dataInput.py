# -*- coding: utf-8 -*-
try:
    chosenUseCase = db(db.t_applicationselection).select().first().as_dict()["f_name"]
except:
    redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Please choose your Application before you enter the Data')))
try:
    goals = db(db.t_goals).select().first().as_dict()
except:
    redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Please choose your Goals before you enter the Data')))


def overview():
    backUrl = URL('ct_start','mainScreen')
    tecproductdataUrl = URL('ct_dataInput','techProductdata')
    tecprocessdataUrl = URL('ct_dataInput','techProcessdata')
    tecfailuredataUrl = URL('ct_dataInput','techFailuredata')
    facilitydataUrl = URL('ct_dataInput','techFacilitydata')
    investmentdataUrl = URL('ct_dataInput','investmentData')
    return dict(mainScreenURL = backUrl, tecProdUrl = tecproductdataUrl, tecProcUrl = tecprocessdataUrl, tecFailUrl = tecfailuredataUrl, facUrl = facilitydataUrl, investUrl=investmentdataUrl )

def productdata():
    record = db.t_economicProductData(1)
    nextUrl = URL('ct_dataInput','overview')
    backUrl = URL('ct_dataInput', 'techProductdata')
    formInput = SQLFORM(db.t_economicProductData, record)
    formInput.custom.submit["_value"] = "Continue"
    #If Data is input for the first time, prefill with standard data from us
    if record is None:
        if chosenUseCase == "agv":
            agvformfill(formInput)
        else:
            bliskformfill(formInput)
    if formInput.process(keepvalues = True, dbio=True).accepted: #Accepts method makes it so it shows the values that were added
        response.flash = 'form accepted'
        redirect(nextUrl)
    return dict(form=formInput, url=nextUrl, backUrl = backUrl, progress=20)

def techProductdata():
    record = db.t_technicalProductData(1)
    if (goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        nextUrl = URL('ct_dataInput','productdata')
    else:
        nextUrl = URL('ct_dataInput','overview')
    backUrl = URL('ct_dataInput', 'overview')
    if chosenUseCase == "agv":
        if not (goals['f_flex'] or goals['f_qual']):
            redirect(nextUrl)
    formInput = SQLFORM(db.t_technicalProductData, record, submit_button='Submit', _keepvalues = True)
    formInput.custom.submit["_value"] = "Continue"
    #If Data is input for the first time, prefill with standard data from us
    if record is None:
        if chosenUseCase == "agv":
            agvformfill(formInput)
        else:
            bliskformfill(formInput)
    if formInput.process(keepvalues = True, dbio=True).accepted:
        response.flash = 'form accepted'
        redirect(nextUrl)
    return dict(form=formInput, url=nextUrl, backUrl = backUrl, progress=30)

def processdata():
    record = db.t_economicProcessData(1)
    nextUrl = URL('ct_dataInput','overview')
    backUrl = URL('ct_dataInput', 'techProcessdata')
    formInput = SQLFORM(db.t_economicProcessData, record, submit_button='Submit')
    formInput.custom.submit["_value"] = "Continue"
    #If Data is input for the first time, prefill with standard data from us
    if record is None:
        if chosenUseCase == "agv":
            agvformfill(formInput)
        else:
            bliskformfill(formInput)
    if formInput.process(keepvalues = True, dbio=True).accepted:
        response.flash = 'form accepted'
        redirect(nextUrl)
    return dict(form=formInput, url=nextUrl, backUrl = backUrl, progress=40)

def techProcessdata():
    record = db.t_technicalProcessData(1)
    if (goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        nextUrl = URL('ct_dataInput','processdata')
    else:
        nextUrl = URL('ct_dataInput','overview')
    backUrl = URL('ct_dataInput', 'overview')
    formInput = SQLFORM(db.t_technicalProcessData, record, submit_button='Submit')
    formInput.custom.submit["_value"] = "Continue"
    #If Data is input for the first time, prefill with standard data from us
    if record is None:
        if chosenUseCase == "agv":
            agvformfill(formInput)
        else:
            bliskformfill(formInput)
    if formInput.process(keepvalues = True, dbio=True).accepted:
        response.flash = 'form accepted'
        redirect(nextUrl)
    return dict(form=formInput, url=nextUrl, backUrl = backUrl, progress=50)

def failuredata():
    record = db.t_economicFailureData(1)
    nextUrl = URL('ct_dataInput','overview')
    if chosenUseCase == "agv":
        backUrl = URL('ct_dataInput', 'overview')
    else:
        backUrl = URL('ct_dataInput', 'techFailuredata')
    formInput = SQLFORM(db.t_economicFailureData, record, submit_button='Submit',)
    formInput.custom.submit["_value"] = "Continue"
    #If Data is input for the first time, prefill with standard data from us
    if record is None:
        if chosenUseCase == "agv":
            agvformfill(formInput)
        else:
            bliskformfill(formInput)
    if formInput.process(keepvalues = True, dbio=True).accepted:
        response.flash = 'form accepted'
        redirect(nextUrl)
    return dict(form=formInput, url=nextUrl, backUrl = backUrl, progress=60)

def techFailuredata():
    record = db.t_technicalFailureData(1)
    if (goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        nextUrl = URL('ct_dataInput','failuredata')
    else:
        nextUrl = URL('ct_dataInput','overview')
    backUrl = URL('ct_dataInput', 'overview')
    if chosenUseCase == "agv":
        formInput = SQLFORM(db.t_technicalFailureData, record, submit_button='Submit')
        formInput.custom.submit["_value"] = "Continue"
    else:
        formInput = SQLFORM(db.t_technicalFailureData, record, submit_button='Submit')
        formInput.custom.submit["_value"] = "Continue"
    #If Data is input for the first time, prefill with standard data from us
    if record is None:
        if chosenUseCase == "agv":
            agvformfill(formInput)
        else:
            bliskformfill(formInput)
    if formInput.process(keepvalues = True, dbio=True, onvalidation=tecFailDataCompletion).accepted:
        response.flash = 'form accepted'
        redirect(nextUrl)
    return dict(form=formInput, url=nextUrl, backUrl = backUrl, progress=70)

def facility():
    record = db.t_economicFacilitydata(1)
    nextUrl = URL('ct_dataInput','overview')
    backUrl = URL('ct_dataInput', 'techFacilitydata')
    if not (goals['f_mob'] or goals['f_sus']):
        backUrl = nextUrl
    formInput = SQLFORM(db.t_economicFacilitydata, record, submit_button='Submit')
    formInput.custom.submit["_value"] = "Continue"
    #If Data is input for the first time, prefill with standard data from us
    if record is None:
        if chosenUseCase == "agv":
            agvformfill(formInput)
        else:
            bliskformfill(formInput)
    if formInput.process(keepvalues = True, dbio=True, onvalidation=facDataCompletion).accepted:
        response.flash = 'form accepted'
        redirect(nextUrl)
    return dict(form=formInput, url=nextUrl, backUrl = backUrl, progress=70.5)

def techFacilitydata():
    record = db.t_facilitydata(1)
    nextUrl = URL('ct_dataInput','facility')
    if not (goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        nextUrl = URL('ct_dataInput', 'overview')
    backUrl = URL('ct_dataInput', 'overview')
    if chosenUseCase == "agv":
        if not (goals['f_mob'] or goals['f_sus'] or goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
            redirect(nextUrl)
    formInput = SQLFORM(db.t_facilitydata, record, submit_button='Submit')
    formInput.custom.submit["_value"] = "Continue"
    #If Data is input for the first time, prefill with standard data from us
    if record is None:
        if chosenUseCase == "agv":
            agvformfill(formInput)
        elif chosenUseCase == "blisk":
            bliskformfill(formInput)
    if formInput.process(keepvalues = True, dbio=True, onvalidation=facDataCompletion).accepted:
        response.flash = 'form accepted'
        redirect(nextUrl)
    return dict(form=formInput, url=nextUrl, backUrl = backUrl, progress=70.5)

def investmentData():
    record = db.t_investmentdata(1)
    nextUrl = URL('ct_dataInput','overview')
    backUrl = URL('ct_dataInput', 'overview')
    formInput = SQLFORM(db.t_investmentdata, record)
    formInput.custom.submit["_value"] = "Continue"
    if formInput.process(keepvalues = True, dbio=True).accepted:
        response.flash = 'form accepted'
        redirect(nextUrl)
    return dict(form=formInput, url=nextUrl, backUrl = backUrl, progress=70.5)

def showDB():
    nextUrl = URL('ct_results','calculations') #technicalPotential
    formInput = SQLFORM.grid(db.t_loops)
    return dict(form=formInput, url=nextUrl, progress=70.5)

########Site to view which data is still missing
def missingData():
    #Get all Fields that are necessary
    necessaryDataDict = getNecessaryInputData()
    #Filter out fields that are already filled in
    for liste in necessaryDataDict.values():
        checkIfRecordsExist(liste)
    for liste in necessaryDataDict.values():
        insertFieldLabel(liste)
    return dict(missingFacData = necessaryDataDict['Facility'], missingFailData = necessaryDataDict['Failure'], missingProcData = necessaryDataDict['Process'], missingProdData = necessaryDataDict['Product'])
##########################Following are functions without any views

def insertFieldLabel(liste):
    for field in liste:
        #Look for the right table
        for table_name in db.tables:
            #In Try Except Block to dodge errors from empty tables
            try:
                for i in range(len(liste)):
                    if liste[i] == field:
                        liste[i] = db[table_name][field].label
                        break
                    pass
            except KeyError:
                continue
        pass
    return

#Onvalidation Methods

def facDataCompletion(form):
    form.vars.f_electricpowerconsumption_network = 1.25
    if chosenUseCase == "blisk":
        form.vars.f_electricpowerconsumption_network = 0
    if chosenUseCase == "agv":
        form.vars.f_paths_requiredwidth = 5

def tecFailDataCompletion(form):
    if chosenUseCase == "agv":
        form.vars.f_feshortterm_ttr = 10/3600

        
#Functions to prefill forms

def agvformfill(form):
    #Facility Data
    form.vars.f_plantarea_total = 1800
    form.vars.f_storagearea = 225
    form.vars.f_reworkarea = 0
    form.vars.f_paths_over5m = 3
    form.vars.f_paths_over35m = 2
    form.vars.f_paths_total = 5
    form.vars.f_electricpowerconsumption_application = 0
    form.vars.f_compressedairconsumption = 0
    form.vars.f_gasconsumption = 0
    form.vars.f_waterconsumption = 0
    #Economic
    form.vars.f_costelectricpower = 0.319
    form.vars.f_costcompressedair = 0.01
    form.vars.f_costgas = 0.01
    form.vars.f_costwater = 0.05
    form.vars.f_energytax = 30
    #Failure Data
    form.vars.f_felongterm_ttr = 1.5
    #Economic Failure Data
    form.vars.f_wagerepairstaff = 20
    form.vars.f_costmaterialrepair = 1000
    form.vars.f_costfinancialcompensation = 5000
    #Process Data
    form.vars.f_productiondaysperyear = 250
    form.vars.f_applicationmaintenance_time = 1
    form.vars.f_applicationoperationtime_planned = 8
    form.vars.f_applicationsetup_number = 1
    form.vars.f_applicationsetup_time = 0.5
    form.vars.f_batchsize = 4
    form.vars.f_personnelbreaktime_planned = 0.5
    form.vars.f_personnelworktimepershift_planned = 7.5
    form.vars.f_shiftsperday = 1
    #Economic Process Data
    form.vars.f_costofapplicationdowntime_planned = 1000
    form.vars.f_costofapplicationdowntime_unplanned = 9000
    form.vars.f_wagesetup = 15
    form.vars.f_wageoperator = 15
    form.vars.f_operator_number = 1
    #Product Data
    form.vars.f_individualizationpercentage = 0
    form.vars.f_inspectedpercentage = 0
    form.vars.f_reworkpercentage = 0
    form.vars.f_reworkduration = 0.1
    form.vars.f_inspectiontime = 0
    form.vars.f_productvariantsonapplication = 1
    form.vars.f_totalproductvariants = 5
    form.vars.f_timetorework = 0.1
    #Economic Product Data
    form.vars.f_sellingprice = 165
    form.vars.f_individualizationadditionalprofit = 0
    form.vars.f_materialcostpart = 120
    form.vars.f_qualitycontrolcostpart = 25
    form.vars.f_disposalcostpart = 0
    form.vars.f_materialcostrework = 50
    form.vars.f_wagerework = 12
    return

def bliskformfill(form):
    #Facility Data
    form.vars.f_plantarea_total = 8100
    form.vars.f_storagearea = 0
    form.vars.f_reworkarea_onepart = 25.3
    form.vars.f_transportarea = 100
    form.vars.f_paths_total = 1
    form.vars.f_electricpowerconsumption_application = 3600
    form.vars.f_electricpowerconsumption_rework = 360
    form.vars.f_compressedairconsumption = 50
    form.vars.f_compressedairconsumption_rework = 25
    form.vars.f_gasconsumption = 0
    form.vars.f_waterconsumption = 5
    #Economic
    form.vars.f_costelectricpower = 0.319
    form.vars.f_costcompressedair = 0.01
    form.vars.f_costgas = 0.01
    form.vars.f_costwater = 0.05
    form.vars.f_energytax = 30
    #Failure Data
    form.vars.f_feaccidents = 0
    form.vars.f_fetoolbreakage_ttr = 2
    #Economic Failure Data
    form.vars.f_wagerepairstaff = 35
    form.vars.f_costmaterialrepair = 0
    form.vars.f_costtoolbreakage = 1000
    form.vars.f_costfinancialcompensation = 0
    #Process Data
    form.vars.f_productiondaysperyear = 250
    form.vars.f_applicationmaintenance_time = 2
    form.vars.f_applicationmaintenance_number = 0.2
    form.vars.f_applicationoperationtime_planned = 16
    form.vars.f_applicationsetup_number = 1
    form.vars.f_applicationsetup_time = 0.5
    form.vars.f_batchsize = 1
    form.vars.f_personnelbreaktime_planned = 0.5
    form.vars.f_personnelworktimepershift_planned = 8
    form.vars.f_shiftsperday = 2
    #Economic Process Data
    form.vars.f_costofapplicationdowntime_planned = 180
    form.vars.f_costofapplicationdowntime_unplanned = 1500
    form.vars.f_wagesetup = 35
    form.vars.f_wageoperator = 35
    form.vars.f_operator_number = 1
    #Product Data
    form.vars.f_individualizationpercentage = 0
    form.vars.f_inspectedpercentage = 100
    form.vars.f_reworkpercentage = 100
    form.vars.f_reworkduration_vibration = 0.01666667
    form.vars.f_reworkduration = 4
    form.vars.f_inspectiontime = 3
    form.vars.f_productvariantsonapplication = 1
    form.vars.f_totalproductvariants = 5
    #Economic Product Data
    form.vars.f_sellingprice = 27500
    form.vars.f_individualizationadditionalprofit = 0
    form.vars.f_materialcostpart = 1500
    form.vars.f_qualitycontrolcostpart = 1000
    form.vars.f_disposalcostpart = 5000
    form.vars.f_materialcostrework = 0
    form.vars.f_wagerework = 35
    return

# -*- coding: utf-8 -*-
from os import path
from gluon.serializers import json
#exec('import applications.%s.modules.evaluation as evaluation' % request.application)
#exec('import applications.%s.modules.fiveGImpact as fiveGImpact' % request.application)
debug = False #Debug variable


f_BTU_inkWh = 0.000293071
kwh_to_co2 = 3/1000
TECinCW = 0.00026
time_interval_signals = 20
#Fetch Chosen Goals
try:
    goals =  db(db.t_goals).select().first().as_dict()
except:
    redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Please choose at least one goal before you continue to the evaluation')))

def isDataMissing():
    necessaryData = getNecessaryInputData()
    for liste in necessaryData.values():
        checkIfRecordsExist(liste)
    for liste in necessaryData.values():
        if liste:
            return True
        pass
    return False
    
if isDataMissing():
    redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Please fill out missing data. You can check what is missing over the menu')))
    pass

################
#General Concept of calculations:
#First, fetch all the necessary data and check which Use Case is chosen
#Then, calculate the base data (the KPIs without 5G effects) by calling calculate<UseCase>Goals function directly
#Update the db for this data and then calculate the 5G effect and subeffects by calling calculate<SubGoal>Impact<UseCase>
#This function takes as argument if latency effects should be considered as well as if 5G effects should be considered
#It sets necessary parameters for the subgoals (e.g. if latency effect, reset availability and reliability) and calls the <UseCase>Goals function again.
################
def calculations():
    ###First check if Goals is empty
    if not isGoalsEmpty(goals):
        redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Please choose at least one goal')))
        return dict()
    #####
    #This Site is used to calculate the 5G values of the inputs
    #####
    ###############
    #For all calculated values:
        #1st row: normal values and direct entries
        #2nd row: 5g values total
        #3rd row: 5g values latency
        #4th row: 5g values availability
        #5th row: 5g values reliability
        #6th row: 5g values communication technology
    ###############
    futureApplication = False
    #Fetch Databases
    try:
        tecProcData = db(db.t_technicalProcessData).select().first().as_dict() #Technical Process Data
    except:
        tecProcData = {}
    if (goals['f_npv'] or goals['f_roi']):
        ecoProcData = db(db.t_economicProcessData).select().first().as_dict() #Economical Process Data
        ecProdData = db(db.t_economicProductData).select().first().as_dict() #Economical Product Data
        ecoFailData = db(db.t_economicFailureData).select().first().as_dict() #Economical Failure Data
    else:
        ecoProcData = {}
        ecProdData = {}
        ecoFailData = {}
    #Necessary due to nature of rows and dicts
    if (db.t_technicalFailureData(1) is None):
        db.t_technicalFailureData.insert(**{'f_feshortterm_ttr':10/3600})
    tecFailData = db(db.t_technicalFailureData).select().first().as_dict() #Technical Failure Data
    try:
        tecProdData = db().select(db.t_technicalProductData.ALL).first().as_dict() #Technical Product Data
    except:
        db.t_technicalProductData.insert(**{})#Insert Empty Row if first Row is missing to evade errors
        tecProdData = db().select(db.t_technicalProductData.ALL).first().as_dict()
    try:
        ecFacData = db().select(db.t_economicFacilitydata.ALL).first().as_dict() #Facility Data
    except:
        ecFacData = {}
    try:
        tecFacData = db().select(db.t_facilitydata.ALL).first().as_dict() #Facility Data
    except:
        tecFacData = {}
    tecFacData.update(ecFacData)
    ##Look up the selected Use Case
    try:
        dictUseCase = db(db.t_applicationselection).select().first().as_dict()
        chosenUseCase = dictUseCase["f_name"]
    except AttributeError: #Switch to default Use Case in case none was chosen
        chosenUseCase = "agv"
    #Fetch all entries made
    if chosenUseCase == "agv":
        #Get the selected 5G Control Tasks
        rowTech = db(db.t_comparetech).select().first()
        try:
            techDict = rowTech.as_dict()
        except AttributeError: #Case if no Loops are defined/Database is empty
            techDict = None
        appMobileRobotsRow = db(db.t_applicationData_mobileRobots).select().first() #Application Data for AGVs
        try:
            appMobileRobotsFutureRow = db(db.t_applicationData_mobileRobots).select()[1]
            futureApplication = True
        except IndexError:
            futureApplication = False
        #appRow = db(db.t_applicationData).select().first() #Application Data
        trackRows = db(db.t_trackSetup).select() #List of the chosen control tasks

        #Calculate Track Data
        accStraightDistance = 0
        accCurveDistance = 0
        accCrossDistance = 0
        accLoadings = 0
        for row in trackRows:
            if row['f_trackType'] == ["Straight"]:
                accStraightDistance += row['f_distance']
            if row['f_trackType'] == ["Curve"]:
                accCurveDistance += row['f_distance']
            if row['f_trackType'] == ["Crossing"]:
                accCrossDistance += row['f_distance']
            if row['f_trackType'] == ["Number of loadings"]:
                accLoadings += row['f_amount']
        appMobileRobotsRow.update(f_loadingsperroute = accLoadings)
        #Calculate TecRobot Data
        #Dummy data, might change later
        if techDict is not None:
            #LTE values
            if techDict['f_lte']:
                availability = 0.999
                reliability = 0.999
                tecFacData['f_paths_requiredwidth'] = 2.5
            #Wifi values
            else:
                reliability = 0.999
                availability = 0.999
                tecFacData['f_paths_requiredwidth'] = 2.5
        #Dummy data if no tech is chosen
        else:
            availability = 0.9990
            reliability = 0.9990
            tecFacData['f_paths_requiredwidth'] = 2.5
        #Overwrite because of possibility to enter own data    
        compareTechData = db().select(db.t_comparetechspecs.ALL).first().as_dict()
        availability = compareTechData['f_availability']
        reliability = compareTechData['f_reliability']
        try:
            #Adding static data, should NOT result in errors
            appMobileRobotsRow.update(f_distancestraight = accStraightDistance)
            appMobileRobotsRow.update(f_distanceturning = accCurveDistance)
            appMobileRobotsRow.update(f_distancecrossing = accCrossDistance)
            appMobileRobotsRow.update(f_distance_total = accCrossDistance+accStraightDistance+accCurveDistance)
            #appMobileRobotsRow.update(f_loadingduration = 300)
            #appMobileRobotsRow.update(f_batteryoperationtimepercharge_planned = 4)
        except:
            redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Application Robot Data Static Data')))
            return dict()
        tecRobData = appMobileRobotsRow.as_dict()
        resultList = calculateAGVGoals(tecProdData,ecProdData,tecProcData, ecoProcData,tecRobData, tecFailData, ecoFailData, tecFacData, techDict, [reliability, availability])
        #Split up result list for overview
        tecProdData = resultList[0]
        ecProdData = resultList[1]
        tecProcData = resultList[2]
        ecoProcData = resultList[3]
        tecRobData = resultList[4]
        tecFailData = resultList[5]
        ecoFailData = resultList[6]
        tecFacData = resultList[7]
        #Calculate economic values
        if (goals['f_roi'] or goals['f_npv'] or goals['f_opexp'] or goals['f_pb']):
            ecoValues_dict = economicCalculator(tecProdData,ecProdData,ecoProcData,ecoFailData,tecFacData, False, chosenUseCase, existCheck = tecRobData['f_existing'])
            if (db.t_ecoValues(1) is None):
                db.t_ecoValues.insert(**ecoValues_dict)
            else:
                db(db.t_ecoValues._id == 1).update(**ecoValues_dict)
            
        #Update UseCase Specific DB Entries
        try:
            db(db.t_applicationData_mobileRobots._id == 1).update(**tecRobData)
        except:
            pass
    elif chosenUseCase == "blisk":
        #Get the selected 5G Control Tasks
        db.t_blisktasks_accumulated.insert(**{'f_millingtime':0}) #Necessary due to accumulated being empty at start
        try:
            tecBliskRoughTask = db(db.t_blisktasks_roughing).select().first().as_dict()
            tecBliskPreTask = db(db.t_blisktasks_prefinish).select().first().as_dict()
            tecBliskBladeTask = db(db.t_blisktasks_bladefinish).select().first().as_dict()
            tecBliskHubTask = db(db.t_blisktasks_hubfinish).select().first().as_dict()
            tecAccumulatedTasks = db(db.t_blisktasks_accumulated).select().first().as_dict()
        except AttributeError: #Case if no Loops are defined/Database is empty
            redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Please first define the Control Tasks ')))
            return dict()
        tecBliskData = db(db.t_bliskData).select().first() #Generell BLISK Data
        
        appBliskData = db(db.t_applicationData_blisk).select().first().as_dict() #Application Data
        reliability = 0
        availability = 0
        tecFacData['f_electricpowerconsumption_network'] = 0
        resultList = calculateBLISKGoals(tecProdData,ecProdData,tecProcData, ecoProcData, appBliskData, tecFailData, ecoFailData, tecFacData, tecBliskData, tecBliskRoughTask, tecBliskPreTask, tecBliskBladeTask,tecBliskHubTask,tecAccumulatedTasks,[reliability, availability])
        #Split up result list for overview
        tecProdData = resultList[0]
        ecProdData = resultList[1]
        tecProcData = resultList[2]
        ecoProcData = resultList[3]
        #appBliskData = resultList[4] #Here for completion but actually not needed
        tecFailData = resultList[5]
        ecoFailData = resultList[6]
        tecFacData = resultList[7]
        #restList[8] tecBliskData not needed
        tecBliskRoughTask = resultList[9]
        tecBliskPreTask = resultList[10]
        tecBliskBladeTask = resultList[11]
        tecBliskHubTask = resultList[12]
        tecAccumulatedTasks = resultList[13]
        ##########Insert updated control loops

        db(db.t_blisktasks_roughing._id == 1).update(**tecBliskRoughTask)
        db(db.t_blisktasks_prefinish._id == 1).update(**tecBliskPreTask)
        db(db.t_blisktasks_bladefinish._id == 1).update(**tecBliskBladeTask)
        db(db.t_blisktasks_hubfinish._id == 1).update(**tecBliskHubTask)
        db(db.t_blisktasks_accumulated._id == 1).update(**tecAccumulatedTasks)

        #Calculate economic values
        if (goals['f_roi'] or goals['f_npv'] or goals['f_opexp'] or goals['f_pb']):
            ecoValues_dict = economicCalculator(tecProdData,ecProdData,ecoProcData,ecoFailData,tecFacData, False, chosenUseCase)
            if (db.t_ecoValues(1) is None):
                db.t_ecoValues.insert(**ecoValues_dict)
            else:
                db(db.t_ecoValues._id == 1).update(**ecoValues_dict)
    #Update the Database Entries
    try:
        if (db.t_technicalProductData(1) is None):
            db(db.t_technicalProductData).insert(**tecProdData)
        else:
            db(db.t_technicalProductData._id == 1).update(**tecProdData)
    except:
        pass
    try:
        if (db.t_economicProductData(1) is None):
            db(db.t_economicProductData).insert(**ecProdData)
        else:
            db(db.t_economicProductData._id == 1).update(**ecProdData)
    except:
        pass
    try:
        if (db.t_economicFailureData(1) is None):
            db(db.t_economicFailureData).insert(**ecoFailData)
        else:
            db(db.t_economicFailureData._id == 1).update(**ecoFailData)
    except:
        pass
    try:
        if (db.t_economicProcessData(1) is None):
            db(db.t_economicProcessData).insert(**ecoProcData)
        else:
            db(db.t_economicProcessData._id == 1).update(**ecoProcData)
    except:
        pass
    try:
        if (db.t_technicalProcessData(1) is None):
            db(db.t_technicalProcessData).insert(**tecProcData)
        else:
            db(db.t_technicalProcessData._id == 1).update(**tecProcData)
    except:
        pass
    try:
        if (db.t_technicalFailureData(1) is None):
            db(db.t_technicalFailureData).insert(**tecFailData)
        else:
            db(db.t_technicalFailureData._id == 1).update(**tecFailData)
    except:
        pass
    try:
        if (db.t_economicFacilitydata(1) is None):
            db(db.t_economicFacilitydata).insert(**tecFacData)
        else:
            db(db.t_economicFacilitydata._id == 1).update(**tecFacData)
    except:
        pass
    try:
        if (db.t_facilitydata(1) is None):
            db(db.t_facilitydata).insert(**tecFacData)
        else:
            db(db.t_facilitydata._id == 1).update(**tecFacData)
    except:
        pass
        ##AGV Use Case applied
    if chosenUseCase == "agv":
        appData = []
        latData = []
        availData = []
        reliData = []
        ictData = []
        appMobileRobotsFutureRow = db(db.t_applicationData_mobileRobots).select()[1]
        appMobileRobotsFutureRow.update(f_distancestraight = tecRobData['f_distancestraight'])
        appMobileRobotsFutureRow.update(f_distanceturning = tecRobData['f_distanceturning'])
        appMobileRobotsFutureRow.update(f_distancecrossing = tecRobData['f_distancecrossing'])
        appMobileRobotsFutureRow.update(f_distance_total = tecRobData['f_distance_total'])
        appMobileRobotsFutureRow.update(f_loadingsperroute = accLoadings)
        tecRobDataFuture = appMobileRobotsFutureRow.as_dict()

        appData = AGV_Impact(tecProdData,ecProdData,tecProcData, ecoProcData,tecRobDataFuture, tecFailData, ecoFailData, tecFacData, techDict)
        latData = calculateLatencyImpactAGV(tecProdData,ecProdData,tecProcData, ecoProcData,tecRobDataFuture, tecFailData, ecoFailData, tecFacData, techDict)
        availData = calculateAvailabilityImpactAGV(tecProdData,ecProdData,tecProcData, ecoProcData,tecRobData, tecFailData, ecoFailData, tecFacData, techDict)
        reliData = calculateReliabilityImpactAGV(tecProdData,ecProdData,tecProcData, ecoProcData,tecRobData, tecFailData, ecoFailData, tecFacData, techDict)
        ictData = calculateICTImpactAGV(tecProdData,ecProdData,tecProcData, ecoProcData,tecRobData, tecFailData, ecoFailData, tecFacData, techDict)
        #Calculate Economic Values and insert into database
    elif chosenUseCase == "blisk":
        appData = []
        latData = []
        availData = []
        reliData = []
        ictData = []
        tecRobDataFuture = {'f_existing': None} #Necessary for economic evaluation, does not influence anything
        appData = BLISK_Impact(tecProdData,ecProdData,tecProcData, ecoProcData,appBliskData, tecFailData, ecoFailData, tecFacData, tecBliskData,tecBliskRoughTask, tecBliskPreTask, tecBliskBladeTask, tecBliskHubTask, tecAccumulatedTasks,fiveG = True)
        latData = calculateLatencyImpactBLISK(tecProdData,ecProdData,tecProcData, ecoProcData,appBliskData, tecFailData, ecoFailData, tecFacData, tecBliskData,tecBliskRoughTask, tecBliskPreTask, tecBliskBladeTask, tecBliskHubTask, tecAccumulatedTasks,fiveG = False)
        availData = calculateAvailabilityImpactBLISK(tecProdData,ecProdData,tecProcData, ecoProcData,appBliskData, tecFailData, ecoFailData, tecFacData, tecBliskData,tecBliskRoughTask, tecBliskPreTask, tecBliskBladeTask, tecBliskHubTask, tecAccumulatedTasks,fiveG = True)
        reliData = calculateReliabilityImpactBLISK(tecProdData,ecProdData,tecProcData, ecoProcData,appBliskData, tecFailData, ecoFailData, tecFacData, tecBliskData,tecBliskRoughTask, tecBliskPreTask, tecBliskBladeTask, tecBliskHubTask, tecAccumulatedTasks,fiveG = True)
        ictData = calculateICTImpactBLISK(tecProdData,ecProdData,tecProcData, ecoProcData,appBliskData, tecFailData, ecoFailData, tecFacData, tecBliskData,tecBliskRoughTask, tecBliskPreTask, tecBliskBladeTask, tecBliskHubTask, tecAccumulatedTasks,fiveG = False)
        #Calculate Economic Values and insert into database
    if (goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        ecoValues_dict_5g = economicCalculator(appData[0],appData[1],appData[3],appData[6],appData[7], True, useCase = chosenUseCase, existCheck = tecRobDataFuture['f_existing'])
        ecoValues_dict_5g_lat = economicCalculator(latData[0],latData[1],latData[3],latData[6],latData[7], useCase = chosenUseCase, existCheck = tecRobDataFuture['f_existing'])
        ecoValues_dict_5g_avail = economicCalculator(availData[0],availData[1],availData[3],availData[6],availData[7], useCase = chosenUseCase, existCheck = tecRobDataFuture['f_existing'])
        ecoValues_dict_5g_reli = economicCalculator(reliData[0],reliData[1],reliData[3],reliData[6],reliData[7], useCase = chosenUseCase, existCheck = tecRobDataFuture['f_existing'])
        ecoValues_dict_5g_ict = economicCalculator(ictData[0],ictData[1],ictData[3],ictData[6],ictData[7], True, useCase = chosenUseCase, existCheck = tecRobDataFuture['f_existing'])
        if (db.t_ecoValues(2) is None):
            db.t_ecoValues.insert(**ecoValues_dict_5g)
            db.t_ecoValues.insert(**ecoValues_dict_5g_lat)
            db.t_ecoValues.insert(**ecoValues_dict_5g_avail)
            db.t_ecoValues.insert(**ecoValues_dict_5g_reli)
            db.t_ecoValues.insert(**ecoValues_dict_5g_ict)
        else:
            db(db.t_ecoValues._id == 2).update(**ecoValues_dict_5g)
            db(db.t_ecoValues._id == 3).update(**ecoValues_dict_5g_lat)
            db(db.t_ecoValues._id == 4).update(**ecoValues_dict_5g_avail)
            db(db.t_ecoValues._id == 5).update(**ecoValues_dict_5g_reli)
            db(db.t_ecoValues._id == 6).update(**ecoValues_dict_5g_ict)
    for data in appData:
        try:
            del data['id']
        except KeyError:
            pass
    for data in latData:
        try:
            del data['id']
        except KeyError:
            pass
    for data in availData:
        try:
            del data['id']
        except KeyError:
            pass
    for data in reliData:
        try:
            del data['id']
        except KeyError:
            pass
    for data in ictData:
        try:
            del data['id']
        except KeyError:
            pass
    if chosenUseCase == "agv":
        if (db.t_applicationData_mobileRobots(2) is None):
            db.t_applicationData_mobileRobots.insert(**appData[4])
            db.t_applicationData_mobileRobots.insert(**latData[4])
            db.t_applicationData_mobileRobots.insert(**availData[4])
            db.t_applicationData_mobileRobots.insert(**reliData[4])
            db.t_applicationData_mobileRobots.insert(**ictData[4])
        else:
            db(db.t_applicationData_mobileRobots._id == 2).update(**appData[4])
            db(db.t_applicationData_mobileRobots._id == 3).update(**latData[4])
            db(db.t_applicationData_mobileRobots._id == 4).update(**availData[4])
            db(db.t_applicationData_mobileRobots._id == 5).update(**reliData[4])
            db(db.t_applicationData_mobileRobots._id == 6).update(**ictData[4])
    if chosenUseCase == "blisk":
        if (db.t_blisktasks_roughing(2) is None):
            db.t_blisktasks_roughing.insert(**appData[9])
            db.t_blisktasks_roughing.insert(**latData[9])
            db.t_blisktasks_roughing.insert(**availData[9])
            db.t_blisktasks_roughing.insert(**reliData[9])
            db.t_blisktasks_roughing.insert(**ictData[9])
        else:
            db(db.t_blisktasks_roughing._id == 2).update(**appData[9])
            db(db.t_blisktasks_roughing._id == 3).update(**latData[9])
            db(db.t_blisktasks_roughing._id == 4).update(**availData[9])
            db(db.t_blisktasks_roughing._id == 5).update(**reliData[9])
            db(db.t_blisktasks_roughing._id == 6).update(**ictData[9])
        if (db.t_blisktasks_prefinish(2) is None):
            db.t_blisktasks_prefinish.insert(**appData[10])
            db.t_blisktasks_prefinish.insert(**latData[10])
            db.t_blisktasks_prefinish.insert(**availData[10])
            db.t_blisktasks_prefinish.insert(**reliData[10])
            db.t_blisktasks_prefinish.insert(**ictData[10])
        else:
            db(db.t_blisktasks_prefinish._id == 2).update(**appData[10])
            db(db.t_blisktasks_prefinish._id == 3).update(**latData[10])
            db(db.t_blisktasks_prefinish._id == 4).update(**availData[10])
            db(db.t_blisktasks_prefinish._id == 5).update(**reliData[10])
            db(db.t_blisktasks_prefinish._id == 6).update(**ictData[10])
        if (db.t_blisktasks_bladefinish(2) is None):
            db.t_blisktasks_bladefinish.insert(**appData[11])
            db.t_blisktasks_bladefinish.insert(**latData[11])
            db.t_blisktasks_bladefinish.insert(**availData[11])
            db.t_blisktasks_bladefinish.insert(**reliData[11])
            db.t_blisktasks_bladefinish.insert(**ictData[11])
        else:
            db(db.t_blisktasks_bladefinish._id == 2).update(**appData[11])
            db(db.t_blisktasks_bladefinish._id == 3).update(**latData[11])
            db(db.t_blisktasks_bladefinish._id == 4).update(**availData[11])
            db(db.t_blisktasks_bladefinish._id == 5).update(**reliData[11])
            db(db.t_blisktasks_bladefinish._id == 6).update(**ictData[11])
        if (db.t_blisktasks_hubfinish(2) is None):
            db.t_blisktasks_hubfinish.insert(**appData[12])
            db.t_blisktasks_hubfinish.insert(**latData[12])
            db.t_blisktasks_hubfinish.insert(**availData[12])
            db.t_blisktasks_hubfinish.insert(**reliData[12])
            db.t_blisktasks_hubfinish.insert(**ictData[12])
        else:
            db(db.t_blisktasks_hubfinish._id == 2).update(**appData[12])
            db(db.t_blisktasks_hubfinish._id == 3).update(**latData[12])
            db(db.t_blisktasks_hubfinish._id == 4).update(**availData[12])
            db(db.t_blisktasks_hubfinish._id == 5).update(**reliData[12])
            db(db.t_blisktasks_hubfinish._id == 6).update(**ictData[12])
        if (db.t_blisktasks_accumulated(2) is None):
            db.t_blisktasks_accumulated.insert(**appData[13])
            db.t_blisktasks_accumulated.insert(**latData[13])
            db.t_blisktasks_accumulated.insert(**availData[13])
            db.t_blisktasks_accumulated.insert(**reliData[13])
            db.t_blisktasks_accumulated.insert(**ictData[13])
        else:
            db(db.t_blisktasks_accumulated._id == 2).update(**appData[13])
            db(db.t_blisktasks_accumulated._id == 3).update(**latData[13])
            db(db.t_blisktasks_accumulated._id == 4).update(**availData[13])
            db(db.t_blisktasks_accumulated._id == 5).update(**reliData[13])
            db(db.t_blisktasks_accumulated._id == 6).update(**ictData[13])
    #If no Use Case is chosen (shouldnt be possible), rows are copied so evaluation will result in no differences
    #2nd row
    if (db.t_technicalProductData(2) is None):
        db.t_technicalProductData.insert(**appData[0])
        #3rd row latency
        db.t_technicalProductData.insert(**latData[0])
        #4th row Availability
        db.t_technicalProductData.insert(**availData[0])
        #5th row Reliability
        db.t_technicalProductData.insert(**reliData[0])
        #6th row ICT
        db.t_technicalProductData.insert(**ictData[0])
    else:
        db(db.t_technicalProductData._id == 2).update(**appData[0])
        db(db.t_technicalProductData._id == 3).update(**latData[0])
        db(db.t_technicalProductData._id == 4).update(**availData[0])
        db(db.t_technicalProductData._id == 5).update(**reliData[0])
        db(db.t_technicalProductData._id == 6).update(**ictData[0])
    #Check if economic goals have been checked, else there would be an error
    if (goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        if (db.t_economicProductData(2) is None):
            db.t_economicProductData.insert(**appData[1])
            db.t_economicProductData.insert(**latData[1])
            db.t_economicProductData.insert(**availData[1])
            db.t_economicProductData.insert(**reliData[1])
            db.t_economicProductData.insert(**ictData[1])
        else:
            db(db.t_economicProductData._id == 2).update(**appData[1])
            db(db.t_economicProductData._id == 3).update(**latData[1])
            db(db.t_economicProductData._id == 4).update(**availData[1])
            db(db.t_economicProductData._id == 5).update(**reliData[1])
            db(db.t_economicProductData._id == 6).update(**ictData[1])
        if (db.t_economicProcessData(2) is None):
            db.t_economicProcessData.insert(**appData[3])
            db.t_economicProcessData.insert(**latData[3])
            db.t_economicProcessData.insert(**availData[3])
            db.t_economicProcessData.insert(**reliData[3])
            db.t_economicProcessData.insert(**ictData[3])
        else:
            db(db.t_economicProcessData._id == 2).update(**appData[3])
            db(db.t_economicProcessData._id == 3).update(**latData[3])
            db(db.t_economicProcessData._id == 4).update(**availData[3])
            db(db.t_economicProcessData._id == 5).update(**reliData[3])
            db(db.t_economicProcessData._id == 6).update(**ictData[3])
        if (db.t_economicFailureData(2) is None):
            db.t_economicFailureData.insert(**appData[6])
            db.t_economicFailureData.insert(**latData[6])
            db.t_economicFailureData.insert(**availData[6])
            db.t_economicFailureData.insert(**reliData[6])
            db.t_economicFailureData.insert(**ictData[6])
        else:
            db(db.t_economicFailureData._id == 2).update(**appData[6])
            db(db.t_economicFailureData._id == 3).update(**latData[6])
            db(db.t_economicFailureData._id == 4).update(**availData[6])
            db(db.t_economicFailureData._id == 5).update(**reliData[6])
            db(db.t_economicFailureData._id == 6).update(**ictData[6])
        if (db.t_economicFacilitydata(2) is None):
            db.t_economicFacilitydata.insert(**appData[7])
            db.t_economicFacilitydata.insert(**latData[7])
            db.t_economicFacilitydata.insert(**availData[7])
            db.t_economicFacilitydata.insert(**reliData[7])
            db.t_economicFacilitydata.insert(**ictData[7])
        else:
            db(db.t_economicFacilitydata._id == 2).update(**appData[7])
            db(db.t_economicFacilitydata._id == 3).update(**latData[7])
            db(db.t_economicFacilitydata._id == 4).update(**availData[7])
            db(db.t_economicFacilitydata._id == 5).update(**reliData[7])
            db(db.t_economicFacilitydata._id == 6).update(**ictData[7])
    if (db.t_technicalProcessData(2) is None):
        db.t_technicalProcessData.insert(**appData[2])
        db.t_technicalProcessData.insert(**latData[2])
        db.t_technicalProcessData.insert(**availData[2])
        db.t_technicalProcessData.insert(**reliData[2])
        db.t_technicalProcessData.insert(**ictData[2])
    else:
        db(db.t_technicalProcessData._id == 2).update(**appData[2])
        db(db.t_technicalProcessData._id == 3).update(**latData[2])
        db(db.t_technicalProcessData._id == 4).update(**availData[2])
        db(db.t_technicalProcessData._id == 5).update(**reliData[2])
        db(db.t_technicalProcessData._id == 6).update(**ictData[2])
    if (db.t_technicalFailureData(2) is None):
        db.t_technicalFailureData.insert(**appData[5])
        db.t_technicalFailureData.insert(**latData[5])
        db.t_technicalFailureData.insert(**availData[5])
        db.t_technicalFailureData.insert(**reliData[5])
        db.t_technicalFailureData.insert(**ictData[5])
    else:
        db(db.t_technicalFailureData._id == 2).update(**appData[5])
        db(db.t_technicalFailureData._id == 3).update(**latData[5])
        db(db.t_technicalFailureData._id == 4).update(**availData[5])
        db(db.t_technicalFailureData._id == 5).update(**reliData[5])
        db(db.t_technicalFailureData._id == 6).update(**ictData[5])
    if (db.t_facilitydata(2) is None):
        db.t_facilitydata.insert(**appData[7])
        db.t_facilitydata.insert(**latData[7])
        db.t_facilitydata.insert(**availData[7])
        db.t_facilitydata.insert(**reliData[7])
        db.t_facilitydata.insert(**ictData[7])
    else:
        db(db.t_facilitydata._id == 2).update(**appData[7])
        db(db.t_facilitydata._id == 3).update(**latData[7])
        db(db.t_facilitydata._id == 4).update(**availData[7])
        db(db.t_facilitydata._id == 5).update(**reliData[7])
        db(db.t_facilitydata._id == 6).update(**ictData[7])
    redirect(URL('ct_results', 'technicalPotential'))
    return dict(url = URL('ct_results', 'technicalPotential'))

def technicalPotential():
    dictUseCase = db(db.t_applicationselection).select().first().as_dict()
    chosenUseCase = dictUseCase["f_name"]
    #Define the dictionaries that will be inserted into the database
    kpiDict = createKPIDict(goals)
    kpiDict_5G = kpiDict.copy()
    kpiDict_5G_lat = kpiDict.copy()
    kpiDict_5G_avail = kpiDict.copy()
    kpiDict_5G_reli = kpiDict.copy()
    kpiDict_5G_ict = kpiDict.copy()
    #Reset KPI DB   
    mainkpi_dict = {}
    mainkpi_dict_labels = {}
    change_dict = {}
    if goals['f_flex']:
        mainkpi_dict["flexibility"] = None
        change_dict["Flexibility"] = None
        mainkpi_dict_labels["flexibility"] = 'Flexibility'
    if goals['f_mob']:
        mainkpi_dict["mobility"] = None
        change_dict["Mobility"] = None
        mainkpi_dict_labels["mobility"] = 'Mobility'
    if goals['f_prod']:
        mainkpi_dict["productivity"] = None
        change_dict["Productivity"] = None
        mainkpi_dict_labels["productivity"] = 'Productivity'
    if goals['f_qual']:
        mainkpi_dict["quality"] = None
        change_dict["Quality"] = None
        mainkpi_dict_labels["quality"] = 'Quality'
    if goals['f_safe']:
        mainkpi_dict["safety"] = None
        change_dict["Safety"] = None
        mainkpi_dict_labels["safety"] = 'Safety'
    if goals['f_sus']:
        mainkpi_dict["sustainability"] = None
        change_dict["Sustainability"] = None
        mainkpi_dict_labels["sustainability"] = 'Sustainability'
    if goals['f_util']:
        mainkpi_dict["utilization"] = None
        change_dict["Utilization"] = None
        mainkpi_dict_labels["utilization"] = 'Utilization'
    mainkpi5g_dict = mainkpi_dict.copy()
    if (not db(db.t_kpi).isempty()):
        db.t_kpi.truncate()
    #Lists that will be passed to the view
    dataset_mainTMP = []
    dataset_flexTMP = []
    dataset_mobTMP = []
    dataset_prodTMP = []
    dataset_qualTMP = []
    dataset_safeTMP = []
    dataset_susTMP = []
    dataset_utilTMP = []
    
    #Flexibility Goal
    if goals['f_flex']:
        pvas = db().select(db.t_technicalProductData.f_productvariantsonapplication)
        pvasVars = [pvas[0].f_productvariantsonapplication,pvas[1].f_productvariantsonapplication,pvas[2].f_productvariantsonapplication,pvas[3].f_productvariantsonapplication,pvas[4].f_productvariantsonapplication,pvas[5].f_productvariantsonapplication]
        tpas = db().select(db.t_technicalProductData.f_totalproductvariants)
        tpasVars = [tpas[0].f_totalproductvariants,tpas[1].f_totalproductvariants,tpas[2].f_totalproductvariants,tpas[3].f_totalproductvariants,tpas[4].f_totalproductvariants,tpas[5].f_totalproductvariants]
        asts = db().select(db.t_technicalProcessData.f_applicationsetup_time)
        astsVars = [asts[0].f_applicationsetup_time,asts[1].f_applicationsetup_time,asts[2].f_applicationsetup_time,asts[3].f_applicationsetup_time,asts[4].f_applicationsetup_time,asts[5].f_applicationsetup_time]
        apts = db().select(db.t_technicalProcessData.f_applicationbusytime_actual)
        aptsVars = [apts[0].f_applicationbusytime_actual,apts[1].f_applicationbusytime_actual,apts[2].f_applicationbusytime_actual,apts[3].f_applicationbusytime_actual,apts[4].f_applicationbusytime_actual,apts[5].f_applicationbusytime_actual]
        #Create lists that contain the calculated KPIs
        flexKpi, flexKpi5G, flexKpi5G_lat, flexKpi5G_avail, flexKpi5G_reli, flexKpi5G_ict  =  goalFlexibility(pvasVars, tpasVars, astsVars, aptsVars)
        flexNames = ['f_product_flexibility', 'f_setup_ratio']
        for i in range(2):
            kpiDict[flexNames[i]] = flexKpi[i]
            kpiDict_5G[flexNames[i]] = flexKpi5G[i]
            kpiDict_5G_lat[flexNames[i]] = flexKpi5G_lat[i]
            kpiDict_5G_avail[flexNames[i]] = flexKpi5G_avail[i]
            kpiDict_5G_reli[flexNames[i]] = flexKpi5G_reli[i]
            kpiDict_5G_ict[flexNames[i]] = flexKpi5G_ict[i]
        mainkpi_dict['flexibility'] =  calculateGoal(flexKpi)
        mainkpi5g_dict['flexibility'] =  calculateGoal(flexKpi5G)
        change_dict['Flexibility'] = [calculateAverage(calculateDeltas(flexKpi,flexKpi5G), True),
                                      calculateAverage(calculateDeltas(flexKpi,flexKpi5G_lat), True),
                                      calculateAverage(calculateDeltas(flexKpi,flexKpi5G_avail), True),
                                      calculateAverage(calculateDeltas(flexKpi,flexKpi5G_reli), True),
                                      calculateAverage(calculateDeltas(flexKpi,flexKpi5G_ict), True)]
        #dataset_flex =  createDataset([kpiDict['f_product_flexibility'], kpiDict['f_setup_ratio']])
        dataset5g_flex =  createDataset([calculateDeltas([kpiDict['f_product_flexibility']],[kpiDict_5G['f_product_flexibility']]),  calculateDeltas([kpiDict['f_setup_ratio']],[kpiDict_5G['f_setup_ratio']])])
        dataset5g_flex_lat =  createDataset([calculateDeltas([kpiDict['f_product_flexibility']],[kpiDict_5G_lat['f_product_flexibility']]),  calculateDeltas([kpiDict['f_setup_ratio']],[kpiDict_5G_lat['f_setup_ratio']])])
        dataset5g_flex_avail =  createDataset([calculateDeltas([kpiDict['f_product_flexibility']],[kpiDict_5G_avail['f_product_flexibility']]),  calculateDeltas([kpiDict['f_setup_ratio']],[kpiDict_5G_avail['f_setup_ratio']])])
        dataset5g_flex_reli =  createDataset([calculateDeltas([kpiDict['f_product_flexibility']],[kpiDict_5G_reli['f_product_flexibility']]),  calculateDeltas([kpiDict['f_setup_ratio']],[kpiDict_5G_reli['f_setup_ratio']])])
        dataset5g_flex_ict =  createDataset([calculateDeltas([kpiDict['f_product_flexibility']],[kpiDict_5G_ict['f_product_flexibility']]),  calculateDeltas([kpiDict['f_setup_ratio']],[kpiDict_5G_ict['f_setup_ratio']])])
        labels_flex = []
        if kpiDict['f_product_flexibility'] is not None:
            labels_flex.append('Product Flexibility (Max)')
        if kpiDict['f_setup_ratio'] is not None:
            labels_flex.append('Setup ratio (Min)')
        dataset_flexTMP.extend([json(labels_flex),json(dataset5g_flex),json(dataset5g_flex_lat),json(dataset5g_flex_avail),json(dataset5g_flex_reli),json(dataset5g_flex_ict)])
        
    #Mobility Goal
    if goals['f_mob']:
        pathssystem = db().select(db.t_facilitydata.f_pathsaccessible)
        pathssystemVars = [pathssystem[0].f_pathsaccessible,pathssystem[1].f_pathsaccessible,pathssystem[2].f_pathsaccessible,pathssystem[3].f_pathsaccessible,pathssystem[4].f_pathsaccessible,pathssystem[5].f_pathsaccessible]
        pathstotal = db().select(db.t_facilitydata.f_paths_total)
        pathstotalVars = [pathstotal[0].f_paths_total,pathstotal[1].f_paths_total,pathstotal[2].f_paths_total,pathstotal[3].f_paths_total,pathstotal[4].f_paths_total,pathstotal[5].f_paths_total]
        tpa = db().select(db.t_facilitydata.f_productionarea)
        tpaVars = [tpa[0].f_productionarea,tpa[1].f_productionarea,tpa[2].f_productionarea,tpa[3].f_productionarea,tpa[4].f_productionarea,tpa[5].f_productionarea]
        ta = db().select(db.t_facilitydata.f_plantarea_total)
        taVars = [ta[0].f_plantarea_total,ta[1].f_plantarea_total,ta[2].f_plantarea_total,ta[3].f_plantarea_total,ta[4].f_plantarea_total,ta[5].f_plantarea_total]
        mobKpis, mobKpis5G, mobKpis5G_lat, mobKpis5G_avail, mobKpis5G_reli, mobKpis5G_ict =  goalMobility(pathssystemVars, pathstotalVars, tpaVars, taVars)
        mobNames = ['f_application_handling_mobility', 'f_space_productivity']
        for i in range(2):
            kpiDict[mobNames[i]] = mobKpis[i]
            kpiDict_5G[mobNames[i]] = mobKpis5G[i]
            kpiDict_5G_lat[mobNames[i]] = mobKpis5G_lat[i]
            kpiDict_5G_avail[mobNames[i]] = mobKpis5G_avail[i]
            kpiDict_5G_reli[mobNames[i]] = mobKpis5G_reli[i]
            kpiDict_5G_ict[mobNames[i]] = mobKpis5G_ict[i]
        mainkpi_dict['mobility'] =  calculateGoal(mobKpis)
        mainkpi5g_dict['mobility'] =  calculateGoal(mobKpis5G)
        change_dict['Mobility'] = [calculateAverage(calculateDeltas(mobKpis,mobKpis5G)),
                                      calculateAverage(calculateDeltas(mobKpis,mobKpis5G_lat)),
                                      calculateAverage(calculateDeltas(mobKpis,mobKpis5G_avail)),
                                      calculateAverage(calculateDeltas(mobKpis,mobKpis5G_reli)),
                                      calculateAverage(calculateDeltas(mobKpis,mobKpis5G_ict))]
        dataset5g_mob =  createDataset([calculateDeltas([kpiDict['f_application_handling_mobility']],[kpiDict_5G['f_application_handling_mobility']]), calculateDeltas([kpiDict['f_space_productivity']],[kpiDict_5G['f_space_productivity']])])
        dataset5g_mob_lat =  createDataset([calculateDeltas([kpiDict['f_application_handling_mobility']],[kpiDict_5G_lat['f_application_handling_mobility']]), calculateDeltas([kpiDict['f_space_productivity']],[kpiDict_5G_lat['f_space_productivity']])])
        dataset5g_mob_avail =  createDataset([calculateDeltas([kpiDict['f_application_handling_mobility']],[kpiDict_5G_avail['f_application_handling_mobility']]), calculateDeltas([kpiDict['f_space_productivity']],[kpiDict_5G_avail['f_space_productivity']])])
        dataset5g_mob_reli =  createDataset([calculateDeltas([kpiDict['f_application_handling_mobility']],[kpiDict_5G_reli['f_application_handling_mobility']]), calculateDeltas([kpiDict['f_space_productivity']],[kpiDict_5G_reli['f_space_productivity']])])
        dataset5g_mob_ict =  createDataset([calculateDeltas([kpiDict['f_application_handling_mobility']],[kpiDict_5G_ict['f_application_handling_mobility']]), calculateDeltas([kpiDict['f_space_productivity']],[kpiDict_5G_ict['f_space_productivity']])])
        labels_mob = []
        if kpiDict['f_application_handling_mobility'] is not None:
            labels_mob.append('Application handling mobility (Max)')
        if kpiDict['f_space_productivity'] is not None:
            labels_mob.append('Space Productivity (Max)')
        dataset_mobTMP.extend([json(labels_mob),json(dataset5g_mob),json(dataset5g_mob_lat),json(dataset5g_mob_avail),json(dataset5g_mob_reli),json(dataset5g_mob_ict)])
        
    #Productivity
    if goals['f_prod']:
        prtpp = db().select(db.t_technicalProcessData.f_runtimeperpart_planned)
        prtppVars = [prtpp[0].f_runtimeperpart_planned,prtpp[1].f_runtimeperpart_planned,prtpp[2].f_runtimeperpart_planned,prtpp[3].f_runtimeperpart_planned,prtpp[4].f_runtimeperpart_planned,prtpp[5].f_runtimeperpart_planned]
        pq = db().select(db.t_technicalProductData.f_producedquantity_actual)
        pqVars = [pq[0].f_producedquantity_actual,pq[1].f_producedquantity_actual,pq[2].f_producedquantity_actual,pq[3].f_producedquantity_actual,pq[4].f_producedquantity_actual,pq[5].f_producedquantity_actual]
        pabt = db().select(db.t_technicalProcessData.f_applicationbusytime_planned)
        pabtVars = [pabt[0].f_applicationbusytime_planned,pabt[1].f_applicationbusytime_planned,pabt[2].f_applicationbusytime_planned,pabt[3].f_applicationbusytime_planned,pabt[4].f_applicationbusytime_planned,pabt[5].f_applicationbusytime_planned]
        pwt = db().select(db.t_technicalProcessData.f_personnelworktime_actual)
        pwtVars = [pwt[0].f_personnelworktime_actual,pwt[1].f_personnelworktime_actual,pwt[2].f_personnelworktime_actual,pwt[3].f_personnelworktime_actual,pwt[4].f_personnelworktime_actual,pwt[5].f_personnelworktime_actual]
        pat = db().select(db.t_technicalProcessData.f_personnelattendancetime_planned)
        patVars = [pat[0].f_personnelattendancetime_planned,pat[1].f_personnelattendancetime_planned,pat[2].f_personnelattendancetime_planned,pat[3].f_personnelattendancetime_planned,pat[4].f_personnelattendancetime_planned,pat[5].f_personnelattendancetime_planned]
        apts = db().select(db.t_technicalProcessData.f_applicationbusytime_actual)
        aptsVars = [apts[0].f_applicationbusytime_actual,apts[1].f_applicationbusytime_actual,apts[2].f_applicationbusytime_actual,apts[3].f_applicationbusytime_actual,apts[4].f_applicationbusytime_actual,apts[5].f_applicationbusytime_actual]
        prodKpis, prodKpis5G, prodKpis5G_lat, prodKpis5G_avail, prodKpis5G_reli, prodKpis5G_ict =  goalProductivity(prtppVars, pqVars, pabtVars, pwtVars, patVars, aptsVars)
        prodNames = ['f_effectiveness', 'f_throughput_ratio', 'f_worker_efficiency']
        for i in range(3):
            kpiDict[prodNames[i]] = prodKpis[i]
            kpiDict_5G[prodNames[i]] = prodKpis5G[i]
            kpiDict_5G_lat[prodNames[i]] = prodKpis5G_lat[i]
            kpiDict_5G_avail[prodNames[i]] = prodKpis5G_avail[i]
            kpiDict_5G_reli[prodNames[i]] = prodKpis5G_reli[i]
            kpiDict_5G_ict[prodNames[i]] = prodKpis5G_ict[i]
        mainkpi_dict['productivity'] =  calculateGoal(prodKpis)
        mainkpi5g_dict['productivity'] =  calculateGoal(prodKpis5G)
        change_dict['Productivity'] = [calculateAverage(calculateDeltas(prodKpis,prodKpis5G)),
                                      calculateAverage(calculateDeltas(prodKpis,prodKpis5G_lat)),
                                      calculateAverage(calculateDeltas(prodKpis,prodKpis5G_avail)),
                                      calculateAverage(calculateDeltas(prodKpis,prodKpis5G_reli)),
                                      calculateAverage(calculateDeltas(prodKpis,prodKpis5G_ict))]
        dataset5g_prod =  createDataset([calculateDeltas([kpiDict['f_effectiveness']],[kpiDict_5G['f_effectiveness']]), calculateDeltas([kpiDict['f_throughput_ratio']],[kpiDict_5G['f_throughput_ratio']]), calculateDeltas([kpiDict['f_worker_efficiency']],[kpiDict_5G['f_worker_efficiency']])])
        dataset5g_prod_lat =  createDataset([calculateDeltas([kpiDict['f_effectiveness']],[kpiDict_5G_lat['f_effectiveness']]), calculateDeltas([kpiDict['f_throughput_ratio']],[kpiDict_5G_lat['f_throughput_ratio']]), calculateDeltas([kpiDict['f_worker_efficiency']],[kpiDict_5G_lat['f_worker_efficiency']])])
        dataset5g_prod_avail =  createDataset([calculateDeltas([kpiDict['f_effectiveness']],[kpiDict_5G_avail['f_effectiveness']]), calculateDeltas([kpiDict['f_throughput_ratio']],[kpiDict_5G_avail['f_throughput_ratio']]), calculateDeltas([kpiDict['f_worker_efficiency']],[kpiDict_5G_avail['f_worker_efficiency']])])
        dataset5g_prod_reli =  createDataset([calculateDeltas([kpiDict['f_effectiveness']],[kpiDict_5G_reli['f_effectiveness']]), calculateDeltas([kpiDict['f_throughput_ratio']],[kpiDict_5G_reli['f_throughput_ratio']]), calculateDeltas([kpiDict['f_worker_efficiency']],[kpiDict_5G_reli['f_worker_efficiency']])])
        dataset5g_prod_ict =  createDataset([calculateDeltas([kpiDict['f_effectiveness']],[kpiDict_5G_ict['f_effectiveness']]), calculateDeltas([kpiDict['f_throughput_ratio']],[kpiDict_5G_ict['f_throughput_ratio']]), calculateDeltas([kpiDict['f_worker_efficiency']],[kpiDict_5G_ict['f_worker_efficiency']])])
        labels_prod = []
        if kpiDict['f_effectiveness'] is not None:
            labels_prod.append('Effectiveness (Max)')
        if kpiDict['f_throughput_ratio'] is not None:
            labels_prod.append('Throughput ratio (Max)')
        if kpiDict['f_worker_efficiency'] is not None:
            labels_prod.append('Worker Efficiency (Max)')
        dataset_prodTMP.extend([json(labels_prod),json(dataset5g_prod),json(dataset5g_prod_lat),json(dataset5g_prod_avail),json(dataset5g_prod_reli),json(dataset5g_prod_ict)])
        
    #Quality Goal
    if goals['f_qual']:
        ftgq = db().select(db.t_technicalProductData.f_firsttimegoodquantity)
        ftgqVars = [ftgq[0].f_firsttimegoodquantity,ftgq[1].f_firsttimegoodquantity,ftgq[2].f_firsttimegoodquantity,ftgq[3].f_firsttimegoodquantity,ftgq[4].f_firsttimegoodquantity,ftgq[5].f_firsttimegoodquantity]
        iq = db().select(db.t_technicalProductData.f_inspectedquantity)
        iqVars = [iq[0].f_inspectedquantity,iq[1].f_inspectedquantity,iq[2].f_inspectedquantity,iq[3].f_inspectedquantity,iq[4].f_inspectedquantity,iq[5].f_inspectedquantity]
        gq = db().select(db.t_technicalProductData.f_goodquantity)
        gqVars = [gq[0].f_goodquantity,gq[1].f_goodquantity,gq[2].f_goodquantity,gq[3].f_goodquantity,gq[4].f_goodquantity,gq[5].f_goodquantity]
        pq = db().select(db.t_technicalProductData.f_producedquantity_actual)
        pqVars = [pq[0].f_producedquantity_actual,pq[1].f_producedquantity_actual,pq[2].f_producedquantity_actual,pq[3].f_producedquantity_actual,pq[4].f_producedquantity_actual,pq[5].f_producedquantity_actual]
        rq = db().select(db.t_technicalProductData.f_timetorework_total)
        rqVars = [rq[0].f_timetorework_total,rq[1].f_timetorework_total,rq[2].f_timetorework_total,rq[3].f_timetorework_total,rq[4].f_timetorework_total,rq[5].f_timetorework_total]
        sq = db().select(db.t_technicalProductData.f_scrapquantity)
        sqVars = [sq[0].f_scrapquantity,sq[1].f_scrapquantity,sq[2].f_scrapquantity,sq[3].f_scrapquantity,sq[4].f_scrapquantity,sq[5].f_scrapquantity]
        twt = db().select(db.t_technicalProcessData.f_totalworktime)
        twtVars = [twt[0].f_totalworktime,twt[1].f_totalworktime,twt[2].f_totalworktime,twt[3].f_totalworktime,twt[4].f_totalworktime,twt[5].f_totalworktime]
        qualKpis, qualKpis5G, qualKpis5G_lat, qualKpis5G_avail, qualKpis5G_reli, qualKpis5G_ict =  goalQuality(ftgqVars, iqVars, gqVars, pqVars, rqVars, sqVars, twtVars)
        qualNames = ['f_first_pass_yield','f_quality_ratio','f_rework_ratio','f_scrap_ratio']
        for i in range(4):
            kpiDict[qualNames[i]] = qualKpis[i]
            kpiDict_5G[qualNames[i]] = qualKpis5G[i]
            kpiDict_5G_lat[qualNames[i]] = qualKpis5G_lat[i]
            kpiDict_5G_avail[qualNames[i]] = qualKpis5G_avail[i]
            kpiDict_5G_reli[qualNames[i]] = qualKpis5G_reli[i]
            kpiDict_5G_ict[qualNames[i]] = qualKpis5G_ict[i]
        mainkpi_dict['quality'] =  calculateGoal(qualKpis)
        mainkpi5g_dict['quality'] =  calculateGoal(qualKpis5G)
        change_dict['Quality'] = [calculateAverageQuality(calculateDeltas(qualKpis,qualKpis5G)),
                                      calculateAverageQuality(calculateDeltas(qualKpis,qualKpis5G_lat)),
                                      calculateAverageQuality(calculateDeltas(qualKpis,qualKpis5G_avail)),
                                      calculateAverageQuality(calculateDeltas(qualKpis,qualKpis5G_reli)),
                                      calculateAverageQuality(calculateDeltas(qualKpis,qualKpis5G_ict))]
        dataset5g_qual =  createDataset([calculateDeltas([kpiDict['f_first_pass_yield']],[kpiDict_5G['f_first_pass_yield']]), calculateDeltas([kpiDict['f_quality_ratio']],[kpiDict_5G['f_quality_ratio']]), calculateDeltas([kpiDict['f_rework_ratio']],[kpiDict_5G['f_rework_ratio']]), calculateDeltas([kpiDict['f_scrap_ratio']],[kpiDict_5G['f_scrap_ratio']])])
        dataset5g_qual_lat =  createDataset([calculateDeltas([kpiDict['f_first_pass_yield']],[kpiDict_5G_lat['f_first_pass_yield']]), calculateDeltas([kpiDict['f_quality_ratio']],[kpiDict_5G_lat['f_quality_ratio']]), calculateDeltas([kpiDict['f_rework_ratio']],[kpiDict_5G_lat['f_rework_ratio']]), calculateDeltas([kpiDict['f_scrap_ratio']],[kpiDict_5G_lat['f_scrap_ratio']])])
        dataset5g_qual_avail =  createDataset([calculateDeltas([kpiDict['f_first_pass_yield']],[kpiDict_5G_avail['f_first_pass_yield']]), calculateDeltas([kpiDict['f_quality_ratio']],[kpiDict_5G_avail['f_quality_ratio']]), calculateDeltas([kpiDict['f_rework_ratio']],[kpiDict_5G_avail['f_rework_ratio']]), calculateDeltas([kpiDict['f_scrap_ratio']],[kpiDict_5G_avail['f_scrap_ratio']])])
        dataset5g_qual_reli =  createDataset([calculateDeltas([kpiDict['f_first_pass_yield']],[kpiDict_5G_reli['f_first_pass_yield']]), calculateDeltas([kpiDict['f_quality_ratio']],[kpiDict_5G_reli['f_quality_ratio']]), calculateDeltas([kpiDict['f_rework_ratio']],[kpiDict_5G_reli['f_rework_ratio']]), calculateDeltas([kpiDict['f_scrap_ratio']],[kpiDict_5G_reli['f_scrap_ratio']])])
        dataset5g_qual_ict =  createDataset([calculateDeltas([kpiDict['f_first_pass_yield']],[kpiDict_5G_ict['f_first_pass_yield']]), calculateDeltas([kpiDict['f_quality_ratio']],[kpiDict_5G_ict['f_quality_ratio']]), calculateDeltas([kpiDict['f_rework_ratio']],[kpiDict_5G_ict['f_rework_ratio']]), calculateDeltas([kpiDict['f_scrap_ratio']],[kpiDict_5G_ict['f_scrap_ratio']])])
        labels_qual = []
        if kpiDict['f_first_pass_yield'] is not None:
            labels_qual.append('First Pass Yield (Max)')
        if kpiDict['f_quality_ratio'] is not None:
            labels_qual.append('Quality ratio (Max)')
        if kpiDict['f_rework_ratio'] is not None:
            labels_qual.append('Rework ratio (Min)')
        if kpiDict['f_scrap_ratio'] is not None:
            labels_qual.append('Scrap ratio (Min)')
        dataset_qualTMP.extend([json(labels_qual),json(dataset5g_qual),json(dataset5g_qual_lat),json(dataset5g_qual_avail),json(dataset5g_qual_reli),json(dataset5g_qual_ict)])
    #Safety Goal
    if goals['f_safe']:
        acc = db().select(db.t_technicalFailureData.f_feaccidents)
        accVars = [acc[0].f_feaccidents,acc[1].f_feaccidents,acc[2].f_feaccidents,acc[3].f_feaccidents,acc[4].f_feaccidents,acc[5].f_feaccidents]
        pwt = db().select(db.t_technicalProcessData.f_personnelworktime_actual)
        pwtVars = [pwt[0].f_personnelworktime_actual,pwt[1].f_personnelworktime_actual,pwt[2].f_personnelworktime_actual,pwt[3].f_personnelworktime_actual,pwt[4].f_personnelworktime_actual,pwt[5].f_personnelworktime_actual]
        apts = db().select(db.t_technicalProcessData.f_applicationbusytime_actual)
        aptsVars = [apts[0].f_applicationbusytime_actual,apts[1].f_applicationbusytime_actual,apts[2].f_applicationbusytime_actual,apts[3].f_applicationbusytime_actual,apts[4].f_applicationbusytime_actual,apts[5].f_applicationbusytime_actual]
        fe = db().select(db.t_technicalFailureData.f_failureevents_total)
        feVars = [fe[0].f_failureevents_total,fe[1].f_failureevents_total,fe[2].f_failureevents_total,fe[3].f_failureevents_total,fe[4].f_failureevents_total,fe[5].f_failureevents_total]
        ttr = db().select(db.t_technicalFailureData.f_failureevents_ttr_total)
        ttrVars = [ttr[0].f_failureevents_ttr_total,ttr[1].f_failureevents_ttr_total,ttr[2].f_failureevents_ttr_total,ttr[3].f_failureevents_ttr_total,ttr[4].f_failureevents_ttr_total,ttr[5].f_failureevents_ttr_total]
        safeKpis, safeKpis5G, safeKpis5G_lat, safeKpis5G_avail, safeKpis5G_reli, safeKpis5G_ict =  goalSafety(accVars, pwtVars, aptsVars, feVars, ttrVars)
        safeNames = ['f_accident_ratio', 'f_mean_time_between_failures','f_mean_time_to_repair']
        for i in range(3):
            kpiDict[safeNames[i]] = safeKpis[i]
            kpiDict_5G[safeNames[i]] = safeKpis5G[i]
            kpiDict_5G_lat[safeNames[i]] = safeKpis5G_lat[i]
            kpiDict_5G_avail[safeNames[i]] = safeKpis5G_avail[i]
            kpiDict_5G_reli[safeNames[i]] = safeKpis5G_reli[i]
            kpiDict_5G_ict[safeNames[i]] = safeKpis5G_ict[i]
        mainkpi_dict['safety'] =  calculateGoal(safeKpis)
        mainkpi5g_dict['safety'] =  calculateGoal(safeKpis5G)
        change_dict['Safety'] = [calculateAverageSafety(calculateDeltas(safeKpis,safeKpis5G)),
                                      calculateAverageSafety(calculateDeltas(safeKpis,safeKpis5G_lat)),
                                      calculateAverageSafety(calculateDeltas(safeKpis,safeKpis5G_avail)),
                                      calculateAverageSafety(calculateDeltas(safeKpis,safeKpis5G_reli)),
                                      calculateAverageSafety(calculateDeltas(safeKpis,safeKpis5G_ict))]
        dataset5g_safe =  createDataset([calculateDeltas([kpiDict['f_accident_ratio']],[kpiDict_5G['f_accident_ratio']]), calculateDeltas([kpiDict['f_mean_time_between_failures']],[kpiDict_5G['f_mean_time_between_failures']]), calculateDeltas([kpiDict['f_mean_time_to_repair']],[kpiDict_5G['f_mean_time_to_repair']])])
        dataset5g_safe_lat =  createDataset([calculateDeltas([kpiDict['f_accident_ratio']],[kpiDict_5G_lat['f_accident_ratio']]), calculateDeltas([kpiDict['f_mean_time_between_failures']],[kpiDict_5G_lat['f_mean_time_between_failures']]), calculateDeltas([kpiDict['f_mean_time_to_repair']],[kpiDict_5G_lat['f_mean_time_to_repair']])])
        dataset5g_safe_avail =  createDataset([calculateDeltas([kpiDict['f_accident_ratio']],[kpiDict_5G_avail['f_accident_ratio']]), calculateDeltas([kpiDict['f_mean_time_between_failures']],[kpiDict_5G_avail['f_mean_time_between_failures']]), calculateDeltas([kpiDict['f_mean_time_to_repair']],[kpiDict_5G_avail['f_mean_time_to_repair']])])
        dataset5g_safe_reli =  createDataset([calculateDeltas([kpiDict['f_accident_ratio']],[kpiDict_5G_reli['f_accident_ratio']]), calculateDeltas([kpiDict['f_mean_time_between_failures']],[kpiDict_5G_reli['f_mean_time_between_failures']]), calculateDeltas([kpiDict['f_mean_time_to_repair']],[kpiDict_5G_reli['f_mean_time_to_repair']])])
        dataset5g_safe_ict =  createDataset([calculateDeltas([kpiDict['f_accident_ratio']],[kpiDict_5G_ict['f_accident_ratio']]), calculateDeltas([kpiDict['f_mean_time_between_failures']],[kpiDict_5G_ict['f_mean_time_between_failures']]), calculateDeltas([kpiDict['f_mean_time_to_repair']],[kpiDict_5G_ict['f_mean_time_to_repair']])])
        labels_safe = []
        if kpiDict['f_accident_ratio'] is not None and kpiDict['f_accident_ratio'] != 1:
            labels_safe.append('Accident Ratio (Min)')
        if kpiDict['f_mean_time_between_failures'] is not None:
            labels_safe.append('Mean Time between Failures Ratio (Max)')
        if kpiDict['f_mean_time_to_repair'] is not None:
            labels_safe.append('Mean Time to Repair Ratio (Min)')
        dataset_safeTMP.extend([json(labels_safe),json(dataset5g_safe),json(dataset5g_safe_lat),json(dataset5g_safe_avail),json(dataset5g_safe_reli),json(dataset5g_safe_ict)])
    #Sustainability Goal
    if goals['f_sus']:
        pq = db().select(db.t_technicalProductData.f_producedquantity_actual)
        pqVars = [pq[0].f_producedquantity_actual,pq[1].f_producedquantity_actual,pq[2].f_producedquantity_actual,pq[3].f_producedquantity_actual,pq[4].f_producedquantity_actual,pq[5].f_producedquantity_actual]
        tec = db().select(db.t_facilitydata.f_energyconsumption_total)
        tecVars = [tec[0].f_energyconsumption_total,tec[1].f_energyconsumption_total,tec[2].f_energyconsumption_total,tec[3].f_energyconsumption_total,tec[4].f_energyconsumption_total,tec[5].f_energyconsumption_total]
        cac = db().select(db.t_facilitydata.f_compressedairconsumption_total)
        cacVars = [cac[0].f_compressedairconsumption_total,cac[1].f_compressedairconsumption_total,cac[2].f_compressedairconsumption_total,cac[3].f_compressedairconsumption_total,cac[4].f_compressedairconsumption_total,cac[5].f_compressedairconsumption_total]
        epc = db().select(db.t_facilitydata.f_electricpowerconsumption_total)
        epcVars = [epc[0].f_electricpowerconsumption_total,epc[1].f_electricpowerconsumption_total,epc[2].f_electricpowerconsumption_total,epc[3].f_electricpowerconsumption_total,epc[4].f_electricpowerconsumption_total,epc[5].f_electricpowerconsumption_total]
        gc = db().select(db.t_facilitydata.f_gasconsumption_total)
        gcVars = [gc[0].f_gasconsumption_total,gc[1].f_gasconsumption_total,gc[2].f_gasconsumption_total,gc[3].f_gasconsumption_total,gc[4].f_gasconsumption_total,gc[5].f_gasconsumption_total]
        wc = db().select(db.t_facilitydata.f_waterconsumption_total)
        wcVars = [wc[0].f_waterconsumption_total,wc[1].f_waterconsumption_total,wc[2].f_waterconsumption_total,wc[3].f_waterconsumption_total,wc[4].f_waterconsumption_total,wc[5].f_waterconsumption_total]
        susKpis, susKpis5G, susKpis5G_lat, susKpis5G_avail, susKpis5G_reli, susKpis5G_ict =  goalSustainability(pqVars, tecVars, cacVars, epcVars, gcVars, wcVars)
        susNames = ['f_compressed_air_consumption_ratio', 'f_electric_power_consumption_ratio','f_gas_consumption_ratio','f_water_consumption_ratio']#,'f_carbon_weight']
        for i in range(4):
            kpiDict[susNames[i]] = susKpis[i]
            kpiDict_5G[susNames[i]] = susKpis5G[i]
            kpiDict_5G_lat[susNames[i]] = susKpis5G_lat[i]
            kpiDict_5G_avail[susNames[i]] = susKpis5G_avail[i]
            kpiDict_5G_reli[susNames[i]] = susKpis5G_reli[i]
            kpiDict_5G_ict[susNames[i]] = susKpis5G_ict[i]
        mainkpi_dict['sustainability'] =  calculateGoal(susKpis)
        mainkpi5g_dict['sustainability'] =  calculateGoal(susKpis5G)
        change_dict['Sustainability'] = [-calculateAverage(calculateDeltas(susKpis,susKpis5G)),
                                      -calculateAverage(calculateDeltas(susKpis,susKpis5G_lat)),
                                      -calculateAverage(calculateDeltas(susKpis,susKpis5G_avail)),
                                      -calculateAverage(calculateDeltas(susKpis,susKpis5G_reli)),
                                      -calculateAverage(calculateDeltas(susKpis,susKpis5G_ict))]
        dataset5g_sus =  createDataset([calculateDeltas([kpiDict['f_compressed_air_consumption_ratio']],[kpiDict_5G['f_compressed_air_consumption_ratio']]), calculateDeltas([kpiDict['f_electric_power_consumption_ratio']],[kpiDict_5G['f_electric_power_consumption_ratio']]), calculateDeltas([kpiDict['f_gas_consumption_ratio']],[kpiDict_5G['f_gas_consumption_ratio']]), calculateDeltas([kpiDict['f_water_consumption_ratio']],[kpiDict_5G['f_water_consumption_ratio']])])
        dataset5g_sus_lat =  createDataset([calculateDeltas([kpiDict['f_compressed_air_consumption_ratio']],[kpiDict_5G_lat['f_compressed_air_consumption_ratio']]), calculateDeltas([kpiDict['f_electric_power_consumption_ratio']],[kpiDict_5G_lat['f_electric_power_consumption_ratio']]), calculateDeltas([kpiDict['f_gas_consumption_ratio']],[kpiDict_5G_lat['f_gas_consumption_ratio']]), calculateDeltas([kpiDict['f_water_consumption_ratio']],[kpiDict_5G_lat['f_water_consumption_ratio']])])
        dataset5g_sus_avail =  createDataset([calculateDeltas([kpiDict['f_compressed_air_consumption_ratio']],[kpiDict_5G_avail['f_compressed_air_consumption_ratio']]), calculateDeltas([kpiDict['f_electric_power_consumption_ratio']],[kpiDict_5G_avail['f_electric_power_consumption_ratio']]), calculateDeltas([kpiDict['f_gas_consumption_ratio']],[kpiDict_5G_avail['f_gas_consumption_ratio']]), calculateDeltas([kpiDict['f_water_consumption_ratio']],[kpiDict_5G_avail['f_water_consumption_ratio']])])
        dataset5g_sus_reli =  createDataset([calculateDeltas([kpiDict['f_compressed_air_consumption_ratio']],[kpiDict_5G_reli['f_compressed_air_consumption_ratio']]), calculateDeltas([kpiDict['f_electric_power_consumption_ratio']],[kpiDict_5G_reli['f_electric_power_consumption_ratio']]), calculateDeltas([kpiDict['f_gas_consumption_ratio']],[kpiDict_5G_reli['f_gas_consumption_ratio']]), calculateDeltas([kpiDict['f_water_consumption_ratio']],[kpiDict_5G_reli['f_water_consumption_ratio']])])
        dataset5g_sus_ict =  createDataset([calculateDeltas([kpiDict['f_compressed_air_consumption_ratio']],[kpiDict_5G_ict['f_compressed_air_consumption_ratio']]), calculateDeltas([kpiDict['f_electric_power_consumption_ratio']],[kpiDict_5G_ict['f_electric_power_consumption_ratio']]), calculateDeltas([kpiDict['f_gas_consumption_ratio']],[kpiDict_5G_ict['f_gas_consumption_ratio']]), calculateDeltas([kpiDict['f_water_consumption_ratio']],[kpiDict_5G_ict['f_water_consumption_ratio']])])
        labels_sus = []
        if kpiDict['f_compressed_air_consumption_ratio'] is not None:
            labels_sus.append('Compressed Air Consumption ratio (Min)')
        if kpiDict['f_electric_power_consumption_ratio'] is not None:
            labels_sus.append('Electric power Consumption ratio (Min)')
        if kpiDict['f_gas_consumption_ratio'] is not None:
            labels_sus.append('Gas Consumption ratio (Min)')
        if kpiDict['f_water_consumption_ratio'] is not None:
            labels_sus.append('Water Consumption ratio (Min)')
        dataset_susTMP.extend([json(labels_sus),json(dataset5g_sus),json(dataset5g_sus_lat),json(dataset5g_sus_avail),json(dataset5g_sus_reli),json(dataset5g_sus_ict)])
    #Utilization Goal
    if goals['f_util']:
        apts = db().select(db.t_technicalProcessData.f_applicationbusytime_actual)
        aptsVars = [apts[0].f_applicationbusytime_actual,apts[1].f_applicationbusytime_actual,apts[2].f_applicationbusytime_actual,apts[3].f_applicationbusytime_actual,apts[4].f_applicationbusytime_actual,apts[5].f_applicationbusytime_actual]
        pabt = db().select(db.t_technicalProcessData.f_applicationbusytime_planned)
        pabtVars = [pabt[0].f_applicationbusytime_planned,pabt[1].f_applicationbusytime_planned,pabt[2].f_applicationbusytime_planned,pabt[3].f_applicationbusytime_planned,pabt[4].f_applicationbusytime_planned,pabt[5].f_applicationbusytime_planned]
        apt = db().select(db.t_technicalProcessData.f_applicationproductiontime_actual)
        aptVars = [apt[0].f_applicationproductiontime_actual,apt[1].f_applicationproductiontime_actual,apt[2].f_applicationproductiontime_actual,apt[3].f_applicationproductiontime_actual,apt[4].f_applicationproductiontime_actual,apt[5].f_applicationproductiontime_actual]
        adt = db().select(db.t_technicalProcessData.f_applicationdowntime_actual)
        adtVars = [adt[0].f_applicationdowntime_actual,adt[1].f_applicationdowntime_actual,adt[2].f_applicationdowntime_actual,adt[3].f_applicationdowntime_actual,adt[4].f_applicationdowntime_actual,adt[5].f_applicationdowntime_actual]
        utilKpis, utilKpis5G, utilKpis5G_lat, utilKpis5G_avail, utilKpis5G_reli, utilKpis5G_ict =  goalUtilization(aptsVars, pabtVars, aptVars, adtVars)
        utilNames = ['f_allocation_efficiency', 'f_availability','f_technical_efficiency','f_utilization_efficiency']
        for i in range(4):
            kpiDict[utilNames[i]] = utilKpis[i]
            kpiDict_5G[utilNames[i]] = utilKpis5G[i]
            kpiDict_5G_lat[utilNames[i]] = utilKpis5G_lat[i]
            kpiDict_5G_avail[utilNames[i]] = utilKpis5G_avail[i]
            kpiDict_5G_reli[utilNames[i]] = utilKpis5G_reli[i]
            kpiDict_5G_ict[utilNames[i]] = utilKpis5G_ict[i]
        mainkpi_dict['utilization'] =  calculateGoal(utilKpis)
        mainkpi5g_dict['utilization'] =  calculateGoal(utilKpis5G)
        change_dict['Utilization'] = [calculateAverage(calculateDeltas(utilKpis,utilKpis5G)),
                                      calculateAverage(calculateDeltas(utilKpis,utilKpis5G_lat)),
                                      calculateAverage(calculateDeltas(utilKpis,utilKpis5G_avail)),
                                      calculateAverage(calculateDeltas(utilKpis,utilKpis5G_reli)),
                                      calculateAverage(calculateDeltas(utilKpis,utilKpis5G_ict))]
        dataset5g_util =  createDataset([calculateDeltas([kpiDict['f_allocation_efficiency']],[kpiDict_5G['f_allocation_efficiency']]), calculateDeltas([kpiDict['f_availability']],[kpiDict_5G['f_availability']]), calculateDeltas([kpiDict['f_technical_efficiency']],[kpiDict_5G['f_technical_efficiency']]), calculateDeltas([kpiDict['f_utilization_efficiency']],[kpiDict_5G['f_utilization_efficiency']])])
        dataset5g_util_lat =  createDataset([calculateDeltas([kpiDict['f_allocation_efficiency']],[kpiDict_5G_lat['f_allocation_efficiency']]), calculateDeltas([kpiDict['f_availability']],[kpiDict_5G_lat['f_availability']]), calculateDeltas([kpiDict['f_technical_efficiency']],[kpiDict_5G_lat['f_technical_efficiency']]), calculateDeltas([kpiDict['f_utilization_efficiency']],[kpiDict_5G_lat['f_utilization_efficiency']])])
        dataset5g_util_avail =  createDataset([calculateDeltas([kpiDict['f_allocation_efficiency']],[kpiDict_5G_avail['f_allocation_efficiency']]), calculateDeltas([kpiDict['f_availability']],[kpiDict_5G_avail['f_availability']]), calculateDeltas([kpiDict['f_technical_efficiency']],[kpiDict_5G_avail['f_technical_efficiency']]), calculateDeltas([kpiDict['f_utilization_efficiency']],[kpiDict_5G_avail['f_utilization_efficiency']])])
        dataset5g_util_reli =  createDataset([calculateDeltas([kpiDict['f_allocation_efficiency']],[kpiDict_5G_reli['f_allocation_efficiency']]), calculateDeltas([kpiDict['f_availability']],[kpiDict_5G_reli['f_availability']]), calculateDeltas([kpiDict['f_technical_efficiency']],[kpiDict_5G_reli['f_technical_efficiency']]), calculateDeltas([kpiDict['f_utilization_efficiency']],[kpiDict_5G_reli['f_utilization_efficiency']])])
        dataset5g_util_ict =  createDataset([calculateDeltas([kpiDict['f_allocation_efficiency']],[kpiDict_5G_ict['f_allocation_efficiency']]), calculateDeltas([kpiDict['f_availability']],[kpiDict_5G_ict['f_availability']]), calculateDeltas([kpiDict['f_technical_efficiency']],[kpiDict_5G_ict['f_technical_efficiency']]), calculateDeltas([kpiDict['f_utilization_efficiency']],[kpiDict_5G_ict['f_utilization_efficiency']])])
        labels_util = []
        if kpiDict['f_allocation_efficiency'] is not None:
            labels_util.append('Allocation Efficiency (Max)')
        if kpiDict['f_availability'] is not None:
            labels_util.append('Availability (Max)')
        if kpiDict['f_technical_efficiency'] is not None:
            labels_util.append('Technical Efficiency (Max)')
        if kpiDict['f_utilization_efficiency'] is not None:
            labels_util.append('Utilization Efficiency (Max)')
        dataset_utilTMP.extend([json(labels_util),json(dataset5g_util),json(dataset5g_util_lat),json(dataset5g_util_avail),json(dataset5g_util_reli),json(dataset5g_util_ict)])
    #Insert dictionaries into database
    if (db.t_kpi(1) is None):
        db.t_kpi.insert(**kpiDict)
        db.t_kpi.insert(**kpiDict_5G)
        db.t_kpi.insert(**kpiDict_5G_lat)
        db.t_kpi.insert(**kpiDict_5G_avail)
        db.t_kpi.insert(**kpiDict_5G_reli)
        db.t_kpi.insert(**kpiDict_5G_ict)
    else:
        db(db.t_kpi._id == 0).update(**kpiDict)
        db(db.t_kpi._id == 1).update(**kpiDict_5G)
        db(db.t_kpi._id == 2).update(**kpiDict_5G_lat)
        db(db.t_kpi._id == 3).update(**kpiDict_5G_avail)
        db(db.t_kpi._id == 4).update(**kpiDict_5G_reli)
        db(db.t_kpi._id == 5).update(**kpiDict_5G_ict)
    #Prepare mainkpi for Graphs
    dataset = []
    labels = []
    dataset_5g = []
    labels_5g = []
    for k in mainkpi_dict:
        dataset.append(mainkpi_dict[k])
        labels.append(mainkpi_dict_labels[k])
    for k in mainkpi5g_dict:
        dataset_5g.append(mainkpi5g_dict[k])
        labels_5g.append(k)
    economicValues = db(db.t_ecoValues).select().as_list()
    ecoLabels = []
    nextUrl = URL('ct_results','economicPotential')
    if not (goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        nextUrl = URL('ct_results','showResults')
    if debug:
        nextUrl = URL('ct_results','testDB')
    backUrl = URL('ct_start','mainScreen')
    for x in range(11):
        ecoLabels.append(str(x))
    if chosenUseCase == "agv":
        #Dummy line for the check for existing and new agvs
        if db(db.t_applicationData_mobileRobots).select().first()['f_existing']:
            net_investment = 1000
    try:
        dictStatusQuo = db(db.t_statusquo).select().first().as_dict()
    except:
        dictStatusQuo = {}
    dictforView = dict(nextUrl = nextUrl, backUrl = backUrl,mainkpi_dict_labels=mainkpi_dict_labels,change_data = json(change_dict), dataset_main = [json(dataset),json(labels),json(dataset_5g)],economicValues = [json(ecoLabels),economicValues], dictStatusQuo = json(dictStatusQuo))
    if goals['f_flex']:
        dictforView["dataset_flex"] = dataset_flexTMP
    if goals['f_mob']:
        dictforView["dataset_mob"] = dataset_mobTMP
    if goals['f_prod']:
        dictforView["dataset_prod"] = dataset_prodTMP
    if goals['f_qual']:
        dictforView["dataset_qual"] = dataset_qualTMP
    if goals['f_safe']:
        dictforView["dataset_safe"] = dataset_safeTMP
    if goals['f_sus']:
        dictforView["dataset_sus"] = dataset_susTMP
    if goals['f_util']:
        dictforView["dataset_util"] = dataset_utilTMP
    if not (goals['f_flex'] or goals['f_mob'] or goals['f_prod'] or goals['f_qual'] or goals['f_sus'] or goals['f_safe'] or goals['f_util']):
        redirect(nextUrl)
    return dictforView

def economicPotential():
    try:
        investData = db(db.t_investmentdata).select().first().as_dict()
        lifetime = investData['f_lifetime'] + 1
    except:
        lifetime = 11
    if (goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        ecoProcData = db(db.t_economicProcessData).select().as_list() #Economical Process Data
        ecProdData = db(db.t_economicProductData).select().as_list() #Economical Product Data
        ecoFailData = db(db.t_economicFailureData).select().as_list() #Economical Failure Data
    else:
        ecoProcData = []
        ecProdData = []
        ecoFailData = []
    try:
        ecFacData = db().select(db.t_economicFacilitydata).as_list() #Facility Data
    except:
        ecFacData = []
    try:
        economicValues = db(db.t_ecoValues).select().as_list()
    except:
        economicValues = []
    ecoLabels = []
    for x in range(lifetime):
        ecoLabels.append(str(x))
    try:
        dictStatusQuo = db(db.t_statusquo).select().first().as_dict()
    except:
        dictStatusQuo = {}
    nextUrl = URL('ct_results','showResults') #vars = dict(investment=investment))
    backUrl = URL('ct_results', 'technicalPotential')
    return dict(nextUrl = nextUrl,economicValues = [json(ecoLabels),economicValues], dictStatusQuo = json(dictStatusQuo), lifetime=lifetime, backUrl = backUrl)

def testDB():
    formList = []
    formList.append(SQLFORM.grid(db.t_economicProcessData))
    formList.append(SQLFORM.grid(db.t_economicProductData))
    formList.append(SQLFORM.grid(db.t_economicFailureData))
    formList.append(SQLFORM.grid(db.t_economicFacilitydata))
    formList.append(SQLFORM.grid(db.t_facilitydata))
    formList.append(SQLFORM.grid(db.t_ecoValues))
    formList.append(SQLFORM.grid(db.t_applicationData_mobileRobots))
    formList.append(SQLFORM.grid(db.t_applicationData_blisk))
    formList.append(SQLFORM.grid(db.t_technicalFailureData))
    formList.append(SQLFORM.grid(db.t_technicalProcessData))
    formList.append(SQLFORM.grid(db.t_technicalProductData))
    formList.append(SQLFORM.grid(db.t_comparetech))
    formList.append(SQLFORM.grid(db.t_comparetechspecs))
    formList.append(SQLFORM.grid(db.t_kpi))
    formList.append(SQLFORM.grid(db.t_bliskData))
    formList.append(SQLFORM.grid(db.t_blisktasks_bladefinish))
    formList.append(SQLFORM.grid(db.t_blisktasks_hubfinish))
    formList.append(SQLFORM.grid(db.t_blisktasks_prefinish))
    formList.append(SQLFORM.grid(db.t_blisktasks_roughing))
    formList.append(SQLFORM.grid(db.t_blisktasks_accumulated))
    formList.append(SQLFORM.grid(db.t_investmentdata))
    formList.append(SQLFORM.grid(db.t_statusquo))
    formList.append(SQLFORM.grid(db.t_goals))
    return dict(forms = formList)

def showResults():
    try:
        economicValues = db(db.t_ecoValues).select().as_list()
    except:
        economicValues = {}
    if (goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        cf_no5g = economicValues[0]['f_cashflow_yearly']
        cf_5g = economicValues[1]['f_cashflow_yearly']
        npvno5g10 = economicValues[0]['f_npv_lifetime']
        npv5g10 = economicValues[1]['f_npv_lifetime']
        opex_no5g = economicValues[0]['f_opex_total']
        opex_5g = economicValues[1]['f_opex_total']
        opexp_no5g = economicValues[0]['f_opex_per_product']
        opexp_5g = economicValues[1]['f_opex_per_product']
        rev_no5g = economicValues[0]['f_revenues']
        rev_5g = economicValues[1]['f_revenues']
        roi_no5g = economicValues[0]['f_roi_lifetime']
        roi_5g = economicValues[1]['f_roi_lifetime']
        pb_no5g = economicValues[0]['f_payback_period']
        pb_5g = economicValues[1]['f_payback_period']
        invest_no5g = economicValues[0]['f_investment']
        invest_5g = economicValues[1]['f_investment']
        t_profit_no5g = economicValues[0]['f_total_profit']
        t_profit_5g = economicValues[1]['f_total_profit']
    else:
        cf_no5g = 0
        cf_5g = 0
        npvno5g10 = 0
        npv5g10 = 0
        opex_no5g = 0
        opex_5g = 0
        opexp_no5g = 0
        opexp_5g = 0
        rev_no5g = 0
        rev_5g = 0
        roi_no5g = 0
        roi_5g = 0
        pb_no5g = 0
        pb_5g = 0
        if cf_5g <= 0:
            pb_5g = '&#8734'
        invest_no5g = 0
        invest_5g = 0
        t_profit_no5g = 0
        t_profit_5g = 0
    kpiList=db(db.t_kpi).select().as_list()
    kpiNo5G = kpiList[0]
    kpi5G = kpiList[1]
    backUrl = URL('ct_results', 'economicPotential')
    if not(goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        backUrl = URL('ct_results', 'technicalPotential')
    return dict(kpiNo5G=kpiNo5G,kpi5G=kpi5G, npv=[npvno5g10,npv5g10], cashflow = [cf_no5g,cf_5g], opexperP = [opexp_no5g,opexp_5g], revenues = [rev_no5g,rev_5g], roi = [roi_no5g,roi_5g], payback=[pb_no5g,pb_5g], invest=[invest_no5g,invest_5g], profit=[t_profit_no5g,t_profit_5g], backUrl = backUrl)


#!/usr/bin/env python
# -*- coding: utf-8 -*-
#From Evaluation

kpis = []

def createKPIDict(goals):
    kpiDict = {}
    if goals['f_flex']:
        kpiDict.update({'f_product_flexibility':None, 'f_setup_ratio': None})
    if goals['f_mob']:
        kpiDict.update({'f_application_handling_mobility':None, 'f_space_productivity':None})
    if goals['f_prod'] :
        kpiDict.update({'f_effectiveness':None, 'f_throughput_ratio':None, 'f_worker_efficiency':None})
    if goals['f_qual']:
        kpiDict.update({'f_first_pass_yield':None, 'f_quality_ratio':None, 'f_rework_ratio':None, 'f_scrap_ratio':None})
    if goals['f_safe']:
        kpiDict.update({'f_accident_ratio':None, 'f_mean_time_between_failures':None, 'f_mean_time_to_repair':None})
    if goals['f_sus']:
        kpiDict.update({'f_compressed_air_consumption_ratio':None, 'f_electric_power_consumption_ratio':None, 'f_gas_consumption_ratio':None, 'f_water_consumption_ratio':None})#, 'f_carbon_weight':None})
    if goals['f_util']:
        kpiDict.update({'f_allocation_efficiency':None, 'f_availability':None, 'f_technical_efficiency':None, 'f_utilization_efficiency':None})
    return kpiDict


def calculateGoal(goalArray):
    newArray = [x for x in goalArray if x is not None]
    arrCount = len(newArray)
    goalVariable = 0
    for goalItem in newArray:
        itemWeight = 1 / arrCount
        goalItemVar = goalItem * itemWeight
        goalVariable = goalVariable + goalItemVar

    return goalVariable

def calculateDeltas(kpiNormal, kpiFiveG):
    deltaList = []
    for x in range(len(kpiNormal)):
        if kpiNormal[x] == 0:
            deltaList.append(0)
        elif kpiNormal[x] is None:
            deltaList.append(0)
        else:
            deltaList.append((kpiFiveG[x]-kpiNormal[x])/kpiNormal[x])
    return deltaList

def calculatePercChange(goalNormal, goalFiveG):
    if goalNormal == 0:
        return goalFiveG
    else:
        change = (goalFiveG/goalNormal + 1)*100
        return change

def calculateAverageSafety(goalFiveG):
    calc = goalFiveG[1]
    calc -= goalFiveG[0]
    calc -= goalFiveG[2]
    return calc/len(goalFiveG)*100
    
def calculateAverage(goalFiveG, neg=False):
    calc = goalFiveG[0]
    for x in range(1,len(goalFiveG)):
        if neg:
            calc -= goalFiveG[x]
        else:
            calc += goalFiveG[x]
    return calc/len(goalFiveG)*100

#Average function for Quality Goal
def calculateAverageQuality(goalFiveG):
    calc = goalFiveG[0] + goalFiveG[1]
    for x in range(2,4):
        calc -= goalFiveG[x]
    return calc/len(goalFiveG)*100

#Arguments should always be of the form [Numberwihtout5G, Numberwith5G]
def goalFlexibility(pvas, tpas, asts, apts):
    returnList = []
    returnList_5g = []
    returnList_5g_lat = []
    returnList_5g_avail = []
    returnList_5g_reli = []
    returnList_5g_ict = []
    #Machine Flexibility
    if tpas[0] == 0:
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
    else:
        calc = calculateSubGoal(pvas,tpas,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
    #Setup Ratio
    if apts[0] == 0:
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
    else:
        calc = calculateSubGoal(asts,apts,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
    return returnList, returnList_5g, returnList_5g_lat, returnList_5g_avail, returnList_5g_reli, returnList_5g_ict

def goalMobility(pathssystem, pathstotal, tpa, ta):
    returnList = []
    returnList_5g = []
    returnList_5g_lat = []
    returnList_5g_avail = []
    returnList_5g_reli = []
    returnList_5g_ict = []
    #Material Handling Mobility
    if pathstotal[0] == 0:
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
    else:
        calc = calculateSubGoal(pathssystem,pathstotal,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
    #Space Productivity
    if ta[0] == 0:
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
    else:
        calc = calculateSubGoal(tpa,ta,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
    return returnList, returnList_5g, returnList_5g_lat, returnList_5g_avail, returnList_5g_reli, returnList_5g_ict

def goalProductivity(prtpp, pq, apt, pwt, pat, aet):
    returnList = []
    returnList_5g = []
    returnList_5g_lat = []
    returnList_5g_avail = []
    returnList_5g_reli = []
    returnList_5g_ict = []
    #Effectiveness
    if pq[0] == 0:
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
    else:
        calc = calculateSubGoal(prtpp,pq,'*')
        calc = calculateSubGoal(calc,apt,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
    #Throughput Ratio
    if aet[0] == 0:
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
    else:
        calc = calculateSubGoal(pq,aet,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
    #Worker Efficiency
    if pat[0] == 0:
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
    else:
        calc = calculateSubGoal(pwt,pat,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
    return returnList, returnList_5g, returnList_5g_lat, returnList_5g_avail, returnList_5g_reli, returnList_5g_ict

def goalQuality(ftgq, iq, gq, pq, rt, sq, twt): #FirstTimeGoodQuantity, Inspected Quantity, Good Quantity, Produced Quantity, Rework Time, Scrap Quanity, Total Work Time
    returnList = []
    returnList_5g = []
    returnList_5g_lat = []
    returnList_5g_avail = []
    returnList_5g_reli = []
    returnList_5g_ict = []
    #First Pass Yield
    if iq[0] == 0:
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
    else:
        calc = calculateSubGoal(ftgq,iq,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
    #Quality Ratio, Rework Ratio and Scrap Ratio
    if (pq[0] == 0):
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
    else:
        calc = calculateSubGoal(gq,pq,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
    #Rework Ratio
    if (twt[0] == 0):
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
    else:
        calc = calculateSubGoal(rt,twt,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
    #Scrap Ratio
    if (pq[0] == 0):
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
    else:
        calc = calculateSubGoal(sq,pq,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
    return returnList, returnList_5g, returnList_5g_lat, returnList_5g_avail, returnList_5g_reli, returnList_5g_ict

def goalSafety(acc, pat, tbf, fe, ttr): #Accidents, Personnel Work Time, App Busy Time Actual, Failure Events, Total Time to Repair
    returnList = []
    returnList_5g = []
    returnList_5g_lat = []
    returnList_5g_avail = []
    returnList_5g_reli = []
    returnList_5g_ict = []
    fe = [x+1 for x in fe]
    feaabt = []
    for x in range(len(fe)):
        feaabt.append(fe[x]*tbf[x])
    #Accident Ratio, Mean Time between Failures, Mean Time to Repair
    if pat[0] == 0:
        returnList.append(1)
        returnList_5g.append(1)
        returnList_5g_lat.append(1)
        returnList_5g_avail.append(1)
        returnList_5g_reli.append(1)
        returnList_5g_ict.append(1)
    elif tbf[0] == 0:
        calc = calculateSubGoal(acc,pat,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
        returnList.append(1)
        returnList_5g.append(1)
        returnList_5g_lat.append(1)
        returnList_5g_avail.append(1)
        returnList_5g_reli.append(1)
        returnList_5g_ict.append(1)
        returnList.append(1)
        returnList_5g.append(1)
        returnList_5g_lat.append(1)
        returnList_5g_avail.append(1)
        returnList_5g_reli.append(1)
        returnList_5g_ict.append(1)
    else:
        calc = calculateSubGoal(acc,pat,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
        calc = calculateSubGoal(tbf,feaabt,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
        calc = calculateSubGoal(ttr,feaabt,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
    return returnList, returnList_5g, returnList_5g_lat, returnList_5g_avail, returnList_5g_reli, returnList_5g_ict

def goalSustainability(pq, tec, cac, epc, gc, wc):
    TECinCW = 0.00026
    returnList = []
    returnList_5g = []
    returnList_5g_lat = []
    returnList_5g_avail = []
    returnList_5g_reli = []
    returnList_5g_ict = []
    #Compressed Air, Electric Power, Water, Carbon Weight Consumption
    if pq[0] == 0:
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
    else:
        calc = calculateSubGoal(cac,pq,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
        
        calc = calculateSubGoal(epc,pq,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])

        calc = calculateSubGoal(gc,pq,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])

        calc = calculateSubGoal(wc,pq,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
    return returnList, returnList_5g, returnList_5g_lat, returnList_5g_avail, returnList_5g_reli, returnList_5g_ict

def goalUtilization(abt, pabt, apt, adt):
    returnList = []
    returnList_5g = []
    returnList_5g_lat = []
    returnList_5g_avail = []
    returnList_5g_reli = []
    returnList_5g_ict = []
    #Allocation Efficiency, Availability
    if pabt[0] == 0:
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
    else:
        calc = calculateSubGoal(abt,pabt,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])

        calc = calculateSubGoal(apt,pabt,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
    #Technical Efficiency
    if (apt[0] + adt[0]) == 0:
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
    else:
        calc = calculateSubGoal(apt,adt,'+')
        calc = calculateSubGoal(apt,calc,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
    #Utilization
    if abt[0] == 0:
        returnList.append(None)
        returnList_5g.append(None)
        returnList_5g_lat.append(None)
        returnList_5g_avail.append(None)
        returnList_5g_reli.append(None)
        returnList_5g_ict.append(None)
    else:
        calc = calculateSubGoal(apt,abt,'/')
        returnList.append(calc[0])
        returnList_5g.append(calc[1])
        returnList_5g_lat.append(calc[2])
        returnList_5g_avail.append(calc[3])
        returnList_5g_reli.append(calc[4])
        returnList_5g_ict.append(calc[5])
        utilization_efficiency = apt[0] / abt[0]
        utilization_efficiency_5g = apt[1] / abt[1]
    return returnList, returnList_5g, returnList_5g_lat, returnList_5g_avail, returnList_5g_reli, returnList_5g_ict

def calculateSubGoal(arg1, arg2, operator):
    returnList = []
    if operator == '+':
        for i in range(6):
            returnList.append(arg1[i]+arg2[i])
    elif operator == '-':
        for i in range(6):
            returnList.append(arg1[i]-arg2[i])
    elif operator == '*':
        for i in range(6):
            returnList.append(arg1[i]*arg2[i])
    elif operator == '/':
        for i in range(6):
            try:
                returnList.append(arg1[i]/arg2[i])
            except ZeroDivisionError:
                returnList.append(0)
    return returnList

def createDataset(argList):
    dataset = []
    for data in argList:
        if data is not None:
            dset = [x*100 for x in data]
            dataset.append(dset)
    return dataset

#!/usr/bin/env python
# -*- coding: utf-8 -*-
#From fiveGImpact

#This module is used to calculate the effect of 5G on a Use Case. Usually, a use case function gets dicts containing all of the relevant data and changes those that are affected by the 5G in the use Case

def EffectOf5G(kpi, percentage):
    kpi_5g = kpi*(1+(percentage*0.01))
    return kpi_5g

####################################
#Subtasks for AGV Use Case

def AGV_Impact(tecProdData1,ecProdData1,tecProcData1, ecoProcData1,tecRobData1, tecFailData1, ecoFailData1, tecFacData1, techDict):
    tecProdData = tecProdData1.copy()
    ecProdData = ecProdData1.copy()
    tecProcData = tecProcData1.copy()
    ecoProcData = ecoProcData1.copy()
    tecRobData = tecRobData1.copy()
    tecFailData = tecFailData1.copy()
    ecoFailData = ecoFailData1.copy()
    tecFacData = tecFacData1.copy()
    #Base data is the calculated data without 5g
    #Update all dependent values
#######################################
    #Dummy data, might change later
    #tecRobData['f_speed_average'] = 1.1
    #tecRobData['f_speedcurve_average'] = 0.55
    #tecRobData['f_speedcrossing_average'] = 0.775
    if tecRobData['f_loadingduration'] is None:
        tecRobData['f_loadingduration'] = 300
    availability = 0.99999
    reliability = 0.99999
    tecFacData['f_paths_requiredwidth'] = 2
    tecFacData['f_electricpowerconsumption_network'] = 1
    
    resultList = calculateAGVGoals(tecProdData,ecProdData,tecProcData, ecoProcData,tecRobData, tecFailData, ecoFailData, tecFacData, techDict, [reliability, availability])
    return resultList

def calculateLatencyImpactAGV(tecProdData1,ecProdData1,tecProcData1, ecoProcData1,tecRobData1, tecFailData1, ecoFailData1, tecFacData1, techDict):
    #Copy data dictionaries
    tecProdData = tecProdData1.copy()
    ecProdData = ecProdData1.copy()
    tecProcData = tecProcData1.copy()
    ecoProcData = ecoProcData1.copy()
    tecRobData = tecRobData1.copy()
    tecFailData = tecFailData1.copy()
    ecoFailData = ecoFailData1.copy()
    tecFacData = tecFacData1.copy()
    #Change relevant data
    if techDict is not None:
        #LTE values
        if techDict['f_lte']:
            availability = 0.9990
            reliability = 0.9990
            tecFacData['f_paths_requiredwidth'] = 2.5
        #Wifi values
        else:
            availability = 0.9950
            reliability = 0.9950
            tecFacData['f_paths_requiredwidth'] = 2.5
    availability = db().select(db.t_comparetechspecs.ALL).first().as_dict()['f_availability']
    reliability = db().select(db.t_comparetechspecs.ALL).first().as_dict()['f_reliability']
    
    #Calculate results
    resultList = calculateAGVGoals(tecProdData,ecProdData,tecProcData, ecoProcData,tecRobData, tecFailData, ecoFailData, tecFacData, techDict, [reliability, availability])
    return resultList

def calculateAvailabilityImpactAGV(tecProdData1,ecProdData1,tecProcData1, ecoProcData1,tecRobData1, tecFailData1, ecoFailData1, tecFacData1, techDict):
    #Base data is the calculated data without 5g
    tecProdData = tecProdData1.copy()
    ecProdData = ecProdData1.copy()
    tecProcData = tecProcData1.copy()
    ecoProcData = ecoProcData1.copy()
    tecRobData = tecRobData1.copy()
    tecFailData = tecFailData1.copy()
    ecoFailData = ecoFailData1.copy()
    tecFacData = tecFacData1.copy()
    #ICT Influence, only Availability is based on 5G
    availability = 0.99999
    tecRobData['f_loadingduration'] = db(db.t_applicationData_mobileRobots).select().first()['f_loadingduration']
    if techDict is not None:
        #LTE values
        if techDict['f_lte']:
            reliability = 0.999
            tecFacData['f_paths_requiredwidth'] = 2.5
        #Wifi values
        else:
            reliability = 0.995
            tecFacData['f_paths_requiredwidth'] = 2.5
    #Dummy data if no tech is chosen
    else:
        reliability = 0.999
        tecFacData['f_paths_requiredwidth'] = 2.5
    reliability = db().select(db.t_comparetechspecs.ALL).first().as_dict()['f_reliability']

    resultList = calculateAGVGoals(tecProdData,ecProdData,tecProcData, ecoProcData,tecRobData, tecFailData, ecoFailData, tecFacData, techDict, [reliability, availability])
    return resultList

def calculateReliabilityImpactAGV(tecProdData1,ecProdData1,tecProcData1, ecoProcData1,tecRobData1, tecFailData1, ecoFailData1, tecFacData1, techDict):
    tecProdData = tecProdData1.copy()
    ecProdData = ecProdData1.copy()
    tecProcData = tecProcData1.copy()
    ecoProcData = ecoProcData1.copy()
    tecRobData = tecRobData1.copy()
    tecFailData = tecFailData1.copy()
    ecoFailData = ecoFailData1.copy()
    tecFacData = tecFacData1.copy()
    #Base data is the calculated data without 5g
    reliability = 0.99999
    tecRobData['f_loadingduration'] = db(db.t_applicationData_mobileRobots).select().first()['f_loadingduration']
    if techDict is not None:
        #LTE values
        if techDict['f_lte']:
            availability = 0.999
            tecFacData['f_paths_requiredwidth'] = 2.5
        #Wifi values
        else:
            availability = 0.995
            tecFacData['f_paths_requiredwidth'] = 2.5
    #Dummy data if no tech is chosen
    else:
        availability = 0.999
        tecFacData['f_paths_requiredwidth'] = 2.5
    availability = db().select(db.t_comparetechspecs.ALL).first().as_dict()['f_availability']

    resultList = calculateAGVGoals(tecProdData,ecProdData,tecProcData, ecoProcData,tecRobData, tecFailData, ecoFailData, tecFacData, techDict, [reliability, availability])
    return resultList

def calculateICTImpactAGV(tecProdData1,ecProdData1,tecProcData1, ecoProcData1,tecRobData1, tecFailData1, ecoFailData1, tecFacData1, techDict):
    tecProdData = tecProdData1.copy()
    ecProdData = ecProdData1.copy()
    tecProcData = tecProcData1.copy()
    ecoProcData = ecoProcData1.copy()
    tecRobData = tecRobData1.copy()
    tecFailData = tecFailData1.copy()
    ecoFailData = ecoFailData1.copy()
    tecFacData = tecFacData1.copy()
    tecRobData['f_loadingduration'] = db(db.t_applicationData_mobileRobots).select().first()['f_loadingduration']
    tecFacData['f_paths_requiredwidth'] = 2
    tecFacData['f_electricpowerconsumption_network'] = 1
    if techDict is not None:
        #LTE values
        if techDict['f_lte']:
            availability = db().select(db.t_comparetechspecs.ALL).first().as_dict()['f_availability']
            reliability = db().select(db.t_comparetechspecs.ALL).first().as_dict()['f_reliability']
        #Wifi values
        else:
            availability = db().select(db.t_comparetechspecs.ALL).first().as_dict()['f_availability']
            reliability = db().select(db.t_comparetechspecs.ALL).first().as_dict()['f_reliability']
    #Dummy data if no tech is chosen
    else:
        availability = 0.999
        reliability = 0.999
    
    resultList = calculateAGVGoals(tecProdData,ecProdData,tecProcData, ecoProcData,tecRobData, tecFailData, ecoFailData, tecFacData, techDict, [reliability, availability])
    return resultList

####################################
#Subtasks for BLISK Use Case
def BLISK_Impact(tecProdData1,ecProdData1,tecProcData1, ecoProcData1,tecAppData1, tecFailData1, ecoFailData1, tecFacData1, tecBliskData1,tecBliskRoughTask1, tecBliskPreTask1, tecBliskBladeTask1, tecBliskHubTask1, tecAccumulatedTasks1,fiveG = False):
    tecProdData = tecProdData1.copy()
    ecProdData = ecProdData1.copy()
    tecProcData = tecProcData1.copy()
    ecoProcData = ecoProcData1.copy()
    tecAppData = tecAppData1.copy()
    tecFailData = tecFailData1.copy()
    ecoFailData = ecoFailData1.copy()
    tecFacData = tecFacData1.copy()
    tecBliskData = tecBliskData1.copy()
    tecBliskRoughTask = tecBliskRoughTask1.copy()
    tecBliskPreTask = tecBliskPreTask1.copy()
    tecBliskBladeTask = tecBliskBladeTask1.copy()
    tecBliskHubTask = tecBliskHubTask1.copy()
    tecAccumulatedTasks = tecAccumulatedTasks1.copy()
    availability = 0.99999
    reliability = 0.99999
    #Base data is the calculated data without 5g
    tecFacData['f_electricpowerconsumption_network'] = 1
    resultList = calculateBLISKGoals(tecProdData,ecProdData,tecProcData, ecoProcData,tecAppData, tecFailData, ecoFailData, tecFacData, tecBliskData, tecBliskRoughTask, tecBliskPreTask, tecBliskBladeTask, tecBliskHubTask, tecAccumulatedTasks,[reliability, availability], fiveG, latency = True)
    return resultList

def calculateLatencyImpactBLISK(tecProdData1,ecProdData1,tecProcData1, ecoProcData1,tecAppData1, tecFailData1, ecoFailData1, tecFacData1, tecBliskData1,tecBliskRoughTask1, tecBliskPreTask1, tecBliskBladeTask1, tecBliskHubTask1, tecAccumulatedTasks1,fiveG = False):
    tecProdData = tecProdData1.copy()
    ecProdData = ecProdData1.copy()
    tecProcData = tecProcData1.copy()
    ecoProcData = ecoProcData1.copy()
    tecAppData = tecAppData1.copy()
    tecFailData = tecFailData1.copy()
    ecoFailData = ecoFailData1.copy()
    tecFacData = tecFacData1.copy()
    tecBliskData = tecBliskData1.copy()
    tecBliskRoughTask = tecBliskRoughTask1.copy()
    tecBliskPreTask = tecBliskPreTask1.copy()
    tecBliskBladeTask = tecBliskBladeTask1.copy()
    tecBliskHubTask = tecBliskHubTask1.copy()
    tecAccumulatedTasks = tecAccumulatedTasks1.copy()
    availability = 0
    reliability = 0
    latency = True
    #Base data is the calculated data without 5g

    resultList = calculateBLISKGoals(tecProdData,ecProdData,tecProcData, ecoProcData,tecAppData, tecFailData, ecoFailData, tecFacData, tecBliskData, tecBliskRoughTask, tecBliskPreTask, tecBliskBladeTask, tecBliskHubTask, tecAccumulatedTasks,[reliability, availability], fiveG, latency = True)
    return resultList

def calculateAvailabilityImpactBLISK(tecProdData1,ecProdData1,tecProcData1, ecoProcData1,tecAppData1, tecFailData1, ecoFailData1, tecFacData1, tecBliskData1,tecBliskRoughTask1, tecBliskPreTask1, tecBliskBladeTask1, tecBliskHubTask1, tecAccumulatedTasks1,fiveG = False):
    #Base data is the calculated data without 5g
    tecProdData = tecProdData1.copy()
    ecProdData = ecProdData1.copy()
    tecProcData = tecProcData1.copy()
    ecoProcData = ecoProcData1.copy()
    tecAppData = tecAppData1.copy()
    tecFailData = tecFailData1.copy()
    ecoFailData = ecoFailData1.copy()
    tecFacData = tecFacData1.copy()
    tecBliskData = tecBliskData1.copy()
    tecBliskRoughTask = tecBliskRoughTask1.copy()
    tecBliskPreTask = tecBliskPreTask1.copy()
    tecBliskBladeTask = tecBliskBladeTask1.copy()
    tecBliskHubTask = tecBliskHubTask1.copy()
    tecAccumulatedTasks = tecAccumulatedTasks1.copy()
    availability = 0.99999
    reliability = 0.99999
    #ICT Influence, only Availability is based on 5G

    resultList = calculateBLISKGoals(tecProdData,ecProdData,tecProcData, ecoProcData,tecAppData, tecFailData, ecoFailData, tecFacData, tecBliskData, tecBliskRoughTask, tecBliskPreTask, tecBliskBladeTask, tecBliskHubTask, tecAccumulatedTasks,[reliability, availability], fiveG, latency = False)
    return resultList

def calculateReliabilityImpactBLISK(tecProdData1,ecProdData1,tecProcData1, ecoProcData1,tecAppData1, tecFailData1, ecoFailData1, tecFacData1, tecBliskData1,tecBliskRoughTask1, tecBliskPreTask1, tecBliskBladeTask1, tecBliskHubTask1, tecAccumulatedTasks1,fiveG = False):
    tecProdData = tecProdData1.copy()
    ecProdData = ecProdData1.copy()
    tecProcData = tecProcData1.copy()
    ecoProcData = ecoProcData1.copy()
    tecAppData = tecAppData1.copy()
    tecFailData = tecFailData1.copy()
    ecoFailData = ecoFailData1.copy()
    tecFacData = tecFacData1.copy()
    tecBliskData = tecBliskData1.copy()
    tecBliskRoughTask = tecBliskRoughTask1.copy()
    tecBliskPreTask = tecBliskPreTask1.copy()
    tecBliskBladeTask = tecBliskBladeTask1.copy()
    tecBliskHubTask = tecBliskHubTask1.copy()
    tecAccumulatedTasks = tecAccumulatedTasks1.copy()
    #Get Availabiity from compared Tech
    availability = 0.99999
    reliability = 0.99999
    #Base data is the calculated data without 5g

    resultList = calculateBLISKGoals(tecProdData,ecProdData,tecProcData, ecoProcData,tecAppData, tecFailData, ecoFailData, tecFacData, tecBliskData, tecBliskRoughTask, tecBliskPreTask, tecBliskBladeTask, tecBliskHubTask, tecAccumulatedTasks,[reliability, availability], fiveG, latency = False)
    return resultList

def calculateICTImpactBLISK(tecProdData1,ecProdData1,tecProcData1, ecoProcData1,tecAppData1, tecFailData1, ecoFailData1, tecFacData1, tecBliskData1,tecBliskRoughTask1, tecBliskPreTask1, tecBliskBladeTask1, tecBliskHubTask1, tecAccumulatedTasks1,fiveG = False):
    tecProdData = tecProdData1.copy()
    ecProdData = ecProdData1.copy()
    tecProcData = tecProcData1.copy()
    ecoProcData = ecoProcData1.copy()
    tecAppData = tecAppData1.copy()
    tecFailData = tecFailData1.copy()
    ecoFailData = ecoFailData1.copy()
    tecFacData = tecFacData1.copy()
    tecBliskData = tecBliskData1.copy()
    tecBliskRoughTask = tecBliskRoughTask1.copy()
    tecBliskPreTask = tecBliskPreTask1.copy()
    tecBliskBladeTask = tecBliskBladeTask1.copy()
    tecBliskHubTask = tecBliskHubTask1.copy()
    tecAccumulatedTasks = tecAccumulatedTasks1.copy()
    reliability = 0
    availability = 0
    
    tecFacData['f_electricpowerconsumption_network'] = 1
    resultList = calculateBLISKGoals(tecProdData,ecProdData,tecProcData, ecoProcData,tecAppData, tecFailData, ecoFailData, tecFacData, tecBliskData, tecBliskRoughTask, tecBliskPreTask, tecBliskBladeTask, tecBliskHubTask, tecAccumulatedTasks,[reliability, availability], fiveG)
    return resultList

#######################################################
    
#Function to calculate the AGV Use Case Elements
def calculateAGVGoals(tecProdData1,ecProdData1,tecProcData1, ecoProcData1,tecRobData1, tecFailData1, ecoFailData1, tecFacData1, techDict, relAvail):
    tecProdData = tecProdData1.copy()
    ecProdData = ecProdData1.copy()
    tecProcData = tecProcData1.copy()
    ecoProcData = ecoProcData1.copy()
    tecRobData = tecRobData1.copy()
    tecFailData = tecFailData1.copy()
    ecoFailData = ecoFailData1.copy()
    tecFacData = tecFacData1.copy()
    
    reliability = relAvail[0]
    availability = relAvail[1]
    #Calculate TecRobot Data
    if (goals['f_flex'] or goals['f_prod'] or goals['f_qual'] or goals['f_safe'] or goals['f_sus'] or goals['f_util'] or goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        tecRobData['f_batteryoperationtimepercharge'] = tecRobData['f_batteryoperationtimepercharge_planned']/(tecRobData['f_speed_average']**0.5)
        tecRobData['f_loadingsdurationroute'] = tecRobData['f_loadingsperroute']*tecRobData['f_loadingduration'] 
        tecRobData['f_transporttime'] = (tecRobData['f_distancestraight']/tecRobData['f_speed_average'])+(tecRobData['f_distancecrossing']/tecRobData['f_speedcrossing_average']) +(tecRobData['f_distanceturning']/tecRobData['f_speedcurve_average'])+tecRobData['f_loadingsdurationroute']
        if tecRobData['f_distance_total'] == 0:
            tecRobData['f_batterydistancepercharge'] = 0
            tecRobData['f_batterychargingsperday'] = 0
        else:
            tecRobData['f_batterydistancepercharge'] = tecRobData['f_batteryoperationtimepercharge']*((tecRobData['f_distancestraight']*tecRobData['f_speed_average'])+(tecRobData['f_distancecrossing']*tecRobData['f_speedcrossing_average'])+(tecRobData['f_distanceturning']*tecRobData['f_speedcurve_average']))/tecRobData['f_distance_total']*3600
            tecRobData['f_batterychargingsperday'] = tecProcData['f_applicationoperationtime_planned']*tecRobData['f_distance_total']/(tecRobData['f_transporttime']/3600)/tecRobData['f_batterydistancepercharge']  #Needs to be changed
        tecRobData['f_batterychargetime_total'] = tecRobData['f_batterychargingduration']*tecRobData['f_batterychargingsperday'] 
#Update TecProcess Data
    if (goals['f_flex'] or goals['f_prod'] or goals['f_qual'] or goals['f_safe'] or goals['f_sus'] or goals['f_util'] or goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        tecProcData['f_applicationdowntime_planned'] = tecRobData['f_batterychargetime_total']
        tecProcData['f_applicationmaintenance_number'] = tecRobData['f_batterychargingsperday']/2 
        tecProcData['f_applicationmaintenance_total'] = tecProcData['f_applicationmaintenance_time']*tecProcData['f_applicationmaintenance_number']  
#Update TecFailure Data
    if (goals['f_flex'] or goals['f_prod'] or goals['f_qual'] or goals['f_safe'] or goals['f_sus'] or goals['f_util'] or goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        tecProcData['f_applicationsetup_timeday'] = tecProcData['f_applicationsetup_time']*tecProcData['f_applicationsetup_number']
        tecProcData['f_applicationbusytime_planned'] = tecProcData['f_applicationoperationtime_planned']-tecProcData['f_applicationdowntime_planned']
        tecProcData['f_applicationproductiontime_planned'] = tecProcData['f_applicationbusytime_planned'] - tecProcData['f_applicationsetup_timeday']
        tecFailData['f_fedisabledcommunication'] = tecProcData['f_applicationproductiontime_planned']/(time_interval_signals/3600)*(1-availability) 
        tecFailData['f_feshortterm'] = tecFailData['f_fedisabledcommunication'] 
        tecFailData['f_feshortterm_ttr_total'] = tecFailData['f_fedisabledcommunication']*tecFailData['f_feshortterm_ttr']
#Update EcoProc Data
    #if (goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
    #    ecoProcData['f_costofapplicationdowntime_unplanned'] = ecoProcData['f_costofapplicationdowntime_planned']*8

    #Calculate Elements and dependents
    #Calculate RuntimeperPart Planned
    if (goals['f_flex'] or goals['f_prod'] or goals['f_qual'] or goals['f_safe'] or goals['f_sus'] or goals['f_util'] or goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        try:
            tecProcData['f_runtimeperpart_planned'] = tecRobData['f_transporttime']/3600/tecProcData['f_batchsize']
            tecProcData['f_runtimeperpart_additional'] = 0
            tecProdData['f_producedquantity_planned'] = tecProcData['f_applicationproductiontime_planned']/tecProcData['f_runtimeperpart_planned']
            tecProcData['f_runsperday_planned'] = tecProdData['f_producedquantity_planned']/tecProcData['f_batchsize']
        except:
            redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Elements RuntimeperPart-Planned cannot be calculated')))
            return dict()
    #Calculate PersonnelAttendanceTime Planned
    if (goals['f_prod'] or goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        try:
            tecProcData['f_personnelattendancetime_planned'] = tecProcData['f_personnelworktimepershift_planned']*tecProcData['f_shiftsperday']
        except:
            redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Elements Personnel Attendance Time cannot be calculated')))
            return dict()
    #Calculate Paths and Area
    if (goals['f_mob']):
        tecFacData['f_transportarea'] = tecRobData['f_distance_total']*tecFacData['f_paths_requiredwidth']
        tecFacData['f_productionarea'] = tecFacData['f_plantarea_total']-tecFacData['f_reworkarea']-tecFacData['f_storagearea']-tecFacData['f_transportarea']
        if (tecFacData['f_paths_requiredwidth'] == 2.5):
            tecFacData['f_pathsaccessible'] = tecFacData['f_paths_over5m']
        else:
            tecFacData['f_pathsaccessible'] = tecFacData['f_paths_over5m']+tecFacData['f_paths_over35m']
    #Calculate Consumptions
    if (goals['f_sus'] or goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        try:
            tecRobData['f_batteryconsumption'] = tecRobData['f_batterycapacity']*tecRobData['f_batteryvoltage']/1000
            tecFacData['f_compressedairconsumption_total'] = tecProcData['f_applicationoperationtime_planned']*(tecFacData['f_compressedairconsumption']*60)
            tecFacData['f_electricpowerconsumption_rework_total'] = 0
            tecFacData['f_electricpowerconsumption_application'] = tecRobData['f_batteryconsumption']*tecRobData['f_batterychargingsperday']
            tecFacData['f_electricpowerconsumption_application_total'] = tecFacData['f_electricpowerconsumption_application']
            tecFacData['f_electricpowerconsumption_network_total'] = tecProcData['f_applicationoperationtime_planned']*tecFacData['f_electricpowerconsumption_network'] 
            tecFacData['f_electricpowerconsumption_total'] = tecFacData['f_electricpowerconsumption_network_total']+tecFacData['f_electricpowerconsumption_application'] 
            tecFacData['f_gasconsumption_total'] = tecProcData['f_applicationoperationtime_planned']*tecFacData['f_gasconsumption']*f_BTU_inkWh 
            tecFacData['f_energyconsumption_total'] = tecFacData['f_gasconsumption_total']+tecFacData['f_electricpowerconsumption_total']+tecFacData['f_compressedairconsumption_total']
            tecFacData['f_waterconsumption_total'] = tecProcData['f_applicationoperationtime_planned']*tecFacData['f_waterconsumption']
        except:
            redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Elements Consumption cannot be calculated')))
            return dict()
    #Calculate ApplicationSetupTime
    if (goals['f_flex'] or goals['f_prod'] or goals['f_qual'] or goals['f_safe'] or goals['f_sus'] or goals['f_util'] or goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        try:
            tecProcData['f_applicationsetup_timeday'] = tecProcData['f_applicationsetup_time']*tecProcData['f_applicationsetup_number']
        except: #TypeError if Calculation is done with None 
            redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Element ApplicationSetupTime cannot be calculated')))
            return dict()
    #Calculate Accidents, FailureeventsTotal, ApplicationDowntimeActual, ApplicationBusyTimeActual, ApplicationProductionTime-Actual, OPEXRepair
    if (goals['f_flex'] or goals['f_prod'] or goals['f_qual'] or goals['f_safe'] or goals['f_sus'] or goals['f_util'] or goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        #Fetch Obstacles for the track, if none available, take standard by us
        try:
            obj_straight = db(db.t_applicationData_obstacles).select().first().as_dict()['f_obstacles_straight']
            obj_curve = db(db.t_applicationData_obstacles).select().first().as_dict()['f_obstacles_curve']
            obj_crossing = db(db.t_applicationData_obstacles).select().first().as_dict()['f_obstacles_crossing']
        except:
            obj_straight = 0.001
            obj_curve = 0.0025
            obj_crossing = 0.005
        try:
            tecFailData['f_feaccidents'] = ((tecRobData['f_distancestraight']*obj_straight+tecRobData['f_distanceturning']*obj_curve+tecRobData['f_distancecrossing']*obj_crossing)*(1-(reliability*availability)))*tecProcData['f_runsperday_planned']
            tecFailData['f_felongterm'] = tecFailData['f_feaccidents']
            tecFailData['f_felongterm_ttr_total'] = tecFailData['f_feaccidents']*tecFailData['f_felongterm_ttr']
            tecFailData['f_failureevents_total'] = tecFailData['f_feshortterm']+tecFailData['f_felongterm']
            tecFailData['f_failureevents_ttr_total'] = tecFailData['f_feshortterm_ttr_total']+tecFailData['f_felongterm_ttr_total']
        except:
            redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Element Accidents or a dependent cannot be calculated')))
            return dict()
            #ApplicationDowntimeActual, ApplicationBusyTimeActual, ApplicationProductionTime-Actual
    if (goals['f_flex'] or goals['f_prod'] or goals['f_qual'] or goals['f_safe'] or goals['f_sus'] or goals['f_util'] or goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        try:
            tecProcData['f_applicationdowntime_unplanned'] = tecFailData['f_failureevents_ttr_total'] 
            tecProcData['f_applicationdowntime_actual'] = tecProcData['f_applicationdowntime_unplanned']+tecProcData['f_applicationdowntime_planned'] 
            tecProcData['f_applicationbusytime_actual'] = tecProcData['f_applicationbusytime_planned']-tecProcData['f_applicationdowntime_unplanned'] 
            tecProcData['f_applicationproductiontime_actual'] = tecProcData['f_applicationbusytime_actual']-tecProcData['f_applicationsetup_timeday']
        except:
            redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Element Downtime Unplanned or a dependent cannot be calculated')))
            return dict()
    if (goals['f_prod'] or goals['f_qual'] or goals['f_sus'] or goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
            #xxxQuantity
        try:
            tecProdData['f_producedquantity_actual'] = tecProcData['f_applicationproductiontime_actual']/tecProcData['f_runtimeperpart_planned']
        except:
            redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Element Produced Quantity Actual or a dependent cannot be calculated')))
            return dict()
    if (goals['f_qual'] or goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        try:
            tecProdData['f_inspectedquantity'] = tecProdData['f_inspectedpercentage']*0.01*tecProdData['f_producedquantity_actual']
            tecProdData['f_reworkquantity'] = tecProdData['f_inspectedquantity']*tecProdData['f_reworkpercentage']*0.01
            tecProdData['f_timetorework_total'] = tecProdData['f_reworkquantity']*tecProdData['f_timetorework']
            if tecProdData['f_reworkpercentage']==100:
                tecProdData['f_firsttimegoodquantity'] = 0
            else:
                tecProdData['f_firsttimegoodquantity'] = tecProdData['f_inspectedquantity']-tecProdData['f_inspectedquantity']*tecProdData['f_reworkpercentage']*0.01
            tecProdData['f_scrapquantity'] = tecProcData['f_batchsize']*tecFailData['f_felongterm'] #AGV Specific
            tecProdData['f_goodquantity'] = tecProdData['f_producedquantity_actual']-tecProdData['f_scrapquantity']
        except:
            redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Element Inspected Quantity or a dependent cannot be calculated')))
            return dict()
    if (goals['f_qual']):
        try:
            tecProcData['f_worktime_product'] = tecProcData['f_runtimeperpart_planned'] + tecProdData['f_inspectiontime'] + tecProdData['f_timetorework']
            tecProcData['f_totalworktime'] = tecProcData['f_worktime_product']*tecProdData['f_producedquantity_actual']
        except:
            redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Element Worktime per Product cannot be calculated')))
            return dict()
    
    mainElementsList = calculateMainGoals(tecProdData,ecProdData,tecProcData, ecoProcData,tecRobData, tecFailData, ecoFailData, tecFacData, [reliability, availability])
    tecProdData = mainElementsList[0]
    ecProdData = mainElementsList[1]
    tecProcData = mainElementsList[2]
    ecoProcData = mainElementsList[3]
    tecRobData = mainElementsList[4]
    tecFailData = mainElementsList[5]
    ecoFailData = mainElementsList[6]
    tecFacData = mainElementsList[7]
        
    return [tecProdData,ecProdData,tecProcData, ecoProcData,tecRobData, tecFailData, ecoFailData, tecFacData]

def calculateBLISKTasks(bliskData, taskDataRough, taskDataPre, taskDataBlade, taskDataHub, tecAccumulatedTasks,relAvail, fiveG = False, latency = False):
    reliability = relAvail[0]
    availability = relAvail[1]
    accumulatedValues = {'f_millingtime':0,'f_tooldamage_weighted':0,'f_tooldamage_critical_weighted':0,'f_vibrations_blisk':0,'f_vibrations_blisk_noreaction':0,'f_feedspeedadoption_number':0,'f_additionaltime_total':0}
    
    taskDataRough['f_millingvolume'] =(bliskData['f_width']-2*taskDataRough['f_surface_thickness'])*bliskData['f_length']*(bliskData['f_height']-taskDataRough['f_surface_thickness'])*bliskData['f_number']
    taskDataPre['f_millingvolume'] = (bliskData['f_width']*bliskData['f_length']*(taskDataRough['f_surface_thickness']-taskDataPre['f_surface_thickness'])*bliskData['f_number'])+(bliskData['f_height']*bliskData['f_length']*(taskDataRough['f_surface_thickness']-taskDataPre['f_surface_thickness'])*bliskData['f_number']*2)
    taskDataBlade['f_millingvolume'] = (bliskData['f_height']*bliskData['f_length']*(taskDataPre['f_surface_thickness']-taskDataBlade['f_surface_thickness'])*2)
    taskDataHub['f_millingvolume'] =(bliskData['f_width']*bliskData['f_length']*(taskDataPre['f_surface_thickness']-taskDataHub['f_surface_thickness'])*2)
    #Calculate Milling time and MRR
    calculateBLISKMRR(bliskData, taskDataRough, latency)
    calculateBLISKMRR(bliskData, taskDataPre, latency)
    calculateBLISKMRR(bliskData, taskDataBlade, latency)
    calculateBLISKMRR(bliskData, taskDataHub, latency)
    #Calculate accumulated value for Milling Time
    tecAccumulatedTasks['f_millingtime'] = taskDataRough['f_millingtime']+taskDataPre['f_millingtime']+taskDataBlade['f_millingtime']+taskDataHub['f_millingtime']
    #Calculate weighted Tool Damages & Critical weighted Tool Damages
    #Berechne Tool Damages mit Availability und Reliability, da beides beim Basis Case 0 ist, kann es so übernommen werden
    taskDataRough['f_tooldamage'] = taskDataRough['f_tooldamage']*(1-availability*reliability)
    taskDataPre['f_tooldamage'] = taskDataPre['f_tooldamage']*(1-availability*reliability)
    taskDataBlade['f_tooldamage'] = taskDataBlade['f_tooldamage']*(1-availability*reliability)
    taskDataHub['f_tooldamage'] = taskDataHub['f_tooldamage']*(1-availability*reliability)
    taskDataRough['f_tooldamage_critical'] = taskDataRough['f_tooldamage_critical']*(1-availability*reliability)
    taskDataPre['f_tooldamage_critical'] = taskDataPre['f_tooldamage_critical']*(1-availability*reliability)
    taskDataBlade['f_tooldamage_critical'] = taskDataBlade['f_tooldamage_critical']*(1-availability*reliability)
    taskDataHub['f_tooldamage_critical'] = taskDataHub['f_tooldamage_critical']*(1-availability*reliability)
    taskDataRough['f_tooldamage_weighted'] = taskDataRough['f_tooldamage']*taskDataRough['f_millingtime']/tecAccumulatedTasks['f_millingtime']
    taskDataPre['f_tooldamage_weighted'] = taskDataPre['f_tooldamage']*taskDataPre['f_millingtime']/tecAccumulatedTasks['f_millingtime']
    taskDataBlade['f_tooldamage_weighted'] = taskDataBlade['f_tooldamage']*taskDataBlade['f_millingtime']/tecAccumulatedTasks['f_millingtime']
    taskDataHub['f_tooldamage_weighted'] = taskDataHub['f_tooldamage']*taskDataHub['f_millingtime']/tecAccumulatedTasks['f_millingtime']
    
    taskDataRough['f_tooldamage_critical_weighted'] = taskDataRough['f_tooldamage_critical']*taskDataRough['f_millingtime']/tecAccumulatedTasks['f_millingtime']
    taskDataPre['f_tooldamage_critical_weighted'] = taskDataPre['f_tooldamage_critical']*taskDataPre['f_millingtime']/tecAccumulatedTasks['f_millingtime']
    taskDataBlade['f_tooldamage_critical_weighted'] = taskDataBlade['f_tooldamage_critical']*taskDataBlade['f_millingtime']/tecAccumulatedTasks['f_millingtime']
    taskDataHub['f_tooldamage_critical_weighted'] = taskDataHub['f_tooldamage_critical']*taskDataHub['f_millingtime']/tecAccumulatedTasks['f_millingtime']
    
    tecAccumulatedTasks['f_tooldamage_weighted'] = taskDataRough['f_tooldamage_weighted']+taskDataPre['f_tooldamage_weighted']+taskDataBlade['f_tooldamage_weighted']+taskDataHub['f_tooldamage_weighted']
    tecAccumulatedTasks['f_tooldamage_critical_weighted'] = taskDataRough['f_tooldamage_critical_weighted']+taskDataPre['f_tooldamage_critical_weighted']+taskDataBlade['f_tooldamage_critical_weighted']+taskDataHub['f_tooldamage_critical_weighted']
    #Calculate Vibrations and damages per BLISK
    calculateBLISKVibrations(taskDataRough, [reliability, availability])
    calculateBLISKVibrations(taskDataPre, [reliability, availability])
    calculateBLISKVibrations(taskDataBlade, [reliability, availability])
    calculateBLISKVibrations(taskDataHub, [reliability, availability])
    
    tecAccumulatedTasks['f_vibrations_blisk'] = taskDataRough['f_vibrations_blisk']+taskDataPre['f_vibrations_blisk']+taskDataBlade['f_vibrations_blisk']+taskDataHub['f_vibrations_blisk']
    tecAccumulatedTasks['f_vibrations_blisk_noreaction'] = taskDataRough['f_vibrations_blisk_noreaction']+taskDataPre['f_vibrations_blisk_noreaction']+taskDataBlade['f_vibrations_blisk_noreaction']+taskDataHub['f_vibrations_blisk_noreaction']
    tecAccumulatedTasks['f_feedspeedadoption_number'] = taskDataRough['f_feedspeedadoption_number']+taskDataPre['f_feedspeedadoption_number']+taskDataBlade['f_feedspeedadoption_number']+taskDataHub['f_feedspeedadoption_number']
    tecAccumulatedTasks['f_additionaltime_total'] = taskDataRough['f_additionaltime_total']+taskDataPre['f_additionaltime_total']+taskDataBlade['f_additionaltime_total']+taskDataHub['f_additionaltime_total']
    
    return 

def calculateBLISKVibrations(taskData, relAvail):
    availability = relAvail[1]
    reliability = relAvail[0]

    taskData['f_vibrations_blisk_noreaction'] = taskData['f_vibrations_shattering']*taskData['f_millingtime']
    taskData['f_vibrations_blisk'] = taskData['f_vibrations_shattering']*taskData['f_millingtime']*(1-availability*reliability)
    taskData['f_feedspeedadoption_number'] = taskData['f_vibrations_blisk_noreaction']-taskData['f_vibrations_blisk']
    taskData['f_additionaltime_total'] = taskData['f_feedspeedadoption_number']*taskData['f_additionaltime_vibration']
    
    #RAUS taskData['f_damage_blisk'] = taskData['f_vibrations_nonreparable']*taskData['f_millingtime']*(1-availability*reliability)        
    return

#Function to calculate MRR and Milling Time for all BLISK Tasks
def calculateBLISKMRR(bliskData, taskData, latency = False):
    taskData['f_mrr_nofiveG'] = taskData['f_radialcuttingdepth']*taskData['f_axialcuttingdepth']*taskData['f_feedspeed_nofiveG']
    taskData['f_mrr_fiveG'] = taskData['f_radialcuttingdepth']*taskData['f_axialcuttingdepth']*taskData['f_feedspeed_fiveG']
    if latency:
        taskData['f_millingtime'] = taskData['f_millingvolume']/taskData['f_mrr_fiveG']
    else:
        taskData['f_millingtime'] = taskData['f_millingvolume']/taskData['f_mrr_nofiveG']
    return

#Function to calculate the BLISK Use Case
def calculateBLISKGoals(tecProdData1,ecProdData1,tecProcData1, ecoProcData1, tecAppData1, tecFailData1, ecoFailData1, tecFacData1, tecBliskData1,tecBliskRoughTask1, tecBliskPreTask1, tecBliskBladeTask1, tecBliskHubTask1, tecAccumulatedTasks1,relAvail, fiveG = False, latency = False):
    tecProdData = tecProdData1.copy()
    ecProdData = ecProdData1.copy()
    tecProcData = tecProcData1.copy()
    ecoProcData = ecoProcData1.copy()
    tecAppData = tecAppData1.copy()
    tecFailData = tecFailData1.copy()
    ecoFailData = ecoFailData1.copy()
    tecFacData = tecFacData1.copy()
    tecBliskData = tecBliskData1.copy()
    tecBliskRoughTask = tecBliskRoughTask1.copy()
    tecBliskPreTask = tecBliskPreTask1.copy()
    tecBliskBladeTask = tecBliskBladeTask1.copy()
    tecBliskHubTask = tecBliskHubTask1.copy()
    tecAccumulatedTasks = tecAccumulatedTasks1.copy()
    reliability = relAvail[0]
    availability = relAvail[1]
    
    calculateBLISKTasks(tecBliskData, tecBliskRoughTask, tecBliskPreTask, tecBliskBladeTask, tecBliskHubTask, tecAccumulatedTasks, relAvail, fiveG, latency)
    #Fill in paths so that no error occurs
    if (goals['f_mob']):
        tecFacData['f_pathsaccessible'] = tecFacData['f_paths_total']
 #################################################TODO Change the formulas   
    if (goals['f_flex'] or goals['f_prod'] or goals['f_qual'] or goals['f_safe'] or goals['f_sus'] or goals['f_util'] or goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        try:
            tecProcData['f_applicationdowntime_planned_blisk'] = tecAppData['f_workpiecechange_duration']+tecAppData['f_toolchange_duration']*tecAppData['f_toolchange_number']
            tecProcData['f_applicationdowntime_planned'] = tecProcData['f_applicationdowntime_planned_blisk']*tecProcData['f_applicationoperationtime_planned']/(tecAccumulatedTasks['f_millingtime']/60)#min to seconds 
            tecProcData['f_applicationmaintenance_total'] = tecProcData['f_applicationmaintenance_time']*tecProcData['f_applicationmaintenance_number']  
        except:
            redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Elements Application Downtime Planned cannot be calculated')))
            return dict()
#Update TecFailure Data
    if (goals['f_flex'] or goals['f_prod'] or goals['f_qual'] or goals['f_safe'] or goals['f_sus'] or goals['f_util'] or goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        try:
            tecProcData['f_applicationsetup_timeday'] = tecProcData['f_applicationsetup_time']*tecProcData['f_applicationsetup_number']
            tecProcData['f_applicationbusytime_planned'] = tecProcData['f_applicationoperationtime_planned']-tecProcData['f_applicationdowntime_planned']
            tecProcData['f_applicationproductiontime_planned'] = tecProcData['f_applicationbusytime_planned'] - tecProcData['f_applicationsetup_timeday']
            tecFailData['f_fetoolbreakage'] = tecProcData['f_applicationproductiontime_planned']*(tecAccumulatedTasks['f_tooldamage_weighted']+tecAccumulatedTasks['f_tooldamage_critical_weighted'])*60
            tecFailData['f_fetoolbreakage_ttr_total'] = tecFailData['f_fetoolbreakage']*tecFailData['f_fetoolbreakage_ttr']
        except:
            redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Elements Application Busy Time Planned cannot be calculated')))
            return dict()
#Update EcoProc Data
    #if (goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
    #    try:
    #        ecoProcData['f_costofapplicationdowntime_unplanned'] = ecoProcData['f_costofapplicationdowntime_planned']*8
    #    except:
    #        redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Cost of Application Downtime is missing')))
    #        return dict()
    #Calculate Elements and dependents
    #Calculate RuntimeperPart Planned
    if (goals['f_mob'] or goals['f_prod'] or goals['f_qual'] or goals['f_sus'] or goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        try:
            tecProcData['f_runtimeperpart_planned'] = tecAccumulatedTasks['f_millingtime']/60 
        except:
            redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Elements RuntimeperPart-Planned cannot be calculated. Milling Time is missing')))
            return dict()
    if (goals['f_prod'] or goals['f_qual'] or goals['f_sus'] or goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        try:
            tecProcData['f_runtimeperpart_additional'] = tecAccumulatedTasks['f_additionaltime_total']/3600
            #Not necessary for BLISK Case
            tecProdData['f_producedquantity_planned'] = tecProcData['f_applicationproductiontime_planned']/tecProcData['f_runtimeperpart_planned']
            #tecProcData['f_runsperday_planned'] = tecProdData['f_producedquantity_planned']
        except:
            redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Elements RuntimeperPart-Additional cannot be calculated')))
            return dict()
    #Calculate PersonnelAttendanceTime Planned ###BIS HIERHIN IST KORREKT SEIN MIT GOALS
    if (goals['f_prod'] or goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        try:
            tecProcData['f_personnelattendancetime_planned'] = tecProcData['f_personnelworktimepershift_planned']*tecProcData['f_shiftsperday']
        except:
            redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Elements Personnel Attendance Time cannot be calculated')))
            return dict()
    #Calculate Paths and Area
    if (goals['f_mob'] or goals['f_qual'] or goals['f_sus'] or goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        tecProdData['f_reworkvibration_number'] = tecAccumulatedTasks['f_vibrations_blisk']
        tecProdData['f_reworkduration_total'] = tecProdData['f_reworkvibration_number']*tecProdData['f_reworkduration_vibration']
        tecProdData['f_timetorework'] = tecProdData['f_reworkduration_total'] + tecProdData['f_reworkduration']
    if (goals['f_mob']):   
        tecFacData['f_reworkarea_parallelparts'] =  roundUp(tecProdData['f_timetorework']/tecProcData['f_runtimeperpart_planned'])
        tecFacData['f_reworkarea'] = tecFacData['f_reworkarea_onepart']*tecFacData['f_reworkarea_parallelparts'] 
        tecFacData['f_productionarea'] = tecFacData['f_plantarea_total']-tecFacData['f_reworkarea']-tecFacData['f_storagearea']-tecFacData['f_transportarea']
    if (goals['f_qual']):
        tecProcData['f_worktime_product'] = tecProcData['f_runtimeperpart_planned'] + tecProcData['f_runtimeperpart_additional'] + tecProdData['f_inspectiontime'] + tecProdData['f_timetorework']
    #Calculate ApplicationSetupTime
    if (goals['f_flex'] or goals['f_prod'] or goals['f_qual'] or goals['f_safe'] or goals['f_sus'] or goals['f_util'] or goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        try:
            tecProcData['f_applicationsetup_timeday'] = tecProcData['f_applicationsetup_time']*tecProcData['f_applicationsetup_number']
        except:
            redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Element ApplicationSetupTime cannot be calculated')))
            return dict()
    #Calculate Accidents, FailureeventsTotal, ApplicationDowntimeActual, ApplicationBusyTimeActual, ApplicationProductionTime-Actual, OPEXRepair
    if (goals['f_safe'] or goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        tecFailData['f_felongterm'] = tecFailData['f_fetoolbreakage']#Cheating for later
        tecFailData['f_failureevents_total'] = tecFailData['f_fetoolbreakage']
    if (goals['f_flex'] or goals['f_prod'] or goals['f_qual'] or goals['f_safe'] or goals['f_sus'] or goals['f_util'] or goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        #try:
        tecFailData['f_failureevents_ttr_total'] = tecFailData['f_fetoolbreakage_ttr_total']
        #ApplicationDowntimeActual, ApplicationBusyTimeActual, ApplicationProductionTime-Actual
        tecProcData['f_applicationdowntime_unplanned'] = tecFailData['f_failureevents_ttr_total'] 
        tecProcData['f_applicationdowntime_actual'] = tecProcData['f_applicationdowntime_unplanned']+tecProcData['f_applicationdowntime_planned'] 
        tecProcData['f_applicationbusytime_actual'] = tecProcData['f_applicationoperationtime_planned']-tecProcData['f_applicationdowntime_actual']
    if (goals['f_prod'] or goals['f_qual'] or goals['f_sus'] or goals['f_util'] or goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        tecProcData['f_applicationproductiontime_actual'] = tecProcData['f_applicationbusytime_actual']-tecProcData['f_applicationsetup_timeday']
        #xxxQuantity
    if (goals['f_prod'] or goals['f_qual'] or goals['f_sus'] or goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        tecProdData['f_producedquantity_actual'] = tecProcData['f_applicationproductiontime_actual']/(tecProcData['f_runtimeperpart_planned']+tecProcData['f_runtimeperpart_additional'])
    if (goals['f_qual'] or goals['f_sus'] or goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):    
        tecProdData['f_inspectedquantity'] = tecProdData['f_inspectedpercentage']*0.01*tecProdData['f_producedquantity_actual']
        #Consumptions
        tecProdData['f_reworkquantity'] = tecProdData['f_inspectedquantity']*tecProdData['f_reworkpercentage']*0.01
    if (goals['f_qual'] or goals['f_sus'] or goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        tecProdData['f_timetorework_total'] = tecProdData['f_reworkquantity']*tecProdData['f_timetorework']
    if (goals['f_qual'] or goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        tecProdData['f_scrapquantity'] = (tecProcData['f_applicationproductiontime_planned']*tecAccumulatedTasks['f_tooldamage_critical_weighted']*60)
        tecProdData['f_goodquantity'] = tecProdData['f_inspectedquantity']-tecProdData['f_scrapquantity']
    if (goals['f_qual']):
        if tecProdData['f_reworkpercentage']==100:
            tecProdData['f_firsttimegoodquantity'] = 0
        else:
            tecProdData['f_firsttimegoodquantity'] = tecProdData['f_inspectedquantity']-tecProdData['f_inspectedquantity']*tecProdData['f_reworkpercentage']*0.01
        tecProcData['f_totalworktime'] = tecProcData['f_worktime_product']*tecProdData['f_producedquantity_actual']
        #except:
         #   redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Element Accidents or a dependent cannot be calculated')))
          #  return dict()
    #Calculate Consumptions
    if (goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        ecoFailData['f_costmaterialrepair'] = ecoFailData['f_costtoolbreakage']
    if (goals['f_sus'] or goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        #try:
        tecFacData['f_electricpowerconsumption_network_total'] = tecProcData['f_applicationoperationtime_planned']*tecFacData['f_electricpowerconsumption_network']
        tecFacData['f_waterconsumption_total'] = tecProcData['f_applicationproductiontime_actual']*tecFacData['f_waterconsumption']
        tecFacData['f_compressedairconsumption_total'] = tecProcData['f_applicationproductiontime_actual']*(tecFacData['f_compressedairconsumption']) +( tecFacData['f_compressedairconsumption_rework']*tecProdData['f_producedquantity_actual']*tecProdData['f_timetorework'])
        tecFacData['f_electricpowerconsumption_rework_total'] = tecFacData['f_electricpowerconsumption_rework']*tecProdData['f_timetorework']*tecProdData['f_producedquantity_actual']
        tecFacData['f_electricpowerconsumption_application_total'] = tecFacData['f_electricpowerconsumption_application']*tecProcData['f_applicationproductiontime_actual']
        tecFacData['f_electricpowerconsumption_total'] = tecFacData['f_electricpowerconsumption_network_total']+tecFacData['f_electricpowerconsumption_application_total']+tecFacData['f_electricpowerconsumption_rework_total']
        tecFacData['f_gasconsumption_total'] = tecProcData['f_applicationproductiontime_actual']*tecFacData['f_gasconsumption']*f_BTU_inkWh 
        tecFacData['f_energyconsumption_total'] = tecFacData['f_gasconsumption_total']+tecFacData['f_electricpowerconsumption_total']+tecFacData['f_compressedairconsumption_total']
        #except:
          #  redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Elements Consumption cannot be calculated')))
          #  return dict()
################################
    mainElementsList = calculateMainGoals(tecProdData,ecProdData,tecProcData, ecoProcData,tecAppData, tecFailData, ecoFailData, tecFacData, [reliability, availability])
    tecProdData = mainElementsList[0]
    ecProdData = mainElementsList[1]
    tecProcData = mainElementsList[2]
    ecoProcData = mainElementsList[3]
    tecAppData = mainElementsList[4]
    tecFailData = mainElementsList[5]
    ecoFailData = mainElementsList[6]
    tecFacData = mainElementsList[7]
    
    return [tecProdData,ecProdData,tecProcData, ecoProcData,tecAppData, tecFailData, ecoFailData, tecFacData, tecBliskData,tecBliskRoughTask, tecBliskPreTask, tecBliskBladeTask, tecBliskHubTask, tecAccumulatedTasks]

#Function to calculate the Elements that are shared between all Applications and Use Cases
def calculateMainGoals(tecProdData1,ecProdData1,tecProcData1, ecoProcData1,tecRobData1, tecFailData1, ecoFailData1, tecFacData1, relAvail):
    tecProdData = tecProdData1.copy()
    ecProdData = ecProdData1.copy()
    tecProcData = tecProcData1.copy()
    ecoProcData = ecoProcData1.copy()
    tecRobData = tecRobData1.copy()
    tecFailData = tecFailData1.copy()
    ecoFailData = ecoFailData1.copy()
    tecFacData = tecFacData1.copy()
    reliability = relAvail[0]
    availability = relAvail[1]
    if (goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        try:
    #Yearly calculations
            tecProdData['f_producedquantity_actual_yearly'] = tecProcData['f_productiondaysperyear']*tecProdData['f_producedquantity_actual']
            #tecProdData['f_producedquantity_planned_yearly'] = tecProcData['f_productiondaysperyear']*tecProdData['f_producedquantity_planned']#Not necessary for BLISK Case 
            tecProdData['f_inspectedquantity_yearly'] = tecProcData['f_productiondaysperyear']*tecProdData['f_inspectedquantity']
            tecProdData['f_reworkquantity_yearly'] = tecProdData['f_inspectedquantity_yearly']*tecProdData['f_reworkpercentage']*0.01
            #tecProdData['f_firsttimegoodquantity_yearly'] = tecProdData['f_inspectedquantity_yearly']-tecProdData['f_reworkquantity_yearly'] #Wird für nix benutzt
            tecProdData['f_goodquantity_yearly'] = tecProcData['f_productiondaysperyear']*tecProdData['f_goodquantity'] 
            tecProdData['f_scrapquantity_yearly'] = tecProcData['f_productiondaysperyear']*tecProdData['f_scrapquantity']
        except:
            redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Elements cannot be upscaled to yearly margins')))
            return dict()
    
    if (goals['f_prod'] or goals['f_safe']):
        try:
            tecProcData['f_personnelworktime_actual'] = tecProcData['f_shiftsperday']*(tecProcData['f_personnelworktimepershift_planned']-tecProcData['f_personnelbreaktime_planned']-tecFailData['f_failureevents_ttr_total']/24)
        except:
            redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Element PersonnelWorkTime-Actual cannot be calculated even though Goal Productivity or Safety is chosen')))
            return dict()
    
    if (goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        try:
        #Economic Product Data
            ecProdData['f_revenuessale'] = tecProdData['f_goodquantity_yearly']*ecProdData['f_sellingprice']
            ecProdData['f_revenuesindividualization'] = tecProdData['f_goodquantity_yearly']*ecProdData['f_individualizationadditionalprofit']*(tecProdData['f_individualizationpercentage']*0.01) 
            ecProdData['f_opexmaterial'] = tecProdData['f_producedquantity_actual_yearly']*ecProdData['f_materialcostpart']
            ecProdData['f_opexqualitycontrol'] = tecProdData['f_inspectedquantity_yearly']*ecProdData['f_qualitycontrolcostpart']
            ecProdData['f_opexrework'] = tecProdData['f_reworkquantity_yearly']*(tecProdData['f_timetorework']*ecProdData['f_wagerework']+ecProdData['f_materialcostrework'])
            ecProdData['f_opexdisposal'] = tecProdData['f_scrapquantity_yearly']*ecProdData['f_disposalcostpart']
            ecProdData['f_revenuesproduct'] = ecProdData['f_revenuessale']+ecProdData['f_revenuesindividualization']
            ecProdData['f_opexproduct'] = ecProdData['f_opexmaterial']+ecProdData['f_opexqualitycontrol']+ecProdData['f_opexrework']+ecProdData['f_opexdisposal']
            #Economic Process Data
            ecoProcData['f_opexdowntime'] = tecProcData['f_productiondaysperyear']*(tecProcData['f_applicationdowntime_planned']*ecoProcData['f_costofapplicationdowntime_planned']+ tecProcData['f_applicationdowntime_unplanned']*ecoProcData['f_costofapplicationdowntime_unplanned'])
            tecFacData['f_opex_consumption_application'] = tecProcData['f_productiondaysperyear']*((tecFacData['f_electricpowerconsumption_application_total']+tecFacData['f_electricpowerconsumption_rework_total'])*(tecFacData['f_costelectricpower']+tecFacData['f_energytax']*kwh_to_co2)+(tecFacData['f_compressedairconsumption_total']*tecFacData['f_costcompressedair'])+(tecFacData['f_gasconsumption_total']*(tecFacData['f_costgas']+tecFacData['f_energytax']*kwh_to_co2*f_BTU_inkWh))+(tecFacData['f_waterconsumption_total']*tecFacData['f_costwater']))
            tecFacData['f_opex_consumption_network'] = tecProcData['f_productiondaysperyear']*tecFacData['f_electricpowerconsumption_network_total']*(tecFacData['f_costelectricpower']+tecFacData['f_energytax']*kwh_to_co2)
            tecFacData['f_opex_facility'] = tecFacData['f_opex_consumption_application']+tecFacData['f_opex_consumption_network']
            #OPEXRepair & OPEXFailure
            ecoFailData['f_opexrepair'] = tecProcData['f_productiondaysperyear']*(tecFailData['f_failureevents_ttr_total']*ecoFailData['f_wagerepairstaff']+tecFailData['f_felongterm']*(ecoFailData['f_costmaterialrepair']+ecoFailData['f_costfinancialcompensation']))
            ecoFailData['f_opexfailure'] = ecoFailData['f_opexrepair']
            ecoProcData['f_capexprocess'] = 0
            ecoProcData['f_opexpersonnelsetup'] = tecProcData['f_productiondaysperyear']*(ecoProcData['f_wagesetup']*tecProcData['f_applicationsetup_time']*tecProcData['f_applicationsetup_number'])
            ecoProcData['f_opexoperation'] = tecProcData['f_productiondaysperyear']*(tecProcData['f_personnelattendancetime_planned']*ecoProcData['f_wageoperator'])*ecoProcData['f_operator_number']
            ecoProcData['f_opexprocess'] = ecoProcData['f_opexdowntime']+ecoProcData['f_opexpersonnelsetup']+ecoProcData['f_opexoperation']
        except:
            redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Element OPEX cannot be calculated')))
    return [tecProdData,ecProdData,tecProcData, ecoProcData,tecRobData, tecFailData, ecoFailData, tecFacData]

####################################
#Calculates what the impacts of 5G are if loops are activated

def economicCalculator(tecProdData,ecProdData,ecoProcData,ecoFailData,tecFacData, fiveG = False, useCase = None, existCheck = None):
    #Translate existCheck, so it isnt None anymore
    if existCheck is None:
        existCheck = False
    #Flag which checks if investmentdata is added by user, if not, some standard values are used
    no_investmentdata = False
    try:
        network_investment_dict = db().select(db.t_investmentdata.ALL).first().as_dict()
    except:
        no_investmentdata = True
    not_needed_devices_dict = db().select(db.t_statusquo.ALL).first().as_dict() #SHOULD ALWAYS BE AVAILABLE IF WE GOT TILL HERE
    if no_investmentdata:
        lifetime = 11
    else:
        try:
            lifetime = network_investment_dict['f_lifetime'] + 1
        except:
            lifetime = 11
    if lifetime > 0:
        lifetime_list = range(lifetime+ 1)
    else:
        lifetime_list = [0]
    try:
        hurdle_rate = network_investment_dict['f_interest']
    except:
        hurdle_rate = 5 #Decided by us
    if not no_investmentdata: #Go in if investmentdata was available
        if (fiveG or (useCase == 'blisk') or (not existCheck)): #Dont do investments for base case
            #Calculate Investment for Application, if values are missing, add 0
            capex_application = 0
            if not not_needed_devices_dict['f_sensor']:
                try:
                    capex_application += network_investment_dict['f_sensors_num']*network_investment_dict['f_sensors_cost']
                except:
                    capex_application += 0
            if not not_needed_devices_dict['f_actuator']:
                try:
                    capex_application += network_investment_dict['f_actuator_num']*network_investment_dict['f_actuator_cost']
                except:
                    capex_application += 0
            if not not_needed_devices_dict['f_controller']:
                try:
                    capex_application += network_investment_dict['f_controller_num']*network_investment_dict['f_controller_cost']
                except:
                    capex_application += 0
        else:
            capex_application = 0
            #Calculate Network Investment Values, same as above
        if fiveG:
            capex_network = 0
            if not not_needed_devices_dict['f_basestation']:
                try:
                    capex_network += network_investment_dict['f_basestation_num']*network_investment_dict['f_basestation_cost']
                except:
                    capex_network = capex_network
            if not not_needed_devices_dict['f_core']:
                try:
                    capex_network += network_investment_dict['f_core_num']*network_investment_dict['f_core_cost']
                except:
                    capex_network = capex_network
            if not not_needed_devices_dict['f_userequipment']:
                try:
                    capex_network += network_investment_dict['f_userequipment_num']*network_investment_dict['f_userequipment_cost']
                except:
                    capex_network = capex_network
            if not not_needed_devices_dict['f_controlnet']:
                try:
                    capex_network += network_investment_dict['f_controlnet_num']*network_investment_dict['f_controlnet_cost']
                except:
                    capex_network = capex_network
            if not not_needed_devices_dict['f_datanet']:
                try:
                    capex_network += network_investment_dict['f_datanet_num']*network_investment_dict['f_datanet_cost']
                except:
                    capex_network = capex_network
            if not not_needed_devices_dict['f_userplane']:
                try:
                    capex_network += network_investment_dict['f_userplane_num']*network_investment_dict['f_userplane_cost']
                except:
                    capex_network = capex_network
        else:
            capex_network = 0
    else:
        capex_application = 0
        capex_network = 0
    opex_total =  ecoFailData['f_opexfailure']+ecProdData['f_opexproduct']+ecoProcData['f_opexprocess']+tecFacData['f_opex_facility']
    if goals['f_opexp']:
        opex_per_product = opex_total/tecProdData['f_producedquantity_actual_yearly']
    else:
        opex_per_product = 0
    investment_app = capex_application+capex_network
    cashflow = ecProdData['f_revenuesproduct']-opex_total
    depreciation_iter = map(lambda x:investment_app/x if (x!= 0) else 0, lifetime_list)#Depreciation per year for the lifetime of the application
    depreciation_dict = {x:x for x in depreciation_iter}
    ###############Calculate NPV, not sure if all years are necessary
    if goals['f_npv']:
        npv_dict = {x:0 for x in lifetime_list}
        npv_dict[0] = 0-investment_app
        cf_delta = 0
        if lifetime > 0:
            for x in range(1,lifetime):
                npv_dict[x] = npv_dict[x-1]+((cashflow)/((1+hurdle_rate/100)**x))
    else:
        npv_dict[0] = 0
    ###############Calculate RoI
    if goals['f_roi']:
        #Determine total profit
        if lifetime > 0:
            t_profit = (cashflow*lifetime-investment_app)
        else:
            t_profit = 0
        if (lifetime > 0 and investment_app >0):
            roi = t_profit/investment_app #TotalProfit/Investment
        else:
            roi = 0
    else:
        roi = 0
        t_profit = 0
    ###############Payback Period
    if goals['f_pb']:
        if lifetime >= 0:
            pb_period = investment_app/cashflow
        else:
            pb_period = 0
    else:
        pb_period = 0
    return {'f_opex_total': opex_total,'f_opex_per_product':opex_per_product,'f_cashflow_yearly':cashflow, 'f_investment':investment_app,'f_capapp':capex_application, 'f_capnet': capex_network, 'f_revenues':ecProdData['f_revenuesproduct'], 'f_npv_lifetime': npv_dict[max(0,lifetime-1)], 'f_roi_lifetime':roi, 'f_payback_period':pb_period, 'f_total_profit':t_profit}

def roundUp(number):
    value = 0
    if number < 0:
        return value
    else:
        value = divmod(number,1)[0] + 1
    return value

def isGoalsEmpty(goal):
    for g in goal:
        if g == 'id':
            continue
        elif goal[g] == True:
            return True
    return False

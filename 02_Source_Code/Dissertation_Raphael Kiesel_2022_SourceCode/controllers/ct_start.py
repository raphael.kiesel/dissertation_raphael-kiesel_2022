# -*- coding: utf-8 -*-
# Versuchen Sie so etwas wie

def welcome():
    urlEvaluator = URL('ct_start','mainScreen')
    urlRecommendation = URL('ct_explanation','mainScreen')
    return dict(urlEv=urlEvaluator, urlRec=urlRecommendation, progress=10)

def emptyDatabase():
    deleteGoals()
    deleteControlTasks()
    deleteInputData()
    deleteApplicationData()
    if (not db(db.t_ecoValues).isempty()):
        db.t_ecoValues.truncate()
    redirect(URL('ct_start', 'mainScreen'))
    return dict(urlRec=URL('ct_start', 'mainScreen'), progress=10)

def deleteDBsData():
    deleteInputData()
    if (not db(db.t_blisktasks_accumulated).isempty()):
        db.t_blisktasks_accumulated.truncate()
    redirect(URL('ct_start', 'mainScreen'))
    return dict(urlRec=URL('ct_start', 'mainScreen'), progress=10)
    
def selectUseCase():
    nextUrl = URL('ct_recommendation', 'requires')
    app = db(db.t_applicationselection).select().first()
    if app.f_name == 'agv':
        formUseCase = TABLE(TR(TD(XML('<button id="checkMOBAUT" class="buttonBox" onclick=' + 'window.location="{{=nextUrl}}";' + '><span class="icon-Choose-Application_Mobility-Automation"></span></button>'), _class="imgRow")),
                        TR(TD(XML("Mobility<br> Automation"), _class="textRow")))
    elif app.f_name == 'macTool':
        formUseCase = TABLE(TR(TD(XML('<button id="checkMCP" class="buttonBox" onclick=' + 'window.location="{{=nextUrl}}";' + '><span class="icon-Choose-Application_Mobile-Control-Panel"></span></button>'), _class="imgRow"),
                       TD(XML('<button id="checkMC"_class="buttonBox" onclick=' + 'window.location="{{=nextUrl}}";' + '><span class="icon-Choose-Use-Case_Motion-Control"></span></button>'), _class="imgRow"),
                       TR(TD(XML("Mobile<br> Control Panels"), _class="textRow"),TD(XML("Motion<br> Control"), _class="textRow"))))
    if formUseCase.process(keepvalues=True).accepted:
        response.flash = 'form accepted'
        db.t_choices.insert(f_useCase = request.vars.useCase)
        redirect(nextUrl)
    return dict(form = formUseCase, url=nextUrl, progress=10)

def mainScreen():
    chosenUseCase = 0
    try:
        chosenUseCase = db(db.t_applicationselection).select().first().as_dict()["f_name"]
    except:
        pass
    goalUrl = URL('ct_goals', 'chooseGoal')
    evaluationUrl = URL('ct_results', 'calculations')
    useCaseUrl = URL('ct_recommendation', 'selectApplication')
    if chosenUseCase == "agv":
        loopUrl = URL('ct_controlLoops', 'enterControlLoops')
    elif chosenUseCase == "blisk":
        loopUrl = URL('ct_controlLoops', 'enterBLISKControlTasks')
    else:
        loopUrl = URL('ct_controlLoops', 'enterControlLoops')
    recommendationUrl = URL('ct_recommendation','requires')
    emptydbUrl = URL('ct_start', 'emptyDatabase')
    return dict(useCaseUrl=useCaseUrl, goalUrl=goalUrl, evaluationUrl=evaluationUrl, loopUrl=loopUrl, recommendationUrl=recommendationUrl, emptydbUrl = emptydbUrl, progress=10)

def downloadData():
    from io import StringIO
    s = StringIO()
    db.export_to_csv_file(s)
    response.headers['Content-Type'] = 'text/csv'
    return s.getvalue()

def uploadData():
    form = FORM(INPUT(_type='file', _name='data'), INPUT(_type='submit'))
    if form.process().accepted:
        db.import_from_csv_file(form.vars.data.file, unique=False)
        ######Maybe i dont need this, maybe I do
        # for every table
        #for tablename in db.tables:
        #    table = db[tablename]
        #    # for every uuid, delete all but the latest
        #    items = db(table).select(table.id, table.uuid,
        #                             orderby=~table.modified_on,
        #                             groupby=table.uuid)
        #    for item in items:
        #        db((table.uuid == item.uuid) & (table.id != item.id)).delete()
    return dict(form=form)

################Function to input our default data
def defaultDataAGV():
    defaults = {}
    choiceNo5G = {}
    choice5G = {}
    controlTask = []
    #Mobile Robots Specific Data
    choiceNo5G['f_existing'] = True
    choiceNo5G['f_batteryoperationtimepercharge_planned'] = 3
    choiceNo5G['f_batterycapacity'] = 20
    choiceNo5G['f_batterychargingduration'] = 4
    choiceNo5G['f_batteryvoltage'] = 24
    choiceNo5G['f_speed_average'] = 0.9
    choiceNo5G['f_speedcurve_average'] = 0.9
    choiceNo5G['f_speedcrossing_average'] = 0.75
    choiceNo5G['f_loadingduration'] = 300
    #Mobile Robots Specific Data
    choice5G['f_existing'] = False
    choice5G['f_batteryoperationtimepercharge_planned'] = 3
    choice5G['f_batterycapacity'] = 20
    choice5G['f_batterychargingduration'] = 4
    choice5G['f_batteryvoltage'] = 24
    choice5G['f_speed_average'] = 1
    choice5G['f_speedcurve_average'] = 1
    choice5G['f_speedcrossing_average'] = 0.8
    choice5G['f_loadingduration'] = 270
    #Compare Technology
    defaults['f_lte'] = True
    defaults['f_wifi'] = False
    defaults['f_reliability'] = 0.999
    defaults['f_availability'] = 0.999
    #Status Quo
    defaults['f_useCase'] = 'mobaut'
    defaults['f_name'] = 'agv'
    
    defaults['f_ncs'] = False
    defaults['f_actuator'] = False
    defaults['f_controller'] = False
    defaults['f_sensor'] = False
    defaults['f_netarchitecture'] = False
    defaults['f_basestation'] = False
    defaults['f_core'] = False
    defaults['f_userequipment'] = False
    defaults['f_controlnet'] = False
    defaults['f_datanet'] = False
    defaults['f_userplane'] = False
    #Control Tasks
    defaults['f_obstacles_straight'] = 0.001
    defaults['f_obstacles_curve'] = 0.0025
    defaults['f_obstacles_crossing'] = 0.005
    #Choices for the Use Case
    #Facility Data
    defaults['f_plantarea_total'] = 1800
    defaults['f_storagearea'] = 225
    defaults['f_reworkarea'] = 0
    defaults['f_paths_over5m'] = 3
    defaults['f_paths_over35m'] = 2
    defaults['f_paths_total'] = 5
    defaults['f_paths_requiredwidth'] = 5
    defaults['f_electricpowerconsumption_application'] = 0
    defaults['f_electricpowerconsumption_network'] = 1.25
    defaults['f_compressedairconsumption'] = 0
    defaults['f_gasconsumption'] = 0
    defaults['f_waterconsumption'] = 0
    #Economic
    defaults['f_costelectricpower'] = 0.3
    defaults['f_costcompressedair'] = 0.01
    defaults['f_costgas'] = 0.01
    defaults['f_costwater'] = 0.05
    defaults['f_energytax'] = 1
    #Failure Data
    defaults['f_felongterm_ttr'] = 1.5
    defaults['f_feshortterm_ttr'] = 10/3600
    #Economic Failure Data
    defaults['f_wagerepairstaff'] = 20
    defaults['f_costmaterialrepair'] = 1000
    defaults['f_costfinancialcompensation'] = 5000
    #Process Data
    defaults['f_productiondaysperyear'] = 250
    defaults['f_applicationmaintenance_time'] = 1
    defaults['f_applicationoperationtime_planned'] = 8
    defaults['f_applicationsetup_number'] = 1
    defaults['f_applicationsetup_time'] = 0.5
    defaults['f_batchsize'] = 4
    defaults['f_personnelbreaktime_planned'] = 0.5
    defaults['f_personnelworktimepershift_planned'] = 7.5
    defaults['f_shiftsperday'] = 1
    #Economic Process Data
    defaults['f_costofapplicationdowntime_planned'] = 1000
    defaults['f_costofapplicationdowntime_unplanned'] = 9000
    defaults['f_wagesetup'] = 15
    defaults['f_wageoperator'] = 15
    defaults['f_operator_number'] = 1
    #Product Data
    defaults['f_individualizationpercentage'] = 0
    defaults['f_inspectedpercentage'] = 0
    defaults['f_reworkpercentage'] = 0
    defaults['f_reworkduration'] = 0.1
    defaults['f_inspectiontime'] = 0
    defaults['f_productvariantsonapplication'] = 1
    defaults['f_totalproductvariants'] = 5
    defaults['f_timetorework'] = 0.1
    #Economic Product Data
    defaults['f_sellingprice'] = 165
    defaults['f_individualizationadditionalprofit'] = 0
    defaults['f_materialcostpart'] = 120
    defaults['f_qualitycontrolcostpart'] = 25
    defaults['f_disposalcostpart'] = 0
    defaults['f_materialcostrework'] = 50
    defaults['f_wagerework'] = 12
    
    controlTask.append({'f_trackType':"Straight", 'f_distance':250},)
    controlTask.append({'f_trackType':"Curve", 'f_distance':20})
    controlTask.append({'f_trackType':"Crossing", 'f_distance':30})
    controlTask.append({'f_trackType':"Number of loadings", 'f_amount':2})
    insertDefaultData([defaults])
    db.t_applicationData_mobileRobots.insert(**db.t_applicationData_mobileRobots._filter_fields(choiceNo5G))
    db.t_applicationData_mobileRobots.insert(**db.t_applicationData_mobileRobots._filter_fields(choice5G))
    db.t_applicationData_obstacles.insert(**db.t_applicationData_obstacles._filter_fields(defaults))
    for row in controlTask:
        db.t_trackSetup.insert(**db.t_trackSetup._filter_fields(row))
    
    redirect(URL('ct_start','mainScreen'))

def defaultDataBLISK():
    defaults = {}
    choiceNo5G = {}
    ctRoughing = {}
    ctPre = {}
    ctBlade = {}
    ctHub = {}
    bliskData = {}
    #Blisk Specific Data
    choiceNo5G['f_workpiecechange_duration'] = 0.166
    choiceNo5G['f_toolchange_duration'] = 0.166
    choiceNo5G['f_toolchange_number'] = 1
    ##Control Task Data
    bliskData['f_width'] = 45
    bliskData['f_length'] = 45
    bliskData['f_height'] = 100
    bliskData['f_number'] = 30
    
    ctRoughing['f_surface_thickness'] = 10
    ctRoughing['f_radialcuttingdepth'] = 2
    ctRoughing['f_axialcuttingdepth'] = 7.5
    ctRoughing['f_feedspeed_nofiveG'] = 1200
    ctRoughing['f_feedspeed_fiveG'] = 1250
    ctRoughing['f_vibrations_shattering'] = 0
    ctRoughing['f_tooldamage'] = 0.0001
    ctRoughing['f_tooldamage_critical'] = 0.00001
    ctRoughing['f_additionaltime_vibration'] = 0
    
    ctPre['f_surface_thickness'] = 5
    ctPre['f_radialcuttingdepth'] = 2
    ctPre['f_axialcuttingdepth'] = 5
    ctPre['f_feedspeed_nofiveG'] = 730
    ctPre['f_feedspeed_fiveG'] = 750
    ctPre['f_vibrations_shattering'] = 0.01
    ctPre['f_tooldamage'] = 0.0001
    ctPre['f_tooldamage_critical'] = 0.00001
    ctPre['f_additionaltime_vibration'] = 0.25
    
    ctBlade['f_surface_thickness'] = 0.011
    ctBlade['f_radialcuttingdepth'] = 0.5
    ctBlade['f_axialcuttingdepth'] = 0.6
    ctBlade['f_feedspeed_nofiveG'] = 350
    ctBlade['f_feedspeed_fiveG'] = 360
    ctBlade['f_vibrations_shattering'] = 0.2
    ctBlade['f_tooldamage'] = 0.00001
    ctBlade['f_tooldamage_critical'] = 0.00001
    ctBlade['f_additionaltime_vibration'] = 5
    
    ctHub['f_surface_thickness'] = 0.017
    ctHub['f_radialcuttingdepth'] = 1
    ctHub['f_axialcuttingdepth'] = 0.6
    ctHub['f_feedspeed_nofiveG'] = 450
    ctHub['f_feedspeed_fiveG'] = 460
    ctHub['f_vibrations_shattering'] = 0.1
    ctHub['f_tooldamage'] = 0.00001
    ctHub['f_tooldamage_critical'] = 0.00001
    ctHub['f_additionaltime_vibration'] = 1
    #Choices for the Use Case
    #Compare Technology
    defaults['f_lte'] = False
    defaults['f_wifi'] = True
    defaults['f_reliability'] = 0
    defaults['f_availability'] = 0
    #Status Quo
    defaults['f_useCase'] = 'mcp'
    defaults['f_name'] = 'blisk'
    
    defaults['f_ncs'] = False
    defaults['f_actuator'] = False
    defaults['f_controller'] = False
    defaults['f_sensor'] = False
    defaults['f_netarchitecture'] = False
    defaults['f_basestation'] = False
    defaults['f_core'] = False
    defaults['f_userequipment'] = False
    defaults['f_controlnet'] = False
    defaults['f_datanet'] = False
    defaults['f_userplane'] = False
    #Facility Data
    defaults['f_plantarea_total'] = 8100
    defaults['f_storagearea'] = 0
    defaults['f_reworkarea_onepart'] = 25.3
    defaults['f_transportarea'] = 100
    defaults['f_paths_total'] = 1
    defaults['f_electricpowerconsumption_application'] = 3600
    defaults['f_electricpowerconsumption_rework'] = 360
    defaults['f_electricpowerconsumption_network'] = 0
    defaults['f_compressedairconsumption'] = 50
    defaults['f_compressedairconsumption_rework'] = 25
    defaults['f_gasconsumption'] = 0
    defaults['f_waterconsumption'] = 5
    #Economic
    defaults['f_costelectricpower'] = 0.3
    defaults['f_costcompressedair'] = 0.01
    defaults['f_costgas'] = 0.01
    defaults['f_costwater'] = 0.05
    defaults['f_energytax'] = 30
    #Failure Data
    defaults['f_feaccidents'] = 0
    defaults['f_fetoolbreakage_ttr'] = 2
    #Economic Failure Data
    defaults['f_wagerepairstaff'] = 35
    defaults['f_costmaterialrepair'] = 0
    defaults['f_costtoolbreakage'] = 1000
    defaults['f_costfinancialcompensation'] = 0
    #Process Data
    defaults['f_productiondaysperyear'] = 250
    defaults['f_applicationmaintenance_time'] = 2
    defaults['f_applicationmaintenance_number'] = 0.2
    defaults['f_applicationoperationtime_planned'] = 16
    defaults['f_applicationsetup_number'] = 1
    defaults['f_applicationsetup_time'] = 0.5
    defaults['f_batchsize'] = 1
    defaults['f_personnelbreaktime_planned'] = 0.5
    defaults['f_personnelworktimepershift_planned'] = 7.5
    defaults['f_shiftsperday'] = 2
    #Economic Process Data
    defaults['f_costofapplicationdowntime_planned'] = 180
    defaults['f_costofapplicationdowntime_unplanned'] = 1500
    defaults['f_wagesetup'] = 35
    defaults['f_wageoperator'] = 35
    defaults['f_operator_number'] = 1
    #Product Data
    defaults['f_individualizationpercentage'] = 0
    defaults['f_inspectedpercentage'] = 100
    defaults['f_reworkpercentage'] = 100
    defaults['f_reworkduration_vibration'] = 0.0166667
    defaults['f_reworkduration'] = 4
    defaults['f_inspectiontime'] = 3
    defaults['f_productvariantsonapplication'] = 1
    defaults['f_totalproductvariants'] = 5
    #Economic Product Data
    defaults['f_sellingprice'] = 27500
    defaults['f_individualizationadditionalprofit'] = 0
    defaults['f_materialcostpart'] = 1500
    defaults['f_qualitycontrolcostpart'] = 25
    defaults['f_disposalcostpart'] = 5000
    defaults['f_materialcostrework'] = 0
    defaults['f_wagerework'] = 35
    
    insertDefaultData([defaults])
    
    db.t_applicationData_blisk.insert(**db.t_applicationData_blisk._filter_fields(choiceNo5G))
    db.t_bliskData.insert(**db.t_bliskData._filter_fields(bliskData))
    db.t_blisktasks_roughing.insert(**db.t_blisktasks_roughing._filter_fields(ctRoughing))
    db.t_blisktasks_prefinish.insert(**db.t_blisktasks_prefinish._filter_fields(ctPre))
    db.t_blisktasks_bladefinish.insert(**db.t_blisktasks_bladefinish._filter_fields(ctBlade))
    db.t_blisktasks_hubfinish.insert(**db.t_blisktasks_hubfinish._filter_fields(ctHub))
    
    redirect(URL('ct_start','mainScreen'))

def insertDefaultData(data):
    tableNameList = ['t_goals', 't_economicFacilitydata', 't_facilitydata', 't_economicFailureData', 't_technicalFailureData','t_economicProcessData', 't_technicalProcessData', ' t_economicProductData', 't_technicalProductData']
    
    db.t_applicationselection.insert(**db.t_applicationselection._filter_fields(data[0]))
    db.t_choices.insert(**db.t_choices._filter_fields(data[0]))
    db.t_comparetechspecs.insert(**db.t_comparetechspecs._filter_fields(data[0]))
    db.t_comparetech.insert(**db.t_comparetech._filter_fields(data[0]))
    db.t_statusquo.insert(**db.t_statusquo._filter_fields(data[0]))
    #Input Data
    db.t_economicFacilitydata.insert(**db.t_economicFacilitydata._filter_fields(data[0]))
    db.t_facilitydata.insert(**db.t_facilitydata._filter_fields(data[0]))
    db.t_economicFailureData.insert(**db.t_economicFailureData._filter_fields(data[0]))
    db.t_technicalFailureData.insert(**db.t_technicalFailureData._filter_fields(data[0]))
    db.t_economicProcessData.insert(**db.t_economicProcessData._filter_fields(data[0]))
    db.t_technicalProcessData.insert(**db.t_technicalProcessData._filter_fields(data[0]))
    db.t_economicProductData.insert(**db.t_economicProductData._filter_fields(data[0]))
    db.t_technicalProductData.insert(**db.t_technicalProductData._filter_fields(data[0]))
############Functions to delete some tables
def deleteGoals():
    if (not db(db.t_goals).isempty()):
        db.t_goals.truncate()

def deleteControlTasks():
    #Delete AGV Control Tasks
    if (not db(db.t_trackSetup).isempty()):
        db.t_trackSetup.truncate()
    if (not db(db.t_applicationData_obstacles).isempty()):
        db.t_applicationData_obstacles.truncate()
    #Delete BLISKS Control Tasks
    if (not db(db.t_bliskData).isempty()):
        db.t_bliskData.truncate()
    if (not db(db.t_blisktasks_roughing).isempty()):
        db.t_blisktasks_roughing.truncate()
    if (not db(db.t_blisktasks_prefinish).isempty()):
        db.t_blisktasks_prefinish.truncate()
    if (not db(db.t_blisktasks_bladefinish).isempty()):
        db.t_blisktasks_bladefinish.truncate()
    if (not db(db.t_blisktasks_hubfinish).isempty()):
        db.t_blisktasks_hubfinish.truncate()
    if (not db(db.t_blisktasks_accumulated).isempty()):
        db.t_blisktasks_accumulated.truncate()

def deleteInputData():
    if (not db(db.t_kpi).isempty()):
        db.t_kpi.truncate()
    #Input Dataf
    if (not db(db.t_economicFailureData).isempty()):
        db.t_economicFailureData.truncate()
    if (not db(db.t_technicalFailureData).isempty()):
        db.t_technicalFailureData.truncate()
    if (not db(db.t_economicProcessData).isempty()):
        db.t_economicProcessData.truncate()
    if (not db(db.t_technicalProcessData).isempty()):
        db.t_technicalProcessData.truncate()
    if (not db(db.t_economicProductData).isempty()):
        db.t_economicProductData.truncate()
    if (not db(db.t_technicalProductData).isempty()):
        db.t_technicalProductData.truncate()
    if (not db(db.t_facilitydata).isempty()):
        db.t_facilitydata.truncate()
    if (not db(db.t_economicFacilitydata).isempty()):
        db.t_economicFacilitydata.truncate()
    #Input Data
    if (not db(db.t_investmentdata).isempty()):
        db.t_investmentdata.truncate()
def deleteApplicationData():
    if (not db(db.t_choices).isempty()):
        db.t_choices.truncate()
    #Delete Choice of chosen Compare Technology
    if (not db(db.t_comparetech).isempty()):
        db.t_comparetech.truncate()
    if (not db(db.t_comparetechspecs).isempty()):
        db.t_comparetechspecs.truncate()
    #Delete current technology possesed for network
    if (not db(db.t_statusquo).isempty()):
        db.t_statusquo.truncate()
    #Delete Choice of Application
    if (not db(db.t_applicationselection).isempty()):
        db.t_applicationselection.truncate()
    if (not db(db.t_applicationData_blisk).isempty()):
        db.t_applicationData_blisk.truncate()
    if (not db(db.t_applicationData_mobileRobots).isempty()):
        db.t_applicationData_mobileRobots.truncate()

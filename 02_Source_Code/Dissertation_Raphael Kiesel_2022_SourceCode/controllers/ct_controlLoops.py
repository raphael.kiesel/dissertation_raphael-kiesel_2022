# -*- coding: utf-8 -*-
# try something like
def enterControlLoops():
    nextUrl = URL('ct_start','mainScreen')
    backUrl = URL('ct_start','mainScreen')
    formControlLoop = SQLFORM(db.t_trackSetup, fields=["f_trackType","f_distance", "f_amount"])
    formControlLoop.custom.submit["_value"] = "Add track part"
    obstaclesRecord = db.t_applicationData_obstacles(1)
    formObstacles = SQLFORM(db.t_applicationData_obstacles, obstaclesRecord)
    formObstacles.custom.submit["_value"] = "Safe"
    if obstaclesRecord is None:
        prefillObstacles(formObstacles)
    currentTrack = SQLFORM.grid(db.t_trackSetup, fields=[db.t_trackSetup.f_trackType,db.t_trackSetup.f_distance,db.t_trackSetup.f_amount],user_signature=False, searchable=False, sortable=False, csv=False, create=False, editable=False, details=False)
    if formControlLoop.process(keepvalues = True).accepted:
        response.flash = 'form accepted'
        redirect(URL('ct_controlLoops','enterControlLoops'))
    if formObstacles.process(keepvalues = True, onsuccess=lambda form: showSuccess(form)).accepted:
        response.flash = 'form accepted'
        #redirect(URL('ct_controlLoops','enterControlLoops'))
    return dict(form=formControlLoop, currentTrack=currentTrack, formObstacles = formObstacles,url=nextUrl, backUrl = backUrl, progress=10)

def enterBLISKControlTasks():
    nextUrl = URL('ct_controlLoops', 'enterBLISKData')
    backUrl = URL('ct_start','mainScreen')
    imgUrl = IMG(_src=URL('static','images/31_2_BLISK_Process_Overview.svg'))
    return dict(url=nextUrl,img = imgUrl, backUrl = backUrl, progress=10)

def enterBLISKData():
    nextUrl = URL('ct_controlLoops', 'enterBLISKRoughing')
    backUrl = URL('ct_controlLoops','enterBLISKControlTasks')
    imgUrl = IMG(_src=URL('static','images/31_1_BLISK_Simplified_Architecture.svg'))
    formControlLoop = SQLFORM(db.t_bliskData)
    formControlLoop.custom.submit["_value"] = "Continue"
    if formControlLoop.process(keepvalues = True).accepted:
        response.flash = 'form accepted'
        redirect(nextUrl)
    return dict(form=formControlLoop, url=nextUrl, img = imgUrl, backUrl = backUrl, progress=10)

def enterBLISKRoughing():
    nextUrl = URL('ct_controlLoops', 'enterBLISKPreFinish')
    backUrl = URL('ct_controlLoops','enterBLISKData')
    imgUrl = IMG(_src=URL('static','images/31_3_BLISK_Roughing.svg'))
    record = db.t_blisktasks_roughing(1)
    formControlLoop = SQLFORM(db.t_blisktasks_roughing, record)
    if record is None:
        prefillRoughing(formControlLoop)
    formControlLoop.custom.submit["_value"] = "Continue"
    if formControlLoop.process(keepvalues = True).accepted:
        response.flash = 'form accepted'
        redirect(nextUrl)
    return dict(form=formControlLoop, url=nextUrl, img = imgUrl, backUrl = backUrl, progress=10)

def enterBLISKPreFinish():
    nextUrl = URL('ct_controlLoops', 'enterBLISKBladeFinish')
    backUrl = URL('ct_controlLoops','enterBLISKRoughing')
    imgUrl = IMG(_src=URL('static','images/31_4_BLISK_Pre-Finishing.svg'))
    record = db.t_blisktasks_prefinish(1)
    formControlLoop = SQLFORM(db.t_blisktasks_prefinish, record)
    if record is None:
        prefillPreFinishing(formControlLoop)
    formControlLoop.custom.submit["_value"] = "Continue"
    if formControlLoop.process(keepvalues = True).accepted:
        response.flash = 'form accepted'
        redirect(nextUrl)
    return dict(form=formControlLoop, url=nextUrl, img = imgUrl, backUrl = backUrl, progress=10)

def enterBLISKBladeFinish():
    nextUrl = URL('ct_controlLoops', 'enterBLISKHubFinish')
    backUrl = URL('ct_controlLoops','enterBLISKPreFinish')
    imgUrl = IMG(_src=URL('static','images/31_5_BLISK_Finishing.svg'))
    record = db.t_blisktasks_bladefinish(1)
    formControlLoop = SQLFORM(db.t_blisktasks_bladefinish, record)
    if record is None:
        prefillBladeFinish(formControlLoop)
    formControlLoop.custom.submit["_value"] = "Continue"
    if formControlLoop.process(keepvalues = True).accepted:
        response.flash = 'form accepted'
        redirect(nextUrl)
    return dict(form=formControlLoop, url=nextUrl, img = imgUrl, backUrl = backUrl, progress=10)

def enterBLISKHubFinish():
    nextUrl = URL('ct_start', 'mainScreen')
    backUrl = URL('ct_controlLoops','enterBLISKBladeFinish')
    imgUrl = IMG(_src=URL('static','images/31_5_BLISK_Finishing.svg'))
    record = db.t_blisktasks_hubfinish(1)
    formControlLoop = SQLFORM(db.t_blisktasks_hubfinish, record)
    if record is None:
        prefillHubFinish(formControlLoop)
    formControlLoop.custom.submit["_value"] = "Continue"
    if formControlLoop.process(keepvalues = True).accepted:
        response.flash = 'form accepted'
        redirect(nextUrl)
    return dict(form=formControlLoop, url=nextUrl, img = imgUrl, backUrl = backUrl, progress=10)

#Helper Form Functions
#On Success function for obstacles form
def showSuccess(form):
    form.custom.submit["_value"] = XML("&#10004")
    pass

def prefillObstacles(form):
    form.vars.f_obstacles_straight = 0.01
    form.vars.f_obstacles_curve = 0.025
    form.vars.f_obstacles_crossing = 0.05
    pass

def prefillRoughing(form):
    form.vars.f_tooldamage_critical = 0.00001
    pass

def prefillPreFinishing(form):
    form.vars.f_tooldamage_critical = 0.00001
    pass

def prefillBladeFinish(form):
    form.vars.f_tooldamage_critical = 0.00001
    form.vars.f_tooldamage = 0.00001
    pass

def prefillHubFinish(form):
    form.vars.f_tooldamage_critical = 0.00001
    form.vars.f_tooldamage = 0.00001
    pass

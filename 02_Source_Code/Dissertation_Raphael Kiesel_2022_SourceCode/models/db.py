# -*- coding: utf-8 -*-

# -------------------------------------------------------------------------
# AppConfig configuration made easy. Look inside private/appconfig.ini
# Auth is for authenticaiton and access control
# -------------------------------------------------------------------------
from gluon.contrib.appconfig import AppConfig
from gluon.tools import Auth

# -------------------------------------------------------------------------
# This scaffolding model makes your app work on Google App Engine too
# File is released under public domain and you can use without limitations
# -------------------------------------------------------------------------

if request.global_settings.web2py_version < "2.15.5":
    raise HTTP(500, "Requires web2py 2.15.5 or newer")

# -------------------------------------------------------------------------
# if SSL/HTTPS is properly configured and you want all HTTP requests to
# be redirected to HTTPS, uncomment the line below:
# -------------------------------------------------------------------------
# request.requires_https()

# -------------------------------------------------------------------------
# once in production, remove reload=True to gain full speed
# -------------------------------------------------------------------------
configuration = AppConfig(reload=True)

if not request.env.web2py_runtime_gae:
    # ---------------------------------------------------------------------
    # if NOT running on Google App Engine use SQLite or other DB
    # ---------------------------------------------------------------------
    db = DAL(configuration.get('db.uri'),
             pool_size=configuration.get('db.pool_size'),
             migrate_enabled=configuration.get('db.migrate'),
             check_reserved=['all'])
else:
    # ---------------------------------------------------------------------
    # connect to Google BigTable (optional 'google:datastore://namespace')
    # ---------------------------------------------------------------------
    db = DAL('google:datastore+ndb')
    # ---------------------------------------------------------------------
    # store sessions and tickets there
    # ---------------------------------------------------------------------
    session.connect(request, response, db=db)
    # ---------------------------------------------------------------------
    # or store session in Memcache, Redis, etc.
    # from gluon.contrib.memdb import MEMDB
    # from google.appengine.api.memcache import Client
    # session.connect(request, response, db = MEMDB(Client()))
    # ---------------------------------------------------------------------

# -------------------------------------------------------------------------
# by default give a view/generic.extension to all actions from localhost
# none otherwise. a pattern can be 'controller/function.extension'
# -------------------------------------------------------------------------
response.generic_patterns = [] 
if request.is_local and not configuration.get('app.production'):
    response.generic_patterns.append('*')

# -------------------------------------------------------------------------
# choose a style for forms
# -------------------------------------------------------------------------
response.formstyle = 'bootstrap4_inline'
response.form_label_separator = ''

# -------------------------------------------------------------------------
# (optional) optimize handling of static files
# -------------------------------------------------------------------------
# response.optimize_css = 'concat,minify,inline'
# response.optimize_js = 'concat,minify,inline'

# -------------------------------------------------------------------------
# (optional) static assets folder versioning
# -------------------------------------------------------------------------
# response.static_version = '0.0.0'

# -------------------------------------------------------------------------
# Here is sample code if you need for
# - email capabilities
# - authentication (registration, login, logout, ... )
# - authorization (role based authorization)
# - services (xml, csv, json, xmlrpc, jsonrpc, amf, rss)
# - old style crud actions
# (more options discussed in gluon/tools.py)
# -------------------------------------------------------------------------

# host names must be a list of allowed host names (glob syntax allowed)
auth = Auth(db, host_names=configuration.get('host.names'))

# -------------------------------------------------------------------------
# create all tables needed by auth, maybe add a list of extra fields
# -------------------------------------------------------------------------
auth.settings.extra_fields['auth_user'] = []
auth.define_tables(username=False, signature=False)

# -------------------------------------------------------------------------
# configure email
# -------------------------------------------------------------------------
mail = auth.settings.mailer
mail.settings.server = 'logging' if request.is_local else configuration.get('smtp.server')
mail.settings.sender = configuration.get('smtp.sender')
mail.settings.login = configuration.get('smtp.login')
mail.settings.tls = configuration.get('smtp.tls') or False
mail.settings.ssl = configuration.get('smtp.ssl') or False

# -------------------------------------------------------------------------
# configure auth policy
# -------------------------------------------------------------------------
auth.settings.registration_requires_verification = False
auth.settings.registration_requires_approval = False
auth.settings.reset_password_requires_verification = True

# -------------------------------------------------------------------------  
# read more at http://dev.w3.org/html5/markup/meta.name.html               
# -------------------------------------------------------------------------
response.meta.author = configuration.get('app.author')
response.meta.description = configuration.get('app.description')
response.meta.keywords = configuration.get('app.keywords')
response.meta.generator = configuration.get('app.generator')
response.show_toolbar = configuration.get('app.toolbar')

# -------------------------------------------------------------------------
# your http://google.com/analytics id                                      
# -------------------------------------------------------------------------
response.google_analytics_id = configuration.get('google.analytics_id')

# -------------------------------------------------------------------------
# maybe use the scheduler
# -------------------------------------------------------------------------
if configuration.get('scheduler.enabled'):
    from gluon.scheduler import Scheduler
    scheduler = Scheduler(db, heartbeat=configuration.get('scheduler.heartbeat'))

# -------------------------------------------------------------------------
# Define your tables below (or better in another model file) for example
#
# >>> db.define_table('mytable', Field('myfield', 'string'))
#
# Fields can be 'string','text','password','integer','double','boolean'
#       'date','time','datetime','blob','upload', 'reference TABLENAME'
# There is an implicit 'id integer autoincrement' field
# Consult manual for more options, validators, etc.
#
# More API examples for controllers:
#
# >>> db.mytable.insert(myfield='value')
# >>> rows = db(db.mytable.myfield == 'value').select(db.mytable.ALL)
# >>> for row in rows: print row.id, row.myfield
# -------------------------------------------------------------------------

# -------------------------------------------------------------------------
# after defining tables, uncomment below to enable auditing
# -------------------------------------------------------------------------
# auth.enable_record_versioning(db)
#############################Functions for in controller
#Helper Functions which contain for each goal the data field names that are required
#Removes field from list if there is a not None entry in the database
def checkIfRecordsExist(liste):
    returnCopy = liste.copy()
    for field in returnCopy:
        #check if Field in any table in database
        for table_name in db.tables:
            #In Try Except Block to dodge errors from empty tables
            try:
                row = db(db[table_name]).select()[0]
            except:
                pass
            try:
                if not (row[field] is None):
                    liste.remove(field)
            except:
                pass
        pass
    return

def getNecessaryInputData():
    try:
        chosenUseCase = db(db.t_applicationselection).select().first().as_dict()["f_name"]
    except:
        redirect(URL('ct_start', 'mainScreen',vars=dict(alert = 'Please choose your Application before you enter the Data')))
    necessaryDataDict = {}
    if chosenUseCase == "agv":
        necessaryDataDict = necessaryInputDataAGV()
    elif chosenUseCase == "blisk":
        necessaryDataDict = necessaryInputDataBlisk()
    return necessaryDataDict

def necessaryInputDataAGV():
    fieldNameDict = {'Facility': [], 'Product': [], 'Failure': [], 'Process': []}
    if goals['f_flex']:
        updateFieldDict(fieldNameDict,inputDataAGVFlexibility())
    if goals['f_mob']:
        updateFieldDict(fieldNameDict,inputDataAGVMobility())
    if goals['f_prod']:
        updateFieldDict(fieldNameDict,inputDataAGVProductivity())
    if goals['f_qual']:
        updateFieldDict(fieldNameDict,inputDataAGVQuality())
    if goals['f_safe']:
        updateFieldDict(fieldNameDict,inputDataAGVSafety())
    if goals['f_sus']:
        updateFieldDict(fieldNameDict,inputDataAGVSustainability())
    if goals['f_util']:
        updateFieldDict(fieldNameDict,inputDataAGVUtilization())
    if (goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        updateFieldDict(fieldNameDict,inputDataAGVEconomic())
    return fieldNameDict

def necessaryInputDataBlisk():
    fieldNameDict = {'Facility': [], 'Product': [], 'Failure': [], 'Process': []}
    if goals['f_flex']:
        updateFieldDict(fieldNameDict,inputDataBLISKFlexibility())
    if goals['f_mob']:
        updateFieldDict(fieldNameDict,inputDataBLISKMobility())
    if goals['f_prod']:
        updateFieldDict(fieldNameDict,inputDataBLISKProductivity())
    if goals['f_qual']:
        updateFieldDict(fieldNameDict,inputDataBLISKQuality())
    if goals['f_safe']:
        updateFieldDict(fieldNameDict,inputDataBLISKSafety())
    if goals['f_sus']:
        updateFieldDict(fieldNameDict,inputDataBLISKSustainability())
    if goals['f_util']:
        updateFieldDict(fieldNameDict,inputDataBLISKUtilization())
    if (goals['f_npv'] or goals['f_roi'] or goals['f_opexp'] or goals['f_pb']):
        updateFieldDict(fieldNameDict,inputDataBLISKEconomic())
    return fieldNameDict

######Update replace function
def updateFieldDict(dictionary, toInsert):
    for key in toInsert.keys():
        dictionary[key] += toInsert[key]
    for key in dictionary.keys():
        dictionary[key] = filterDoubles(dictionary[key])
    return

###########Function to filter doubles out of list
def filterDoubles(liste):
    liste = list(dict.fromkeys(liste))
    return liste

####Input Data per Application per Goal
def inputDataAGVFlexibility():
    facData = []
    prodData = []
    failData = []
    procData = []
    ##Failure Data
    failData.append('f_felongterm_ttr')
    #Process Data
    procData.append('f_productiondaysperyear')
    procData.append('f_applicationmaintenance_time')
    procData.append('f_applicationoperationtime_planned')
    procData.append('f_applicationsetup_number')
    procData.append('f_applicationsetup_time')
    procData.append('f_batchsize')
    #Product Data
    prodData.append('f_productvariantsonapplication')
    prodData.append('f_totalproductvariants')
    return {'Facility': facData, 'Product': prodData, 'Failure': failData, 'Process': procData}
        
def inputDataAGVMobility():
    facData = []
    prodData = []
    failData = []
    procData = []
    #Necessary Facility Data
    facData.append('f_plantarea_total')
    facData.append('f_storagearea')
    facData.append('f_reworkarea')
    facData.append('f_paths_over5m')
    facData.append('f_paths_over35m')
    facData.append('f_paths_total')
    return {'Facility': facData, 'Product': prodData, 'Failure': failData, 'Process': procData}

def inputDataAGVProductivity():
    facData = []
    prodData = []
    failData = []
    procData = []
    ##Failure Data
    failData.append('f_felongterm_ttr')
    #Process Data
    procData.append('f_productiondaysperyear')
    procData.append('f_applicationmaintenance_time')
    procData.append('f_applicationoperationtime_planned')
    procData.append('f_applicationsetup_number')
    procData.append('f_applicationsetup_time')
    procData.append('f_batchsize')
    procData.append('f_personnelbreaktime_planned')
    procData.append('f_personnelworktimepershift_planned')
    procData.append('f_shiftsperday')
    return {'Facility': facData, 'Product': prodData, 'Failure': failData, 'Process': procData}

def inputDataAGVQuality():
    facData = []
    prodData = []
    failData = []
    procData = []
    ##Failure Data
    failData.append('f_felongterm_ttr')
    #Process Data
    procData.append('f_productiondaysperyear')
    procData.append('f_applicationmaintenance_time')
    procData.append('f_applicationoperationtime_planned')
    procData.append('f_applicationsetup_number')
    procData.append('f_applicationsetup_time')
    procData.append('f_batchsize')
    #Product Data
    prodData.append('f_inspectedpercentage')
    prodData.append('f_reworkpercentage')
    prodData.append('f_timetorework')
    prodData.append('f_inspectiontime')
    return {'Facility': facData, 'Product': prodData, 'Failure': failData, 'Process': procData}

def inputDataAGVSafety():
    facData = []
    prodData = []
    failData = []
    procData = []
    ##Failure Data
    failData.append('f_felongterm_ttr')
    #Process Data
    procData.append('f_productiondaysperyear')
    procData.append('f_applicationmaintenance_time')
    procData.append('f_applicationoperationtime_planned')
    procData.append('f_applicationsetup_number')
    procData.append('f_applicationsetup_time')
    procData.append('f_batchsize')
    procData.append('f_personnelbreaktime_planned')
    procData.append('f_personnelworktimepershift_planned')
    procData.append('f_shiftsperday')
    return {'Facility': facData, 'Product': prodData, 'Failure': failData, 'Process': procData}

def inputDataAGVSustainability():
    facData = []
    prodData = []
    failData = []
    procData = []
    #Necessary Facility Data
    facData.append('f_compressedairconsumption')
    facData.append('f_gasconsumption')
    facData.append('f_waterconsumption')
    ##Failure Data
    failData.append('f_felongterm_ttr')
    #Process Data
    procData.append('f_productiondaysperyear')
    procData.append('f_applicationmaintenance_time')
    procData.append('f_applicationoperationtime_planned')
    procData.append('f_applicationsetup_number')
    procData.append('f_applicationsetup_time')
    procData.append('f_batchsize')
    return {'Facility': facData, 'Product': prodData, 'Failure': failData, 'Process': procData}

def inputDataAGVUtilization():
    facData = []
    prodData = []
    failData = []
    procData = []
    ##Failure Data
    failData.append('f_felongterm_ttr')
    #Process Data
    procData.append('f_productiondaysperyear')
    procData.append('f_applicationmaintenance_time')
    procData.append('f_applicationoperationtime_planned')
    procData.append('f_applicationsetup_number')
    procData.append('f_applicationsetup_time')
    procData.append('f_batchsize')
    return {'Facility': facData, 'Product': prodData, 'Failure': failData, 'Process': procData}

def inputDataAGVEconomic():
    facData = []
    prodData = []
    failData = []
    procData = []
    #Necessary Facility Data
    facData.append('f_compressedairconsumption')
    facData.append('f_gasconsumption')
    facData.append('f_waterconsumption')
    
    facData.append('f_costelectricpower')
    facData.append('f_costcompressedair')
    facData.append('f_costgas')
    facData.append('f_costwater')
    facData.append('f_energytax')
    ##Failure Data
    failData.append('f_felongterm_ttr')
    failData.append('f_wagerepairstaff')
    failData.append('f_costmaterialrepair')
    failData.append('f_costfinancialcompensation')
    #Process Data
    procData.append('f_productiondaysperyear')
    procData.append('f_applicationmaintenance_time')
    procData.append('f_applicationoperationtime_planned')
    procData.append('f_applicationsetup_number')
    procData.append('f_applicationsetup_time')
    procData.append('f_personnelbreaktime_planned')
    procData.append('f_personnelworktimepershift_planned')
    procData.append('f_shiftsperday')
    procData.append('f_batchsize')
    #Process Data Economic
    procData.append('f_costofapplicationdowntime_planned')
    procData.append('f_costofapplicationdowntime_unplanned')
    procData.append('f_wagesetup')
    procData.append('f_wageoperator')
    procData.append('f_operator_number')
    #Product Data
    prodData.append('f_inspectedpercentage')
    prodData.append('f_reworkpercentage')
    prodData.append('f_timetorework')
    prodData.append('f_individualizationpercentage')
    #Product Data Economic
    prodData.append('f_sellingprice')
    prodData.append('f_individualizationadditionalprofit')
    prodData.append('f_materialcostpart')
    prodData.append('f_qualitycontrolcostpart')
    prodData.append('f_disposalcostpart')
    prodData.append('f_materialcostrework')
    prodData.append('f_wagerework')
    return {'Facility': facData, 'Product': prodData, 'Failure': failData, 'Process': procData}

def inputDataBLISKFlexibility():
    facData = []
    prodData = []
    failData = []
    procData = []
    ##Failure Data
    failData.append('f_fetoolbreakage_ttr')
    #Process Data
    procData.append('f_productiondaysperyear')
    procData.append('f_applicationmaintenance_time')
    procData.append('f_applicationoperationtime_planned')
    procData.append('f_applicationsetup_number')
    procData.append('f_applicationsetup_time')
    procData.append('f_applicationmaintenance_number')
    #Product Data
    prodData.append('f_inspectedpercentage')
    prodData.append('f_totalproductvariants')
    prodData.append('f_productvariantsonapplication')
    return {'Facility': facData, 'Product': prodData, 'Failure': failData, 'Process': procData}

def inputDataBLISKMobility():
    facData = []
    prodData = []
    failData = []
    procData = []
    #Necessary Facility Data
    facData.append('f_plantarea_total')
    facData.append('f_storagearea')
    facData.append('f_reworkarea_onepart')
    facData.append('f_transportarea')
    facData.append('f_paths_total')
    #Product Data
    prodData.append('f_reworkduration_vibration')
    prodData.append('f_reworkduration')
    return {'Facility': facData, 'Product': prodData, 'Failure': failData, 'Process': procData}

def inputDataBLISKProductivity():
    facData = []
    prodData = []
    failData = []
    procData = []
    ##Failure Data
    failData.append('f_fetoolbreakage_ttr')
    #Process Data
    procData.append('f_productiondaysperyear')
    procData.append('f_applicationmaintenance_time')
    procData.append('f_applicationoperationtime_planned')
    procData.append('f_applicationsetup_number')
    procData.append('f_applicationsetup_time')
    procData.append('f_applicationmaintenance_number')
    procData.append('f_personnelbreaktime_planned')
    procData.append('f_personnelworktimepershift_planned')
    procData.append('f_shiftsperday')
    return {'Facility': facData, 'Product': prodData, 'Failure': failData, 'Process': procData}

def inputDataBLISKQuality():
    facData = []
    prodData = []
    failData = []
    procData = []
    ##Failure Data
    failData.append('f_fetoolbreakage_ttr')
    #Process Data
    procData.append('f_productiondaysperyear')
    procData.append('f_applicationmaintenance_time')
    procData.append('f_applicationoperationtime_planned')
    procData.append('f_applicationsetup_number')
    procData.append('f_applicationsetup_time')
    procData.append('f_applicationmaintenance_number')
    #Product Data
    prodData.append('f_inspectedpercentage')
    prodData.append('f_reworkpercentage')
    prodData.append('f_reworkduration_vibration')
    prodData.append('f_reworkduration')
    prodData.append('f_inspectiontime')
    return {'Facility': facData, 'Product': prodData, 'Failure': failData, 'Process': procData}

def inputDataBLISKSafety():
    facData = []
    prodData = []
    failData = []
    procData = []
    #Failure Data
    failData.append('f_feaccidents')
    failData.append('f_fetoolbreakage_ttr')
    #Process Data
    procData.append('f_productiondaysperyear')
    procData.append('f_applicationmaintenance_time')
    procData.append('f_applicationoperationtime_planned')
    procData.append('f_applicationsetup_number')
    procData.append('f_applicationsetup_time')
    procData.append('f_applicationmaintenance_number')
    procData.append('f_personnelbreaktime_planned')
    procData.append('f_personnelworktimepershift_planned')
    procData.append('f_shiftsperday')
    #Product Data
    prodData.append('f_inspectedpercentage')
    return {'Facility': facData, 'Product': prodData, 'Failure': failData, 'Process': procData}

def inputDataBLISKSustainability():
    facData = []
    prodData = []
    failData = []
    procData = []
    #Necessary Facility Data
    facData.append('f_compressedairconsumption')
    facData.append('f_compressedairconsumption_rework')
    facData.append('f_gasconsumption')
    facData.append('f_waterconsumption')
    facData.append('f_electricpowerconsumption_application')
    facData.append('f_electricpowerconsumption_rework')
    ##Failure Data
    failData.append('f_fetoolbreakage_ttr')
    #Process Data
    procData.append('f_productiondaysperyear')
    procData.append('f_applicationmaintenance_time')
    procData.append('f_applicationoperationtime_planned')
    procData.append('f_applicationsetup_number')
    procData.append('f_applicationsetup_time')
    procData.append('f_applicationmaintenance_number')
    #Product Data
    prodData.append('f_inspectedpercentage')
    prodData.append('f_reworkpercentage')
    prodData.append('f_reworkduration_vibration')
    prodData.append('f_reworkduration')
    return {'Facility': facData, 'Product': prodData, 'Failure': failData, 'Process': procData}

def inputDataBLISKUtilization():
    facData = []
    prodData = []
    failData = []
    procData = []
    ##Failure Data
    failData.append('f_fetoolbreakage_ttr')
    #Process Data
    procData.append('f_productiondaysperyear')
    procData.append('f_applicationmaintenance_time')
    procData.append('f_applicationoperationtime_planned')
    procData.append('f_applicationsetup_number')
    procData.append('f_applicationsetup_time')
    procData.append('f_applicationmaintenance_number')
    return {'Facility': facData, 'Product': prodData, 'Failure': failData, 'Process': procData}

def inputDataBLISKEconomic():
    facData = []
    prodData = []
    failData = []
    procData = []
    #Necessary Facility Data
    facData.append('f_compressedairconsumption')
    facData.append('f_compressedairconsumption_rework')
    facData.append('f_gasconsumption')
    facData.append('f_waterconsumption')
    facData.append('f_electricpowerconsumption_application')
    facData.append('f_electricpowerconsumption_rework')
    #Economic Facility Data
    facData.append('f_costelectricpower')
    facData.append('f_costcompressedair')
    facData.append('f_costgas')
    facData.append('f_costwater')
    facData.append('f_energytax')
    ##Failure Data
    failData.append('f_fetoolbreakage_ttr')
    failData.append('f_wagerepairstaff')
    failData.append('f_costtoolbreakage')
    failData.append('f_costfinancialcompensation')
    #Process Data
    procData.append('f_productiondaysperyear')
    procData.append('f_applicationmaintenance_time')
    procData.append('f_applicationoperationtime_planned')
    procData.append('f_applicationsetup_number')
    procData.append('f_applicationsetup_time')
    procData.append('f_applicationmaintenance_number')
    procData.append('f_personnelbreaktime_planned')
    procData.append('f_personnelworktimepershift_planned')
    procData.append('f_shiftsperday')
    #Process Data Economic
    procData.append('f_costofapplicationdowntime_planned')
    procData.append('f_costofapplicationdowntime_unplanned')
    procData.append('f_wagesetup')
    procData.append('f_wageoperator')
    procData.append('f_operator_number')
    #Product Data
    prodData.append('f_inspectedpercentage')
    prodData.append('f_reworkpercentage')
    prodData.append('f_reworkduration_vibration')
    prodData.append('f_reworkduration')
    prodData.append('f_inspectiontime')
    prodData.append('f_individualizationpercentage')
    #Product Data Economic
    prodData.append('f_sellingprice')
    prodData.append('f_individualizationadditionalprofit')
    prodData.append('f_materialcostpart')
    prodData.append('f_qualitycontrolcostpart')
    prodData.append('f_disposalcostpart')
    prodData.append('f_materialcostrework')
    prodData.append('f_wagerework')
    return {'Facility': facData, 'Product': prodData, 'Failure': failData, 'Process': procData}

# -*- coding: utf-8 -*-
#TechnicalFailure Database
##########################################
db.define_table('t_technicalFailureData',
    Field('f_feaccidents', type='float', requires=IS_EMPTY_OR(IS_FLOAT_IN_RANGE(0,None)),
          label=T('Failure Events - Accidents/Collisions'), comment=""),
    Field('f_fetoolbreakage', type='float',
          label=T('Failure Events - Tool Breakage'), comment=""),
    Field('f_fetoolbreakage_ttr', type='float', requires=IS_EMPTY_OR(IS_FLOAT_IN_RANGE(0,None)),
          label=T('Failure Events - Tool Breakage - Time to Repair per Breakage'), comment=" h"),
    Field('f_fetoolbreakage_ttr_total', type='float',
          label=T('Failure Events - Tool Breakage - Time to Repair Total'), comment=" h/d"),
    Field('f_fedisabledcommunication', type='float',
          label=T('Failure Events - Disabled Communication'), comment="Failures/Day"),
    Field('f_felongterm', type='float',
          label=T('Failure Events - Long Term')),
    Field('f_felongterm_ttr', type='float', requires=IS_EMPTY_OR(IS_FLOAT_IN_RANGE(0,None)),
          label=T('Failure Events - Long Term - Time to Repair per Failure'), comment=" h"),
    Field('f_felongterm_ttr_total', type='float',
          label=T('Failure Events - Long Term - Time to Repair Total')),
    Field('f_feshortterm', type='float', requires=IS_EMPTY_OR(IS_FLOAT_IN_RANGE(0,None)),
          label=T('Failure Events - Short Term')),
    Field('f_feshortterm_ttr', type='float',
          label=T('Failure Events - Short Term - Time to Repair per Failure')),
    Field('f_feshortterm_ttr_total', type='float',
          label=T('Failure Events - Short Term - Time to Repair Total')),
    Field('f_failureevents_total', type='float',
          label=T('Failure Events - Total')),
    Field('f_failureevents_ttr_total', type='float',
          label=T('Failure Events - Total - Time to Repair')),
    Field('f_timebetweenfailures', type='float',
          label=T('Time between failures'), comment="Hour"),
    Field('f_mintimetorepair', type='float',
          label=T('Minimum Time to repair'), comment="Hour"),
    Field('f_maxtimetorepair', type='float',
          label=T('Maximum Time to repair'), comment="Hour"),
    Field('f_timetofailure', type='float',
          label=T('Time to failure')),
    migrate=settings.migrate)

############################################

#EconomicFailure Database
##########################################
db.define_table('t_economicFailureData',
    Field('f_wagerepairstaff', type='float',
          label=T('Failure Events - Wage of Repairments Staff'), comment=" €/h"),
    Field('f_costmaterialrepair', type='float',
          label=T('Accidents - Average Application Repair Cost '), comment=" €"),
    Field('f_costtoolbreakage', type='float',
          label=T('Failure Events - Cost for Tool Breakage'), comment=" €/failure"),
    Field('f_costfinancialcompensation', type='float',
          label=T('Accidents - Average Compensation Cost'), comment=" €/failure"),
    Field('f_opexrepair', type='float',
          label=T('Opexrepair')),
    Field('f_opexaccidents', type='float',
          label=T('Opexaccidents')),
    Field('f_opexfailure', type='float',
          label=T('Opexfailure')),
    format='%(f_wagerepairstaff)s',
    migrate=settings.migrate)

############################################

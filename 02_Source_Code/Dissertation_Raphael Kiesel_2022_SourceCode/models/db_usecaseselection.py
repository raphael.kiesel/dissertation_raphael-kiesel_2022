############################################

db.define_table('t_applicationselection',
    Field('f_name', type='string',
          label=T('Name')),
    format='%(f_name)s',
    migrate=settings.migrate)

############################################

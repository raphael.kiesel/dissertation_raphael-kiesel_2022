#Database for specific choices made
##########################################
db.define_table('t_choices',
    Field('f_useCase',type='string',
          label=T('Use Case')),
    Field('f_country', type='string',
          label=T('Country')),
    format='%(f_useCase)s',
    migrate=settings.migrate)

#############################################

db.define_table('t_comparetech',
    Field('f_lte',type='boolean',
          label=T('4G/LTE-A')),
    Field('f_wifi', type='boolean',
          label=T('WiFi')),
    migrate=settings.migrate)

db.define_table('t_comparetechspecs',
    Field('f_availability',type='float',
          label=T('Availability'), comment="%"),
    Field('f_reliability',type='float',
          label=T('Reliability'), comment="%"),
    migrate=settings.migrate)
##################################################

db.define_table('t_goals',
    Field('f_flex',type='boolean',
          label=T('Flexibility')),
    Field('f_mob',type='boolean',
          label=T('Mobility')),
    Field('f_prod', type='boolean',
          label=T('Productivity')),
    Field('f_qual', type='boolean',
          label=T('Quality')),
    Field('f_safe', type='boolean',
          label=T('Safety')),
    Field('f_sus', type='boolean',
          label=T('Sustainability')),
    Field('f_util', type='boolean',
          label=T('Utilization')),
    Field('f_roi', type='boolean',
          label=T('Return of Investment')),
    Field('f_npv', type='boolean',
          label=T('Net Present Value')),
    Field('f_pb', type='boolean',
          label=T('Payback Period')),
    Field('f_opexp', type='boolean',
          label=T('Opex per Product')),
    format='%(f_mob)s',
    migrate=settings.migrate)

#############################################

db.define_table('t_statusquo',
    Field('f_ncs',type='boolean', #####Only to check if the 3 below are to be shown or not
          label=T('Networked Control System')),
    Field('f_actuator',type='boolean',
          label=T('Actuator')),
    Field('f_controller',type='boolean',
          label=T('Controller')),
    Field('f_sensor',type='boolean',
          label=T('Sensor')),
    Field('f_netarchitecture',type='boolean', #####Only to check if the ones below are to be shown or not
          label=T('Network Architecture')),
    Field('f_basestation', type='boolean',
          label=T('5G Base Station/gNodeB')),
    Field('f_core', type='boolean',
          label=T('5G Core')),
    Field('f_userequipment',type='boolean',
          label=T('5G User Equipment')),
    Field('f_controlnet',type='boolean',
          label=T('Control Network')),
    Field('f_datanet',type='boolean',
          label=T('Data Network')),
    Field('f_userplane',type='boolean',
          label=T('User Plane')),
    migrate=settings.migrate)

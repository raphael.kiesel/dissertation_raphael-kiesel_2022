from gluon.storage import Storage
settings = Storage()

settings.migrate = True
settings.title = '5G_Smart'
settings.subtitle = 'powered by web2py'
settings.author = 'WZL - Alexander Mann'
settings.author_email = 'alexander.mann@wzl.rwth-aachen.de'
settings.keywords = None
settings.description = None
settings.layout_theme = 'Default'
settings.database_uri = 'sqlite://storage.sqlite'
settings.security_key = 'afabcc96-2fba-4b3d-ace4-beb5fab81c7e'
settings.email_server = 'logging'
settings.email_sender = None
settings.email_login = None
settings.login_method = 'local'
settings.login_config = None
settings.plugins = []

# Welcome to my GitLab! 

Dear Visitor,

welcome to the GitLab of my dissertation "Techno-Economic Evaluation of 5G Technology for Latency-Critical Applications in Production".

In this GitLab, you find the browser-based tool that enables a techno-economic evaluation of 5G technology for latency-critical applications in production. It was implemented using web2py. The GitLab is structured in 3 files. Their content will be briefly defined in the following.


In case of questions, please feel free to reach out to me anytime!

Best regards,


**Raphael Kiesel**

Aachen | 04.04.2022

email: mail@raphael-kiesel.de | raphael.kiesel@rwth-aachen.de


### 01_Compiled_Version
The folder “01_Compiled_Version” contains the source code in accordance with Figure 5.11. 
- **Dissertation_Raphael Kiesel_Compiled:** This version is for Windows. 
- **Dissertation_Raphael Kiesel_2022_web2py:** This version is for Mac/Linux Users. 
The tool is generally usable with any browser, however, the recommended browser ist GoogleChrome.

### 02_Source_Code
The folder “02_Source_Code” contains the web2py source code in order to understand the underlying mathematical relations to quantify the techno-economic impact of 5G technology on latency-critical applications in production. 

### 03_Excel_Files
The folder “03_Excel_Files” contains two excel sheets, one for each application, which is currenty implemented in the tool. 
The excel files do not contain the complete model but mainly focus on the relation between data, elements, KPI, and the performance goals. 
